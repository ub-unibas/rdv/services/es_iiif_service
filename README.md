# es_iiif_service

IIIF Presentation API and Search/Autocomplete API based on Elasticsearch

returns IIIF collections or manifests (Presentation API) or Annotation-lists (Search API)

developed for a flexible presentation of clipped newspaper articles in UniversalViewer

## Features
* flexible definition where (sub-)collections end and manifests start
* flexible definition how to build collection and manifest structure
* multiple manifests can be joined to one manifests (= "Lesemappe" for different articles)

## Necessary configuration
* ini file to define structure, hosts, field mapping, ...: see and rename ini/template.ini
* each new project needs a new ini-file: naming convention ini/<project>.ini
* after ini file is created it is loaded directly when the project API is called e.g. host/<project>/collection
* build own subclasses from IIIFCollection and IIIFManifest to define image Path, which metadata shall be shown, ...

## Url-syntax
* collection: host/<project>/collection/<one or multiple collection-filters sperated by "/">
* manifest: host/<project>/<one or multiple ids sperated by ",">/manifest
* search: host/<project>/services/<one or multiple ids sperated by ",">/query?q=
* autocomplete: host/<project>/services/<one or multiple ids sperated by ",">/autocomplete?q=

## Examples
* collection: https://ub-iiifpresentation.ub.unibas.ch/ezas/collection
* manifest: https://ub-iiifpresentation.ub.unibas.ch/ezas/1565d/manifest/
* search: https://ub-iiifpresentation.ub.unibas.ch/ezas/services/1565d/autocomplete/?q=basel
* autocomplete: https://ub-iiifpresentation.ub.unibas.ch/ezas/services/1565d/autocomplete/?q=geld

## Requirements
 * IIIF Image Server: e.g. SIPI
 * Elasticsearch-server containing metadata for manifests
 * Elasticsearch-server containing fulltext (optional)
 * Redis for caching
 * nginx or uwsgi module for apache: sudo apt-get install libapache2-mod-proxy-uwsgi

## external pre-requirements
 * set up IIIF Image Server and add images
 * set up Elasticsearch
 * ingest manifest/collection data (elasticsearch)
   * index structure: ingest one document for each manifest (and a array field containing all pages)
     * or one document for each page
   * necessary fields:
     * manifest_id: id to which manifest a page belongs
     * page_id: what is the id for each image (filename)
   * ingest fulltext-search data (elasticsearch)
     * index structure: one document for each page
     * necessary fields:
       * manifest_id: id to which manifest a page belongs
       * fulltext_field: fulltext to search within
       * textcoords: array of arrays for each word on one page,
         * each array contains the word and a string containing all coordinates (HPOS,VPOS,WIDTH,HEIGHT)
         * e.g.: [[word1, "10,20,40,80"], [word2, "70,20,30,50"], ... ]

## setting up the project
 * set up venv für python3: /home/iiif/venv:
   * install CMRESLogging, ub-unibas-utilities, es_iiif_service
 * set up redis (explained for ubuntu):
   * install from packet manager
   * change /etc/redis.conf:
     * supervised systemd
     * bind 127.0.0.1
     * unixsocket /run/redis/redis.sock (make sure it has correct permissions)
 * install apache and apache uwsgi module or use nginx
   * add and adapt (https/servername) conf-file from deployment folder and copy to
 * run service:
   * for local
     * set RDV_DEBUG=1
     * setup virtual environment: python3 -m venv ./venv
     * install requirements: ./venv/bin/python3 -m pip install -r requirements.txt
     * start service: ./venv/bin/python3 uwsgi_IIIF.py
   * for development
     * export FLASK_APP=iiifStartService.py
     * export FLASK_ENV=development
     * flask run
   * for production
     * https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-ubuntu-16-04
     * copy service esIIIFService to /etc/systemd/system/ and start/enable service (runs as user iiif (add to group redis))
     * copy uswgi_IIIF.py and uswgi_IIIF.ini to /home/iiif/es_iiif_service or change esIIIFService settings
     * copy and adapt ini files to /home/iiif/es_iiif_service/ini
     * rename global_template.ini to global.ini, rename logger_template.ini or adapt path in global_template.ini


## Remark

There is a need to tag the project, as the es_iiif_service is a requirement for es_iiif_service (I don't really understand why...)
