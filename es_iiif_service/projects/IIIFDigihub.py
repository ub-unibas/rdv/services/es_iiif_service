from es_iiif_service.projects.IIIFBaseProj import IIIFBaseProjManifest_v3, IIIFBaseProjManifest
from copy import deepcopy
import urllib.parse

class IIIFDigihubManifest_v3(IIIFBaseProjManifest_v3):
    proj_label = "Digihub Universitätsbibliothek Basel"
    config_store_id = "digihub"
    continous_ini = "digihub2"

    def _get_resource_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        goobi_file = page[self._canvas_id_field].replace("/gpfs/nasfs02/ub-digi/", "")
        link = "{}/{}/{}".format(self.imageserver_host, "digispace", urllib.parse.quote_plus(goobi_file))
        return link

    def _get_image_dimensions(self, page):
        image_dimensions = {"width": 0, "height": 0}

        return image_dimensions

class IIIFDigihubManifest(IIIFBaseProjManifest):
    proj_label = "Digihub Universitätsbibliothek Basel"
    config_store_id = "digihub"
    continous_ini = "digihub2"

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        goobi_file = page[self._canvas_id_field].replace("/gpfs/nasfs02/ub-digi/", "")
        link = "{}/{}/{}".format(self.imageserver_host, "digispace", urllib.parse.quote_plus(goobi_file))
        return link

    def _get_image_dimensions(self, page, page_id, image_dimensions_db={}):
        image_dimensions = {"width": 0, "height": 0}
        return image_dimensions
