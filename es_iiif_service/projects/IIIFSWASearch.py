from copy import deepcopy

from es_iiif_service import IIIFCollection, IIIFManifest_v3, IIIFManifest
from collections import OrderedDict

class IIIFSWASearchManifest_v3(IIIFManifest_v3):

    config_store_id = "swa"

    def __init__(self, **kwargs):
        """build Manifest for Portraets project"""
        super(IIIFSWASearchManifest_v3, self).__init__(**kwargs)

    def _get_new_manifest(self, page_metadata: dict, label: str = "Sammelanzeige SWA Search") -> dict:
        """return a fresh manifest for a page (already containing metadata)
        if multiple manifests are merged to one manifest no AlephLink and Schlagwort is added on toplevel

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_manifest)
        print(page_metadata)
        new_manifest["metadata"] = []
        new_manifest["requiredStatement"] = self._build_lang_map_l_v("Attribution",
                                                                     "SWA Search UB Basel")
        new_manifest["rights"] = "http://rightsstatements.org/vocab/InC/1.0/"
        new_manifest["behavior"] = ["unordered"]
        new_manifest["logo"] = [{
            "id": "https://www.unibas.ch/dam/jcr:93abfa0e-58d6-45ed-930a-de414ca40b13/uni-basel-logo.svg",
            "type": "Image",
            "format": "image/png",
            "service": [{
                "type": "ImageService2",
                "profile": "level2",
                "id": "https://www.unibas.ch/dam/jcr:93abfa0e-58d6-45ed-930a-de414ca40b13/uni-basel-logo.svg"
            }]
        }]

        if self.multiple_ids_marker or self._flex:

            new_manifest["label"] = {"de": ["IIIF Presentation API 3.0 (Beta) SWA Search UB Basel"],
                                     "en": ["IIIF Presentation API 3.0 (Beta) SWA Search UB Basel"]}
            new_manifest["description"] = {"""de": ["Sie nutzen die IIIF Presentation API der UB Basel. Wir befinden uns noch im Beta-Betrieb. "
                                                 "Nutzen Sie die Manifeste in anderen IIIF-Applikationen, aber Achtung diese sind noch nicht zu 100% stabil."]"""
                                           "en": ["You are using the IIIF Presentaton API (Beta) from UB Basel. "
                                                  "We are still in beta. Reuse the manifest in other IIIF Manifests but they are not 100% stable."]}
            """new_manifest["summary"] = {"de": ["Sie nutzen die IIIF Presentation API der UB Basel. Wir befinden uns noch im Beta-Betrieb. "
                                                 "Nutzen Sie die Manifeste in anderen IIIF-Applikationen, aber Achtung diese sind noch nicht zu 100% stabil."],
                                           "en": ["You are using the IIIF Presentaton API (Beta) from UB Basel. "
                                                 "We are still in beta. Reuse the manifest in other IIIF Manifests but they are not 100% stable."]}"""
        else:
            label = page_metadata.get(self._manifest_label_field, "Kein Titel")
            if label and isinstance(label, list) and isinstance(label[0], dict):
                label = label[0].get("label", label)
            new_manifest.update(self._build_lang_map_l(self._build_full_manifest_label(page_metadata, label)))
            # is already included: new_manifest["description"] = "Description: Dossier " + self._build_full_manifest_label(page_metadata, label)
        return new_manifest

    def _get_new_canvas(self, page: dict) -> dict:
        """
        return a fresh canvas for a page (already containing metadata)
        if multiple manifests are merged to one manifest  AlephLink and Schlagwort are added to canvas metadata

        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        new_canvas = deepcopy(self.iiif_canvas)
        metadata_fields = []
        new_canvas.update(self._build_lang_map_l(page["title"][0].get("label")))

        from rdv_querybuilder_swa_ubit.swa_search.rdv import SWAView
        view_object = SWAView(page_id=page.get("id"), page=page, lang="de", iiif_host=self.s.base_url,
                              es_host=self.s.es_host)
        new_canvas["metadata"] = view_object.get_label_value_metadata(dict_type="iiif_v3",
                                                                      exclude_fields=[])

        if self.multiple_ids_marker:
            for label in metadata_fields:
                if label in page and page[label] and page[label] != "None":
                    if label == "swisscovery_Link":
                        page[label] = '<a href="' + page[label] + '">swisscovery Link</a>'
                    new_canvas["metadata"].append(self._build_lang_map_l_v(label, page[label], "de"))

        return new_canvas

    def _get_resource_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        return self.imageserver_host + page[self._canvas_id_field].get("url")

    def _get_image_dimensions(self, page):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page.get("iiif_imgs",{})[self._image_width_field]
            height = page.get("iiif_imgs",{})[self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            pass
        return image_dimensions

    @classmethod
    def _build_partof_map(self, id_, type_="Collection"):
        return None

class IIIFSWASearchManifest(IIIFManifest):

    config_store_id = "swa"
    def __init__(self, **kwargs):
        """build Manifest for DIZAS project"""
        super(IIIFSWASearchManifest, self).__init__(**kwargs)

    def _get_new_manifest(self, page_metadata: dict, label: str = "Porträtsammlung UB") -> dict:
        """return a fresh manifest for a page (already containing metadata)
        if multiple manifests are merged to one manifest no AlephLink and Schlagwort is added on toplevel

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_manifest)

        new_manifest["metadata"] = []
        new_manifest["attribution"] = "SWA Search UB Basel"
        new_manifest["license"] = "https://ub-easyweb.ub.unibas.ch/de/portraets/"
        new_manifest["logo"] = {
            "@id": "https://www.unibas.ch/dam/jcr:93abfa0e-58d6-45ed-930a-de414ca40b13/uni-basel-logo.svg",
            "service": {
                "@context": "http://iiif.io/api/image/2/context.json",
                "@id": "http://example.org/image-service/logo",
                "profile": "http://iiif.io/api/image/2/level2.json"
            }
        }
        if self.project == "swasearch2":
            new_manifest["viewingHint"] = "continuous"
            new_manifest["viewingDirection"] = "left-to-right"

        if self.multiple_ids_marker or self._flex:
            # new_manifest["label"] = label
            new_manifest["label"] = {"de": ["IIIF Presentation API 2.1 (Beta) SWA Search UB Basel"],
                                     "en": ["IIIF Presentation API 2.1 (Beta) SWA Search UB Basel"]}
            new_manifest["description"] = {
                "de": ["Sie nutzen die IIIF Presentation API  der UB Basel. Wir befinden uns noch im Beta-Betrieb. "
                       "Nutzen Sie die Manifeste in anderen IIIF-Applikationen, aber Achtung diese sind noch nicht zu 100% stabil."],
                "en": ["You are using the IIIF Presentaton API (Beta) from UB Basel. "
                       "We are still in beta. Reuse the manifest in other IIIF Manifests but they are not 100% stable."]}
        else:
            label = page_metadata.get(self._manifest_label_field, "Kein Titel")
            # to get label from id-label fields
            if label and isinstance(label, list) and isinstance(label[0], dict):
                label = label[0].get("label", label)
            new_manifest["label"] = self._build_full_manifest_label(page_metadata, label)
            # is already included: new_manifest["description"] = "Description: Dossier " + self._build_full_manifest_label(page_metadata, label)
        return new_manifest

    def _get_new_canvas(self, page: dict) -> dict:
        """
        return a fresh canvas for a page (already containing metadata)
        if multiple manifests are merged to one manifest  AlephLink and Schlagwort are added to canvas metadata

        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        # doppelte Seiten: http://ub-universalviewer.ub.unibas.ch/uv/uv.html#?manifest=http://ub-test-iiifpresentation.ub.unibas.ch/dizas/collection
        new_canvas = deepcopy(self.iiif_canvas)


        metadata_fields = []

        if self.multiple_ids_marker:
            newspaper_descriptor = "title"
            aleph_link = "swisscovery Link"
            additional_metadata_fields = [newspaper_descriptor, aleph_link]
            metadata_fields.extend(additional_metadata_fields)

        from rdv_querybuilder_swa_ubit.swa_search.rdv import SWAView
        view_object = SWAView(page_id=page.get("id"), page=page, lang="de", iiif_host=self.s.base_url,
                              es_host=self.s.es_host)


        new_canvas["metadata"] = view_object.get_label_value_metadata(dict_type="iiif_v2",
                                                                      exclude_fields=["Digitalisat Artikel",
                                                                                      "Digitalisate Dossier",
                                                                                      "Dokumentensammlung"])

        if self.multiple_ids_marker:
            for label in metadata_fields:
                if label in page and page[label] and page[label] != "None":
                    if label == "swisscovery Link":
                        page[label] = '<a href="' + page[label][0]["link"] + '">swisscovery Link</a>'
                    new_canvas["metadata"].append({"label": label, "value": page[label]})

        return new_canvas

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        return self.imageserver_host + page[self._canvas_id_field].get("url")

    def _get_image_dimensions(self, page, page_id, image_dimensions_db={}) -> dict:
        image_dimensions = {"width":"", "height": ""}

        try:
            width = page.get("iiif_imgs", {})[self._image_width_field]
            height = page.get("iiif_imgs", {})[self._image_height_field]
            image_dimensions = {"width": width,
                                "height": height}
        except TypeError:
            pass
        return  image_dimensions