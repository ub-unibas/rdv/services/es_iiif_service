from es_iiif_service import IIIFCollection, IIIFManifestPDF
from rdv_data_helpers_ubit.projects.swasachdok.swasachdok import PDF_LINKS

class IIIFSWAKleinschriftenCollection(IIIFCollection):
    iiif_top_collection = {
        "@context": [
        "http://wellcomelibrary.org/ld/ixif/0/context.json",
        "http://iiif.io/api/presentation/2/context.json"
        ],
        "@id": "http://unibas_newspapers",
        "label": "SWA Kleinschriften",
        "description": "SWA Kleinschriften",
        "@type": "sc:Collection",
        "collections": [],
    }

    def __init__(self, **kwargs):
        """build Collection for Impresso project"""
        super(IIIFSWAKleinschriftenCollection, self).__init__(**kwargs)

    def _get_agg_field_type(self, field: str) -> dict:
        """
        return ES search snippet how aggregation for special fields shall be build
        (e.g. for date: aggregation for every year)

        :param field: field name in es
        :return: dict containing ES search snippet used for building aggregation
        """
        collection_es_func = {}
        return collection_es_func.get(field, {})


class IIIFSWAKleinschriftenManifest(IIIFManifestPDF):

    def __init__(self, **kwargs):
        """build Manifest for Impresso project"""
        super(IIIFSWAKleinschriftenManifest, self).__init__(**kwargs)

    def _get_image_path(self, page: dict) -> str:
        subpath = page[self._canvas_id_field]
        link = subpath.get("link")
        link = link.replace("ub2.unibas.ch/", "ub.unibas.ch/")
        link = link.replace("ub.unibas.ch/digi/a125/", "ub-sachdokpdf.ub.unibas.ch/")
        return link

    def _get_image_label(self, page: dict) -> str:
        subpath = page[self._canvas_id_field]
        label = subpath.get("label")
        return label

    def _get_pdf_thumbnail(self, page: dict) -> str:
        pdf_id = page.get(PDF_LINKS, {}).get("link","")
        if "/sachdok/" in pdf_id:
            pdf_id = "-".join(pdf_id.split("/sachdok/")[-1].split("/"))
        else:
            pdf_id = "-".join(pdf_id.split("https://ub-sachdokpdf.ub.unibas.ch/")[-1].split("/"))
        if pdf_id:
            return "https://ub-sipi.ub.unibas.ch/swa_pdfs/{}@1/full/200,/0/default.jpg".format(pdf_id)

    def _get_image_dimensions(self, page, page_id, image_dimensions_db):
        image_dimensions = {}
        return image_dimensions