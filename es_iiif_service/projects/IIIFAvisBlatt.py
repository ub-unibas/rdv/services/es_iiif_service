from urllib import parse

from es_iiif_service.projects import IIIFImpressoCollection, IIIFImpressoManifest
from es_iiif_service.projects.IIIFBaseProj import IIIFBaseProjManifest_v3, IIIFBaseProjManifest

class IIIFAvisBlattManifest_v3(IIIFBaseProjManifest_v3):
    proj_label = "AVIS-Blatt"
    config_store_id = "newspapers"
    continous_ini = "avis_ocr2"

    def _get_seealso_path(self, page: dict, type_: str) -> str:
        """ build see also path

        :param page: dict containing all metadata from ES for a page
        :param type_: type of seeAlso Representation
        :return: URL for seeAlso Representation
        """

        return self.seealso_host

    def _get_page_id(self, page: dict) -> str:
        if page[self._canvas_id_field].get("iiif_url","").startswith("http"):
            return page[self._canvas_id_field]["iiif_url"]
        else:
            return page[self._canvas_id_field]["identifier"]

    def _add_seealso(self, new_canvas: dict, page: dict) -> None:
        """
        update iiif Canvas with links to seeAlso/Rendering Representations
        :param new_canvas: dict containing iiif canvas
        :param page: dict containing all metadata from ES for a page
        """
        new_canvas["seeAlso"] = []
        xml = page.get("pages", {}).get("xml_files","")
        bsb_plugin = [{"label": self._build_lang_map(xml,"de"), "type": "Dataset", "@id": self._get_seealso_path(page, "xml") + "/" + xml,
                       "format": "application/xml+alto", "profile": "http://www.loc.gov/standards/alto/v3/alto.xsd"} ]
        # todo: anpassen: da bsb plugin nur mit einseitigen Alto XML umgehen kann
        new_canvas["seeAlso"].extend(bsb_plugin)

        new_canvas["rendering"] = new_canvas["seeAlso"]

    def _get_new_canvas(self, page: dict) -> dict:
        """
        to be overwritten: return a fresh canvas for a page (already containing metadata)
        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        from copy import deepcopy
        new_canvas = deepcopy(self.iiif_canvas)
        newspaper_date_field = "date"
        newspaper_issue_field = "issue_number"
        date = "/".join(page.get(newspaper_date_field, ""))
        number = page.get(newspaper_issue_field, "")
        issue_def = " ".join([date, str(number)])

        new_canvas["label"] = " ".join(["AVIS-Blatt", issue_def])
        self._add_seealso(new_canvas=new_canvas, page=page)
        return new_canvas

    def _get_resource_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        if page[self._canvas_id_field].get("iiif_url","").startswith("http"):
            return page[self._canvas_id_field]["iiif_url"]
        else:
            return self.imageserver_host + page[self._canvas_id_field]["identifier"] + ".jpx"

    def _get_image_dimensions(self, page):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._canvas_id_field][self._image_width_field]
            height = page[self._canvas_id_field][self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            pass

        return image_dimensions

class IIIFAvisBlattManifest(IIIFBaseProjManifest):
    proj_label = "AVIS-Blatt"
    config_store_id = "newspapers"
    continous_ini = "avis_ocr2"

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        if page[self._canvas_id_field].get("iiif_url","").startswith("http"):
            return page[self._canvas_id_field]["iiif_url"]
        else:
            return self.imageserver_host + page[self._canvas_id_field]["identifier"] + ".jpx"

    def _get_image_dimensions(self, page, page_id, image_dimensions_db={}):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._canvas_id_field][self._image_width_field]
            height = page[self._canvas_id_field][self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            pass
        return image_dimensions