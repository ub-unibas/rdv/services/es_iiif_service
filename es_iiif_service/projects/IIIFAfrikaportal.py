from copy import deepcopy
from cache_decorator_redis_ubit import CacheDecorator
from es_iiif_service import IIIFCollection, IIIFManifest, ViewGenerator, IIIFManifest_v3

from es_iiif_service.projects.IIIFBaseProj import IIIFBaseProjManifest_v3, IIIFBaseProjManifest



class IIIIFAfrikaportalCollection(IIIFCollection):
    iiif_top_collection = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "@id": "http://afrikaportal",
        "label": "Afrikaportal",
        "description": "Afrikaportal",
        "@type": "sc:Collection",
        "collections": [],
    }


class IIIFAfrikaportalManifest(IIIFBaseProjManifest_v3):
    proj_label = "PARC"
    config_store_id = "afrikaportal"
    continous_ini = "afrikaportal2"

    def _get_new_manifest(self, page_metadata: dict, label: str = "Sammelanzeige Afrikaportal") -> dict:
        """return a fresh manifest for a page (already containing metadata)
        if multiple manifests are merged to one manifest no AlephLink and Schlagwort is added on toplevel

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_manifest)

        new_manifest["metadata"] = []
        new_manifest["requiredStatement"] = self._build_lang_map_l_v("Attribution",
                                                                     "Platform for African research collections")
        new_manifest["rights"] = "http://rightsstatements.org/vocab/InC/1.0/"
        new_manifest["behavior"] = ["unordered"]
        new_manifest["logo"] = [{
            "id": "https://www.unibas.ch/dam/jcr:93abfa0e-58d6-45ed-930a-de414ca40b13/uni-basel-logo.svg",
            "type": "Image",
            "format": "image/png",
            "service": [{
                "type": "ImageService2",
                "profile": "level2",
                "id": "https://www.unibas.ch/dam/jcr:93abfa0e-58d6-45ed-930a-de414ca40b13/uni-basel-logo.svg"
            }]
        }]

        if self.multiple_ids_marker or self._flex:

            new_manifest["label"] = {"de": ["IIIF Presentation API 3.0 (Beta) Platform for African research collections"],
                                     "en": ["IIIF Presentation API 3.0 (Beta) Platform for African research collections"]}
            new_manifest["description"] = {"""de": ["Sie nutzen die IIIF Presentation API der UB Basel. Wir befinden uns noch im Beta-Betrieb. "
                                                     "Nutzen Sie die Manifeste in anderen IIIF-Applikationen, aber Achtung diese sind noch nicht zu 100% stabil."]"""
                                           "en": ["You are using the IIIF Presentaton API (Beta) from UB Basel. "
                                                  "We are still in beta. Reuse the manifest in other IIIF Manifests but they are not 100% stable."]}
            """new_manifest["summary"] = {"de": ["Sie nutzen die IIIF Presentation API der UB Basel. Wir befinden uns noch im Beta-Betrieb. "
                                                 "Nutzen Sie die Manifeste in anderen IIIF-Applikationen, aber Achtung diese sind noch nicht zu 100% stabil."],
                                           "en": ["You are using the IIIF Presentaton API (Beta) from UB Basel. "
                                                 "We are still in beta. Reuse the manifest in other IIIF Manifests but they are not 100% stable."]}"""
        else:
            label = page_metadata.get(self._manifest_label_field, "Kein Titel")
            if label and isinstance(label, list) and isinstance(label[0], dict):
                label = label[0].get("label", label)
            new_manifest.update(self._build_lang_map_l(self._build_full_manifest_label(page_metadata, label)))
            # is already included: new_manifest["description"] = "Description: Dossier " + self._build_full_manifest_label(page_metadata, label)
        return new_manifest

    def _get_image_dimensions(self, page):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._canvas_id_field][self._image_width_field]
            height = page[self._canvas_id_field][self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):

            pass
        return image_dimensions

    def _get_resource_path(self, page: dict) -> str:
        """
        to be overwritten: get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        if page[self._canvas_id_field].get("id", "").startswith("http"):
            return page[self._canvas_id_field].get("id")
        else:
            # the media server collection is stored in the es field "media_server_collection"
            return self.imageserver_host + page.get("media_server_collection", "afrikaportal") + "/" + page[self._canvas_id_field].get("id") + '/iiif'

class IIIFAfrikaportalManifest_old(IIIFBaseProjManifest):
    proj_label = "PARC"
    config_store_id = "afrikaportal"
    continous_ini = "afrikaportal2"

    def __init__(self, **kwargs):
        """build Manifest for Impresso project"""
        super(IIIFAfrikaportalManifest, self).__init__(**kwargs)

    def _get_new_manifest(self, page_metadata, label):
        """to be overwritten: return a fresh manifest (already containing metadata)

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """

        new_manifest = deepcopy(self.iiif_manifest)
        if self.project == "afrikaportal2":
            new_manifest["viewingHint"] = "continuous"
            new_manifest["viewingDirection"] = "left-to-right"
        if self.project != "afrikaportal":
            new_manifest["label"] = label
        else:
            if self.multiple_ids_marker or self._flex:
                new_manifest["label"] = "IIIF Presentation API 3.0 (Beta) Afrika Portal"
            else:
                new_manifest["label"] = page_metadata.get(self._manifest_label_field,"")
        return new_manifest

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        file = page[self._canvas_id_field]

        if not file.startswith("http"):
            return self.imageserver_host + page[self._canvas_id_field]
        else:
            return page[self._canvas_id_field]