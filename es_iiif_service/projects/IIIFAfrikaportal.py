from copy import deepcopy
from cache_decorator_redis_ubit import CacheDecorator
from es_iiif_service import IIIFCollection, IIIFManifest, ViewGenerator, IIIFManifest_v3

from rdv_query_builder import AfricaView

class IIIFAfrikaportal:
    """
    functions which can be used by IIIFImpressoManifest and IIIFImpressoCollection
    """
    iiif_manifest = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "@id": "",
        "@type": "sc:Manifest",
        "label": "",
        "navDate": "",
        "license": "http://rightsstatements.org/vocab/CNE/1.0/",
        "attribution": "Afrikaportal Basel",
        "sequences": [],

        #"logo": "https://www.unibas.ch/dam/jcr:93abfa0e-58d6-45ed-930a-de414ca40b13/uni-basel-logo.svg"
    }

    def get_uuid_query(self, host, project, uuid):
        cache = CacheDecorator()
        query = cache.get_keyvalue_cache(uuid)
        return query



class IIIFAfrikaportal_v3:
    """
    functions which can be used by IIIFImpressoManifest and IIIFImpressoCollection
    """
    iiif_manifest = {
        "@context": "http://iiif.io/api/presentation/3/context.json",
        "id": "",
        "type": "Manifest",
        "label": "",
        "items": [],
    }

    def get_uuid_query(self, host, project, uuid):
        cache = CacheDecorator()
        query = cache.get_keyvalue_cache(uuid)
        return query



class IIIIFAfrikaportalCollection(IIIFAfrikaportal, IIIFCollection):
    iiif_top_collection = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "@id": "http://afrikaportal",
        "label": "Afrikaportal",
        "description": "Afrikaportal",
        "@type": "sc:Collection",
        "collections": [],
    }

    def __init__(self, **kwargs):
        """build Collection for Impresso project"""
        super(IIIIFAfrikaportalCollection, self).__init__(**kwargs)


class IIIFAfrikaportalManifest(IIIFAfrikaportal_v3, IIIFManifest_v3):

    def __init__(self, **kwargs):
        """build Manifest for Impresso project"""
        super(IIIFAfrikaportalManifest, self).__init__(**kwargs)

    def _get_new_manifest(self, page_metadata: dict, label: str = "Sammelanzeige Afrikaportal") -> dict:
        """return a fresh manifest for a page (already containing metadata)
        if multiple manifests are merged to one manifest no AlephLink and Schlagwort is added on toplevel

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_manifest)

        new_manifest["metadata"] = []
        new_manifest["requiredStatement"] = self._build_lang_map_l_v("Attribution",
                                                                     "Afrikaportal UB Basel")
        new_manifest["rights"] = "http://rightsstatements.org/vocab/InC/1.0/"
        new_manifest["behavior"] = ["unordered"]
        new_manifest["logo"] = [{
            "id": "https://www.unibas.ch/dam/jcr:93abfa0e-58d6-45ed-930a-de414ca40b13/uni-basel-logo.svg",
            "type": "Image",
            "format": "image/png",
            "service": [{
                "type": "ImageService2",
                "profile": "level2",
                "id": "https://www.unibas.ch/dam/jcr:93abfa0e-58d6-45ed-930a-de414ca40b13/uni-basel-logo.svg"
            }]
        }]

        if self.multiple_ids_marker or self._flex:

            new_manifest["label"] = {"de": ["IIIF Presentation API 3.0 (Beta) Afrikaportal UB Basel"],
                                     "en": ["IIIF Presentation API 3.0 (Beta) Afrikaportal UB Basel"]}
            new_manifest["description"] = {"""de": ["Sie nutzen die IIIF Presentation API der UB Basel. Wir befinden uns noch im Beta-Betrieb. "
                                                     "Nutzen Sie die Manifeste in anderen IIIF-Applikationen, aber Achtung diese sind noch nicht zu 100% stabil."]"""
                                           "en": ["You are using the IIIF Presentaton API (Beta) from UB Basel. "
                                                  "We are still in beta. Reuse the manifest in other IIIF Manifests but they are not 100% stable."]}
            """new_manifest["summary"] = {"de": ["Sie nutzen die IIIF Presentation API der UB Basel. Wir befinden uns noch im Beta-Betrieb. "
                                                 "Nutzen Sie die Manifeste in anderen IIIF-Applikationen, aber Achtung diese sind noch nicht zu 100% stabil."],
                                           "en": ["You are using the IIIF Presentaton API (Beta) from UB Basel. "
                                                 "We are still in beta. Reuse the manifest in other IIIF Manifests but they are not 100% stable."]}"""
        else:
            label = page_metadata.get(self._manifest_label_field, "Kein Titel")
            if label and isinstance(label, list) and isinstance(label[0], dict):
                label = label[0].get("label", label)
            new_manifest.update(self._build_lang_map_l(self._build_full_manifest_label(page_metadata, label)))
            # is already included: new_manifest["description"] = "Description: Dossier " + self._build_full_manifest_label(page_metadata, label)
        return new_manifest



    def _get_new_canvas(self, page: dict) -> dict:
        """
        return a fresh canvas for a page (already containing metadata)
        if multiple manifests are merged to one manifest  AlephLink and Schlagwort are added to canvas metadata

        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        # doppelte Seiten: http://ub-universalviewer.ub.unibas.ch/uv/uv.html#?manifest=http://ub-test-iiifpresentation.ub.unibas.ch/dizas/collection
        new_canvas = deepcopy(self.iiif_canvas)


        metadata_fields = []

        if self.multiple_ids_marker:
            newspaper_descriptor = "field_title"
            descriptor = self.tostr(page[newspaper_descriptor])

            new_canvas.update(self._build_lang_map_l(descriptor))

        from rdv_query_builder import AfricaView
        view_object = AfricaView(page_id = page.get("id"), page = page, lang = "de", iiif_host=self.s.base_url, es_host=self.s.es_host)

        new_canvas["metadata"] = view_object.get_label_value_metadata(dict_type="iiif_v3",
                                                           exclude_fields=["Digitalisat Artikel", "Digitalisate Dossier", "Dokumentensammlung"])
        new_canvas.update(self._build_lang_map_l(page.get("field_title")[0]))
        if self.multiple_ids_marker:
            for label in metadata_fields:
                if label in page and page[label] and page[label] != "None":
                    new_canvas["metadata"].append(self._build_lang_map_l_v(label, page[label], "de"))

        return new_canvas

    def _get_resource_path(self, page: dict) -> str:
        """
        to be overwritten: get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        file = page[self._canvas_id_field]
        if not file.startswith("http"):
            return self.imageserver_host + page[self._canvas_id_field]
        else:
            return page[self._canvas_id_field]

class IIIFAfrikaportalManifest_old(IIIFAfrikaportal, IIIFManifest):

    def __init__(self, **kwargs):
        """build Manifest for Impresso project"""
        super(IIIFAfrikaportalManifest, self).__init__(**kwargs)

    def _get_new_manifest(self, page_metadata, label):
        """to be overwritten: return a fresh manifest (already containing metadata)

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """

        new_manifest = deepcopy(self.iiif_manifest)
        if self.project == "afrikaportal2":
            new_manifest["viewingHint"] = "continuous"
            new_manifest["viewingDirection"] = "left-to-right"
        if self.project != "afrikaportal":
            new_manifest["label"] = label
        else:
            if self.multiple_ids_marker or self._flex:
                new_manifest["label"] = "IIIF Presentation API 3.0 (Beta) Afrika Portal"
            else:
                new_manifest["label"] = page_metadata.get(self._manifest_label_field,"")
        return new_manifest

    def _get_new_canvas(self, page: dict) -> dict:
        """
        to be overwritten: return a fresh canvas for a page (already containing metadata)
        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        new_canvas = deepcopy(self.iiif_canvas)
        institution = page.get("main_institution",[""])[0]

        # view_object = AfricaView(page_id=page.get("id"), page=page, lang="de", iiif_host=self.s.base_url)
        # new_canvas["metadata"] = view_object.get_iiif_view(page, institution, "de")
        # new_canvas["metadata"] = view_object.get_label_value_metadata(dict_type="iiif_v2")

        view_object = ViewGenerator(self.s)
        new_canvas["metadata"] = view_object.get_iiif_view(page, institution, "de")
        new_canvas["label"] = page["field_title"][0]
        return new_canvas

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        file = page[self._canvas_id_field]

        if not file.startswith("http"):
            return self.imageserver_host + page[self._canvas_id_field]
        else:
            return page[self._canvas_id_field]