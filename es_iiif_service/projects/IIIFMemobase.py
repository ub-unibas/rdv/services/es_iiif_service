from copy import deepcopy
import datetime

from es_iiif_service import IIIFCollection_v3, IIIFManifest_v3, ViewGenerator

file_type = "(jpg OR mp3 OR mp4)"

class IIIFMemboase:

    def _build_manifest_url(self, id_: str, page: dict = {}, flex=False) -> str:
        """ build url for manifest

        :param id_: id for manifest
        :param page: example page for which manifest is built
        :return: return url for manifest
        """
        urlpart_manifest = "/manifest/" if not flex else "/flex_manifest/"
        if self.project != "memobase_inst":
            memobase_id = [i["ns2:identifier"] for i in page["identifier"] if i["@typeLabel"] == "Main"][0]
        else:
            memobase_id = id_
        manif_url = self.iiif_service_host + self.project + "/" + self.url_encode4es(str(memobase_id)) + urlpart_manifest
        return manif_url

class IIIFMemobaseCollection(IIIFMemboase, IIIFCollection_v3):
    iiif_top_collection = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "id": "http://memobase",
        "label": IIIFCollection_v3._build_lang_map("Memobase"),
        "summary": IIIFCollection_v3._build_lang_map("Memobase"),
        "type": "Collection",
        "items": [],
    }

    def _build_col_filters(self):
        filters = super()._build_col_filters()
        filters.append(              {
                "query_string": {
                  "fields": [
                    "format.essenceLocator.locatorInfo"
                  ],
                  "query": "\\.{}".format(file_type)
                }
              })
        return filters

    def __init__(self, **kwargs):
        """build Collection for Memobase project"""
        super().__init__(**kwargs)


class IIIFMemobaseManifest(IIIFMemboase, IIIFManifest_v3):

    def __init__(self, **kwargs):
        """build Manifest for Memobase project"""
        super().__init__(**kwargs)

    def _get_manifest_query(self, limit: int, terms_manifest_id_field: str) -> dict:
        """ to be overwritten, define query to get all data for a manifest

        :param limit: max number of pages to be returned
        :param terms_manifest_id_field: es field name to be used to limit query to a certain manifest
        :return: es query dict
        """

        if self.search_marker:
            query_term = self._manifest_ids[0][2:]
            manifest_query = {
                "size": limit,
                # todo: aus ini auslesen
                "_source": {"excludes": ["*fulltext", "textcoords*"]},
                "query": {
                    "bool": {
                        "must": [
                                    {"exists": {"field": self._canvas_id_field}},
                                    {"match": {self.s.mapping_fulltext["fulltext"]: query_term}},
                                    {
                                        "query_string": {
                                            "fields": [
                                                "format.essenceLocator.locatorInfo"
                                            ],
                                            "query": "\\.{}".format(file_type)
                                        }
                                    }

                                ] + self._get_col_filters4es()
                    }
                }
            }

        else:

            manifest_query = {
                "size": limit,
                # todo: aus ini auslesen
                "_source": {"excludes": ["*fulltext", "textcoords*"]},
                "query": {
                    "bool": {
                        "must": [
                            {"exists": {"field": self._canvas_id_field}},
                            {"terms": {terms_manifest_id_field: self._manifest_ids}},
                                    {
                                        "query_string": {
                                            "fields": [
                                                "format.essenceLocator.locatorInfo"
                                            ],
                                            "query": "\\.{}".format(file_type)
                                        }
                                    }

                        ]+ self._get_col_filters4es()
                    }

                }
            }

        return manifest_query

    def _get_duration(self, page):
        """return duration of av objects, tbo"""

        duration = page.get("format", {}).get("duration", {}).get("time", "")
        seconds = 0
        if duration:
            duration = datetime.datetime.strptime(duration, "%H:%M:%S").time()
            duration = datetime.datetime.combine(datetime.date.min, duration) - datetime.datetime.min
            seconds = duration.total_seconds()
        av_duration = {"duration": seconds}
        return av_duration

    def _get_object_type(self, page: dict) -> str:
        """
        to be overwritten: determine object type
        :param page: dict containing all metadata from ES for a page
        :return: object type: sound, video, image, iiif
        """
        return page["type"]["objectType"]["@typeLabel"]

    def _get_resource_path(self, page: dict) -> str:
        """
        to be overwritten: get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        if isinstance(page["format"]["essenceLocator"], list):
            return page["format"]["essenceLocator"][0]["locatorInfo"]
        else:
            return page["format"]["essenceLocator"]["locatorInfo"]

    def _get_page_id(self, page: dict) -> str:
        memobase_id = [i["ns2:identifier"] for i in page["identifier"] if i["@typeLabel"] == "Main"][0]
        return memobase_id

    def _get_nested_es_metadata(self, page):
        metadata = []

        meta_defs = [("Zeit", "coverage.temporal.PeriodOfTime.@period"),
                    ("Beschreibung", "description"),
                     ("Video-Format", "format.videoFormat.technicalAttributeString"),
                     ("Ressource-link", "format.essenceLocator.locatorInfo")]

        for definition in meta_defs:
            l, f = definition
            paths = f.split(".")
            value = self._recurse_dict(page, paths)
            if isinstance(value, list):
                for v in value:
                    type_l = l + ": " + v.get("@typeLabel")
                    v = v.get("ns2:"+f.split(".")[-1]) or v.get("#text")
                    metadata.append(self._build_lang_map_l_v(type_l, v))
            elif "@typeLabel" in value:
                type_l = l + ": " + value.get("@typeLabel")
                v = value.get("ns2:" + f.split(".")[-1]) or value.get("#text")
                metadata.append(self._build_lang_map_l_v(type_l, v))
            elif value:
                metadata.append(self._build_lang_map_l_v(l, value))

        return metadata

    @classmethod
    def _recurse_dict(cls, page, paths):
        path = paths.pop(0)
        page_obj = page.get(path, {})
        if paths:
            return cls._recurse_dict(page_obj, paths)
        else:
            return page_obj

    def _get_attribution(self, page: dict) -> dict:
        if not isinstance(page["rights"], list):
            page["rights"]= [page["rights"]]
        memobase_usage = [i["ns2:rights"] for i in page["rights"] if i.get("@typeLabel") == "Usage"]
        return {"requiredStatement": self._build_lang_map_l_v("Usage",memobase_usage[0] if memobase_usage else "")}

    def _get_new_canvas(self, page: dict) -> dict:
        """
        to be overwritten: return a fresh canvas for a page (already containing metadata)
        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        new_canvas = deepcopy(self.iiif_canvas)

        new_canvas.update(self._build_lang_map_l(page["title"]["ns2:title"]))
        new_canvas.update(self._get_attribution(page))
        new_canvas["metadata"] = self._get_nested_es_metadata(page)
        return new_canvas

    def _get_new_manifest(self, page_metadata, label):
        """to be overwritten: return a fresh manifest (already containing metadata)

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """

        new_manifest = deepcopy(self.iiif_manifest)
        new_manifest.update(self._get_attribution(page_metadata))
        new_manifest.update(self._build_lang_map_l(page_metadata["title"]["ns2:title"]))
        return new_manifest