from copy import deepcopy
from urllib import parse

from es_iiif_service import IIIFCollection, IIIFManifest, ESAggTemplates

class IIIFAllData:
    """
    functions which can be used by IIIFImpressoManifest and IIIFImpressoCollection
    """

    def _build_manifest_metadata(self, iiif_dict: dict, page: dict) -> None:
        """add metadata section to a iiif manifest

        :param iiif_dict: iiif template object for manifest
        :param page: metadata for a page
        """
        page_copy = deepcopy(page)

        del page_copy[self._canvas_id_field]
        iiif_dict["metadata"] = []
        print(page_copy)
        for key, value in page_copy.items():
            if isinstance(value, str) and len (value ) > 300:
                value = value[:300] + " ..."
            elif isinstance(value, list) and len (value ) > 30:
                value = "; ".join(value[:30]) + " ..."
            iiif_dict["metadata"].append({"label": key, "value": value})

        iiif_dict["label"] = page_copy.get(self._manifest_label_field,"")

        return iiif_dict

    def _build_manifest_url(self, id_: str, page: dict = {}, flex=False) -> str:
        """ build url for manifest

        :param id_: id for manifest
        :param page: example page for which manifest is built
        :return: return url for manifest
        """
        if page.get("list") and "vlm" in page.get("list"):
            manif_url = page.get("files")
        else:
            manif_url = self.iiif_service_host + self.project + "/" + self.url_encode4es(str(id_)) + "/manifest/"

        return manif_url

class IIIFAllDataCollection(IIIFAllData, IIIFCollection):
    iiif_top_collection = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "@id": "http://digispace",
        "label": "Not for public use",
        "description": "SHowing all ES metadata",
        "@type": "sc:Collection",
        "collections": [],
    }

    def __init__(self, **kwargs):
        """build Collection for Impresso project"""
        super(IIIFAllDataCollection, self).__init__(**kwargs)


    def _get_new_manifest(self, page: dict, label="") -> dict:
        """overwritten: return a fresh manifest reference to a collection

        :param page: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        # todo erweitern um vlm manifeste: if list manifest add value from files
        # vielleicht version und band in manifest verlagern?
        new_manifest = deepcopy(self.iiif_collection_manifest)

        new_manifest["label"] = page.get(self._manifest_label_field, "")
        return new_manifest




class IIIFAllDataManifest(IIIFAllData, IIIFManifest):

    def __init__(self, **kwargs):
        """build Manifest for Impresso project"""
        super(IIIFAllDataManifest, self).__init__(**kwargs)


    def _get_new_manifest(self, page_metadata: dict, label:str="") -> dict:
        """return a fresh manifest for a page (already containing metadata)

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_manifest)
        if self.project == "portraets2":
            new_manifest["viewingHint"] = "continuous"
            new_manifest["viewingDirection"] = "left-to-right"
        self._build_manifest_metadata(new_manifest, page_metadata)
        return new_manifest

    def _get_new_canvas(self, page: dict) -> dict:
        """
        to be overwritten: return a fresh canvas for a page (already containing metadata)
        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        new_canvas = deepcopy(self.iiif_canvas)

        filename = page.get(self._canvas_id_field,"")
        try:
            label = "_".join([str(page.get("version","")), str(page.get("band","")), str(filename.split("%2F")[-1])])
        except (KeyError, AttributeError):
            label = "tbd"
        new_canvas["metadata"] = []
        new_canvas["metadata"].append({"label": "filename", "value": filename})
        new_canvas["metadata"].append({"label": "label", "value": label})
        new_canvas["label"] = page.get(self._manifest_label_field,"")
        return new_canvas

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        if "mets_iiifpages" in page and page["mets_iiifpages"]:
            if isinstance(page["mets_iiifpages"], list):
                iiif_image = page["mets_iiifpages"][0]["url"]
            else:
                iiif_image = page["mets_iiifpages"]["url"]
            return iiif_image
        elif self.project == "digispace_resolved":
            return self.imageserver_host + page[self._canvas_id_field].split("%2F")[-1]
        else:
            return self.imageserver_host + page[self._canvas_id_field]