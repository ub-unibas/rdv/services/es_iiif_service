from copy import deepcopy
import json

from es_iiif_service import IIIFCollection, IIIFManifest, ViewGenerator


class IIIFZettelkatalog:
    """
    functions which can be used by IIIFImpressoManifest and IIIFImpressoCollection
    """

    def _build_manifest_metadata(self, iiif_dict: dict, page: dict) -> None:
        """add metadata section to a iiif manifest

        :param iiif_dict: iiif template object for manifest
        :param page: metadata for a page
        """
        page_copy = deepcopy(page)
        del page_copy[self._canvas_id_field]
        cat_lookup = {"ak1": "Autoren- und Titelkatalog bis 1939", "ak2": "Dissertationenkatalog bis 1980",
                      "ak3": "Verwaltungspublikationen von Universitäten bis 1980",
                      "ak4": "Autoren- und Titelkatalog bis 1939 (nur Bilder)"}
        cat = page.get("catalog")
        label =  page_copy.get(self._manifest_label_field, "")
        iiif_dict["label"] = cat_lookup[cat] + ": " +label
        iiif_dict["metadata"] = []
        iiif_dict["metadata"].append({"label": "Katalog", "value": cat_lookup[cat]})
        precursor = page.get("precursor")
        if precursor:
            iiif_dict["metadata"].append({"label": "vorheriger Eintrag", "value":
                "<a target=\"_self\" href=\"https://ub-universalviewer.ub.unibas.ch/uv/uv.html#?manifest="
                + self.s.base_url + "{}/{}/manifest/\">{}</a>".format(cat, precursor, precursor)})
        iiif_dict["metadata"].append({"label": "Eintrag", "value": label})
        successor =page.get("successor")
        if successor:
            iiif_dict["metadata"].append({"label": "folgender Eintrag", "value":
                "<a target=\"_self\" href=\"https://ub-universalviewer.ub.unibas.ch/uv/uv.html#?manifest="
                + self.s.base_url + "{}/{}/manifest/\">{}</a>".format(cat, successor, successor)})

        return iiif_dict

    def _get_new_manifest(self, page_metadata, label):
        """to be overwritten: return a fresh manifest (already containing metadata)

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """

        new_manifest = deepcopy(self.iiif_manifest)
        new_manifest["label"] = label
        return new_manifest


class IIIFZettelkatalogCollection(IIIFZettelkatalog, IIIFCollection):
    iiif_top_collection = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "@id": "http://ub-zettelkatalog",
        "label": "Zettelkatalog",
        "description": "Zettelkatalog",
        "@type": "sc:Collection",
        "collections": [],
    }

    def __init__(self, **kwargs):
        """build Collection for Impresso project"""
        super(IIIFZettelkatalogCollection, self).__init__(**kwargs)


class IIIFZettelkatalogManifest(IIIFZettelkatalog, IIIFManifest):
    iiif_manifest = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "@id": "",
        "@type": "sc:Manifest",
        "label": "",
        "navDate": "",
        "license": "",
        "attribution": "",
        "sequences": [],
        "logo": "https://www.unibas.ch/dam/jcr:93abfa0e-58d6-45ed-930a-de414ca40b13/uni-basel-logo.svg"
    }

    def __init__(self, **kwargs):
        """build Manifest for Impresso project"""
        super(IIIFZettelkatalogManifest, self).__init__(**kwargs)

    def _get_new_manifest(self, page_metadata: dict, label: str = "") -> dict:
        """return a fresh manifest for a page (already containing metadata)

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_manifest)

        self._build_manifest_metadata(new_manifest, page_metadata)
        return new_manifest

    def _get_new_canvas(self, page: dict) -> dict:
        """
        to be overwritten: return a fresh canvas for a page (already containing metadata)
        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        new_canvas = deepcopy(self.iiif_canvas)
        filename = page.get(self._canvas_id_field, "")

        new_canvas["metadata"] = []
        page_id = int(filename.split("-")[-1].split(".")[0])
        label = "Seite {}".format(page_id)
        # new_canvas["metadata"].append({"label": "filename", "value": filename})
        # TODO: anpassen
        new_canvas["metadata"].append({"label": "Bestelllink", "value":
            "<a href=\"https://ub-test-webform.ub.unibas.ch/form_zettelkatalog/header?bildid={}\">Bestellen</a>".format(
                page_id)})

        return new_canvas

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        return self.imageserver_host + page[self._canvas_id_field]


class IIIFZettelkatalogIDManifest(IIIFZettelkatalogManifest):

    def get_page_range(self):
        range = 10
        manifest_id = self._manifest_ids[0]
        manif_ids = [int(manifest_id) - range, int(manifest_id), int(manifest_id) + range]
        query_string = " OR ".join(["{:08d}".format(x) for x in manif_ids])
        term_query = ["{:08d}".format(x) for x in manif_ids]
        return term_query

    def _get_manifest_query(self, limit: int, terms_manifest_id_field: str):
        # https://127.0.0.1:5001/ak1_number/19534/manifest/&cv=9
        term_query = self.get_page_range()
        manifest_query = {
            "size": limit,
            "query": {
                "bool": {
                    "must": [
                                {"exists": {"field": self._canvas_id_field}},
                                {"terms": {"file_ids": term_query}}

                            ] + self._get_col_filters4es()
                }

            }
        }
        return manifest_query

    def _build_canvas_structure(self, results):
        pages = []
        for result in results["hits"]["hits"]:

            # add painless script fields
            result["_source"].update(result.get("fields", {}))
            source = json.dumps(result["_source"])
            source_b = source.encode('utf-8')
            source_dict = json.loads(source_b.decode())
            # if pages are stored as a list within one ES entry, create a copy of all metadata for each page
            if isinstance(source_dict.get(self._canvas_id_field, ""), list):
                term_query = self.get_page_range()
                try:
                    start_i = source_dict["file_ids"].index(term_query[0])
                except ValueError:
                    import traceback
                    traceback.print_exc()
                    start_i = 0
                try:
                    stop_i = source_dict["file_ids"].index(term_query[-1])
                except ValueError:
                    import traceback
                    traceback.print_exc()
                    stop_i = len(source_dict["file_ids"]) - 1

                for n, value in enumerate(source_dict[self._canvas_id_field][start_i:stop_i]):
                    source_copy = deepcopy(source_dict)
                    source_copy[self._canvas_id_field] = value
                    # get same position from width and height array for a specific image
                    try:
                        source_copy[self._image_width_field] = source_dict[self._image_width_field][n]
                        source_copy[self._image_height_field] = source_dict[self._image_height_field][
                            n]
                    except KeyError:
                        pass
                    pages.append(source_copy)
            else:
                pages.append(source_dict)
        return pages

    def return_result(self) -> str:
        manifest_id = self._manifest_ids[0]
        filtered_results = deepcopy(self.result)
        filtered_results["sequences"][0]["canvases"] = []
        for k in self.result["sequences"][0]["canvases"]:
            img_id = k["@id"].split("-")[-1][:8]
            k["label"] = img_id
            if int(img_id) > int(manifest_id) - 10 and int(img_id) < int(manifest_id) + 10:
                filtered_results["sequences"][0]["canvases"].append(k)

        start_canvas = [u["@id"] for u in filtered_results["sequences"][0]["canvases"] if
                u["@id"].endswith("0{}.TIF".format(int(manifest_id)))][0]
        filtered_results["sequences"][0]["startCanvas"] = start_canvas

        return json.dumps(filtered_results)