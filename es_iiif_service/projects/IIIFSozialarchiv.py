from copy import deepcopy
from urllib import parse

from es_iiif_service import IIIFCollection, IIIFManifest, ESAggTemplates


class IIIFSozialarchivCollection(IIIFCollection):
    iiif_top_collection = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "@id": "http://unibas_newspapers",
        "label": "Sozialarchiv",
        "description": "Sozialarchiv Hack",
        "@type": "sc:Collection",
        "collections": [],
    }

    def __init__(self, **kwargs):
        """build Collection for Impresso project"""
        super(IIIFSozialarchivCollection, self).__init__(**kwargs)

    def _get_agg_field_type(self, field: str) -> dict:
        """
        return ES search snippet how aggregation for special fields shall be build
        (e.g. for date: aggregation for every year)

        :param field: field name in es
        :return: dict containing ES search snippet used for building aggregation
        """
        collection_es_func = {}
        return collection_es_func.get(field, {})


class IIIFSozialarchivManifest(IIIFManifest):

    def __init__(self, **kwargs):
        """build Manifest for Impresso project"""
        super(IIIFSozialarchivManifest, self).__init__(**kwargs)

    def _get_new_canvas(self, page: dict) -> dict:
        """
        to be overwritten: return a fresh canvas for a page (already containing metadata)
        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        new_canvas = deepcopy(self.iiif_canvas)
        new_canvas["metadata"] = []

        for label in []:
            if label in page and page[label]:
                new_canvas["metadata"].append({"label": label, "value": page[label]})

        #new_canvas["label"] = "Seite " + str(page["page"])
        return new_canvas
