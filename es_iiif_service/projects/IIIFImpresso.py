from copy import deepcopy
from urllib import parse
import json

from es_iiif_service import IIIFCollection, IIIFManifest, ESAggTemplates, ViewGenerator

class IIIFImpresso:
    """
    functions which can be used by IIIFImpressoManifest and IIIFImpressoCollection
    """

    def _build_manifest_metadata(self, iiif_dict: dict, page: dict) -> None:
        """add metadata section to a iiif manifest

        :param iiif_dict: iiif template object for manifest
        :param page: metadata for a page
        """

        if self.search_marker:
            iiif_dict["label"] = "Suche: {}".format(self._manifest_ids[0][2:])
        else:
            view_object = ViewGenerator(self.s)
            newspaper_date_field = "date"
            newspaper_issue_field = "issue_number"
            sys_id = str(page["goobi_name"].split("_")[1])
            # sys_id = page["sys_number"]
            metadata = self.get_metadata(sys_id)
            iiif_dict.setdefault("metadata", [])
            for key, value in metadata.items():
                iiif_dict["metadata"].append({"label": key, "value": value})
            #iiif_dict["metadata"].extend(view_object.get_iiif_view(page, institution="", lang="de",inst_spreadsheet_id=self.s.project["proj_sheet"]))
            date = "/".join(page.get(newspaper_date_field, ""))
            number = page.get(newspaper_issue_field, "")
            issue_def = " ".join([date, str(number)])
            if self._flex:
                iiif_dict["label"] = "IIIF Presentation API 2.1 (Beta) Newspapers"
            else:
                iiif_dict["label"] = metadata.get("Titel", "Kein Titel ") + " " + issue_def
            iiif_dict["description"] = "Description: " + metadata.get("Titel", "Kein Titel ") + " " + issue_def

            if newspaper_date_field in page:
                iiif_dict["navDate"] = page[newspaper_date_field][0]
        return iiif_dict


class IIIFImpressoCollection(IIIFCollection,IIIFImpresso):
    iiif_top_collection = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "@id": "http://unibas_newspapers",
        "label": "Impresso",
        "description": "Newspapers from UB Basel for Impresso",
        "@type": "sc:Collection",
        "collections": [],
    }

    def __init__(self, **kwargs):
        """build Collection for Impresso project"""
        super(IIIFImpressoCollection, self).__init__(**kwargs)



    def _get_new_manifest(self, page: dict, label="") -> dict:
        """overwritten: return a fresh manifest reference to a collection

        :param page: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_collection_manifest)
        self._build_manifest_metadata(new_manifest, page)
        return new_manifest

    def _get_agg_field_type(self, field: str) -> dict:
        """
        return ES search snippet how aggregation for special fields shall be build
        (e.g. for date: aggregation for every year)

        :param field: field name in es
        :return: dict containing ES search snippet used for building aggregation
        """
        collection_es_func = {"year": ESAggTemplates.number_es_field,
                              "month": ESAggTemplates.number_es_field,
                              "day": ESAggTemplates.number_es_field}
        return collection_es_func.get(field, {})


class IIIFImpressoManifest(IIIFManifest,IIIFImpresso):

    def __init__(self, **kwargs):
        """build Manifest for Impresso project"""
        super(IIIFImpressoManifest, self).__init__(**kwargs)

    def _get_new_manifest(self, page_metadata: dict, label:str="") -> dict:
        """return a fresh manifest for a page (already containing metadata)

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_manifest)
        self._build_manifest_metadata(new_manifest, page_metadata)
        return new_manifest

    def _build_canvas_structure(self, results):
        pages = []
        for result in results["hits"]["hits"]:

            # add painless script fields
            result["_source"].update(result.get("fields", {}))
            source = json.dumps(result["_source"])
            source_b = source.encode('utf-8')
            source_dict = json.loads(source_b.decode())
            # if pages are stored as a list within one ES entry, create a copy of all metadata for each page
            if isinstance(source_dict.get("pages", ""), list):

                for n, value in enumerate(source_dict["pages"]):
                    source_copy = deepcopy(source_dict)
                    source_copy.update(value)
                    source_copy["pages"] = source_copy["identifier"]
                    pages.append(source_copy)
            else:
                pages.append(source_dict)
        return pages

    def _get_new_canvas(self, page: dict) -> dict:
        """
        to be overwritten: return a fresh canvas for a page (already containing metadata)
        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        new_canvas = deepcopy(self.iiif_canvas)
        new_canvas["metadata"] = []
        impresso_id = "identifier_impresso"
        if self.search_marker:
            view_object = ViewGenerator(self.s)
            new_canvas["metadata"].extend(view_object.get_iiif_view(page, institution="", lang="de",
                                                                   inst_spreadsheet_id=self.s.project["proj_sheet"]))
        else:
            for label in [impresso_id]:
                if label in page and page[label]:
                    new_canvas["metadata"].append({"label": label, "value": page[label]})
        new_canvas["seeAlso"] = []
        self._add_seealso(new_canvas, page)
        if self._flex:
            newspaper_date_field = "date"
            newspaper_issue_field = "issue_number"
            date = "/".join(page.get(newspaper_date_field, ""))
            number = page.get(newspaper_issue_field, "")
            issue_def = " ".join([date, str(number)])

            new_canvas["label"] = " ".join([page.get("newspaper", ""), issue_def])
        else:
            new_canvas["label"] = "Seite " + str(page["page"])
        return new_canvas

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        # todo: anpassen
        if self.project == "avis_ocr" or self.project == "avis_ocr2":
            return self.imageserver_host + page[self._canvas_id_field] + ".tif"
        if self.project == "ub-newspapers":

            if page.get("newspaper") in ["arbeitgeber", "handelsztg", "wirtsozpol"]:
                return "https://ub-sipi.ub.unibas.ch/impresso/" + page[self._canvas_id_field]
            else:
                if page.get("newspaper") in ["chrivoaub", "wandindes"]:
                    sipi_project = "volksbote"
                elif page.get("newspaper") in ["avis", "avisba", "hochprdo", "wochnaaud"]:
                    sipi_project = "avis-blatt"
                else:
                    sipi_project = page.get("newspaper")
                return self.imageserver_host + sipi_project + "/" + page[self._canvas_id_field] + ".tif"

        else:
            return self.imageserver_host + page[self._canvas_id_field] # + ".tif"

    def _add_seealso(self, new_canvas: dict, page: dict) -> None:
        """
        update iiif Canvas with links to seeAlso/Rendering Representations
        :param new_canvas: dict containing iiif canvas
        :param page: dict containing all metadata from ES for a page
        """

        new_canvas["seeAlso"].extend(
            [{"label": "Alto-XML", "@id": self.seealso_host + xml, "format": "text/xml"} for xml in
             page.get("xml_files") if page.get("xml_files")])

        new_canvas["rendering"] = new_canvas["seeAlso"]
