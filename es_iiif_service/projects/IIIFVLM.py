from copy import deepcopy

from es_iiif_service import IIIFCollection, IIIFManifest, ViewGenerator

class IIIFVLM:
    """
    functions which can be used by IIIFImpressoManifest and IIIFImpressoCollection
    """


    def _get_new_manifest(self, page_metadata, label):
        """to be overwritten: return a fresh manifest (already containing metadata)

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """

        new_manifest = deepcopy(self.iiif_manifest)
        if self.project != "afrikaportal":
            new_manifest["label"] = label
        else:
            new_manifest["label"] = page_metadata.get(self._manifest_label_field,"")
        return new_manifest

class IIIFVLMCollection(IIIFVLM, IIIFCollection):
    iiif_top_collection = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "@id": "http://afrikaportal",
        "label": "Afrikaportal",
        "description": "Afrikaportal",
        "@type": "sc:Collection",
        "collections": [],
    }

    def __init__(self, **kwargs):
        """build Collection for Impresso project"""
        super(IIIFVLMCollection, self).__init__(**kwargs)


class IIIFVLMManifest(IIIFVLM, IIIFManifest):

    def __init__(self, **kwargs):
        """build Manifest for Impresso project"""
        super(IIIFVLMManifest, self).__init__(**kwargs)

    def _get_new_canvas(self, page: dict) -> dict:
        """
        to be overwritten: return a fresh canvas for a page (already containing metadata)
        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        new_canvas = deepcopy(self.iiif_canvas)
        institution = page.get("main_institution",[""])[0]
        metadata = []
        new_canvas["metadata"] = metadata
        aleph_link = "<a href=\"https://aleph.unibas.ch/F/?local_base=DSV0{}&con_lng=GER&func=find-b&find_code=SYS&request={}\">Aleph Link</a>".format(page.get("dsv"), page.get("sys"))

        metadata.append({"label": "Titel", "value": page.get("cat_title", ["Unbekannt"])})
        metadata.append({"label": "Signatur", "value": page.get("cat_signatur", ["Unbekannt"])})
        metadata.append({"label": "Ort", "value": "; ".join(page.get("cat_place", ["Unbekannt"]))})
        metadata.append({"label": "Massstab", "value": "; ".join(page.get("cat_scale", ["Unbekannt"]))})
        metadata.append({"label": "Erscheinungsjahr", "value": page.get("cat_year", None)})
        metadata.append({"label": "e-rara Link" if page.get("dsv") == 1 else "e-manuscripta Link", "value": "<a href=\"{}\">Link</a>".format(page.get("cat_url",[])[0])})
        metadata.append({"label": "Aleph Link", "value": aleph_link})

        new_canvas["label"] = page.get("cat_title",[""])[0]

        return new_canvas

    def get_image_dimensions(self, page, page_id, image_dimensions_db={}) -> dict:

        return {"width": page["width"], "height": page["height"]}

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        vlm_id = page[self._canvas_id_field].get("url")
        return vlm_id

    def _get_page_id(self, page: dict) -> str:
        vlm_id = page[self._canvas_id_field].get("url").split("/")[-1]
        return vlm_id