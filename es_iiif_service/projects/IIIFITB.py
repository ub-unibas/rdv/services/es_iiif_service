from es_iiif_service.projects.IIIFBaseProj import IIIFBaseProjManifest_v3, IIIFBaseProjManifest

class IIIFITBManifest_v3(IIIFBaseProjManifest_v3):
    proj_label = "Druckermarken ITB Universitätsbibliothek Basel"
    config_store_id = "itb_int"
    continous_ini = "itb_druckermarken2"

    def _get_resource_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        if page[self._canvas_id_field].get("url","").startswith("http"):
            url = page[self._canvas_id_field].get("url")
            url.replace("https://tools.wmflabs.org/zoomviewer/proxy.php?", "https://zoomviewer.toolforge.org/proxy.php?")
            return url
        else:
            return self.imageserver_host + page[self._canvas_id_field].get("url")

    def _get_image_dimensions(self, page):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._canvas_id_field][self._image_width_field]
            height = page[self._canvas_id_field][self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            pass
        return image_dimensions

    def get_annotations(self, page):
        page_id = self._get_page_id(page)
        annotation = [
            {
                "id": f"{self._build_iiif_id('annlist', page_id)}_2",
                "type": "AnnotationPage",
                "items": [
                    {
                        "id": "https://iiif.io/api/cookbook/recipe/0021-tagging/annotation/p0002-tag",
                        "type": "Annotation",
                        "motivation": "tagging",
                        "body": {
                            "type": "TextualBody",
                            "value": "Druckermarke",
                            "language": "de",
                            "format": "text/plain"
                        },
                        "target": f"{self._build_iiif_id('canvas', page_id)}#xywh={self.calculate_xywh(page)}"
                    }
                ]
            }
        ]
        return annotation

class IIIFITBManifest(IIIFBaseProjManifest):
    proj_label = "Druckermarken ITB Universitätsbibliothek Basel"
    config_store_id = "itb_int"
    continous_ini = "itb_druckermarken2"

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        if page[self._canvas_id_field].get("url","").startswith("http"):
            return page[self._canvas_id_field].get("url")
        else:
            return self.imageserver_host + page[self._canvas_id_field].get("url")

    def _get_image_dimensions(self, page, page_id, image_dimensions_db={}):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._canvas_id_field][self._image_width_field]
            height = page[self._canvas_id_field][self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            pass
        return image_dimensions
