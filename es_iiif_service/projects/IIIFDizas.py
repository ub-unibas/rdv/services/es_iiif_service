from copy import deepcopy
import requests
from urllib import parse
from collections import OrderedDict

from es_iiif_service import IIIFCollection, IIIFManifest, ESAggTemplates, ViewGenerator


class IIIFDizasCollection(IIIFCollection):
    iiif_top_collection = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "@id": "http://dizas",
        "label": "DIZAS",
        "description": "Ausschnittesammlung DIZAS",
        "@type": "sc:Collection",
        "collections": [],
    }

    def __init__(self, **kwargs):
        """build Collection for DIZAS project"""
        super(IIIFDizasCollection, self).__init__(**kwargs)

    def _get_new_manifest(self, page: dict, label: str) -> dict:
        """to be overwritten: return a fresh manifest reference to a collection

        :param page: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_collection_manifest)
        newspaper_descriptor = "title"
        if self._manifest_filter:
            new_manifest["label"] = "Sammelmappe: " + " | ".join([str(p[newspaper_descriptor]) for p in page])
        else:
            new_manifest["label"] = self._build_full_manifest_label(page, label)
        return new_manifest

    def _get_agg_field_type(self, field: str) -> dict:
        """
        return ES search snippet how aggregation for special fields shall be build
        (e.g. for date: aggregation for every year)

        :param field: field name in es
        :return: dict containing ES search snippet used for building aggregation
        """
        collection_es_func = {"year": ESAggTemplates.number_es_field}
        return collection_es_func.get(field, {})



class IIIFDizasManifest(IIIFManifest):

    def __init__(self, **kwargs):
        """build Manifest for DIZAS project"""
        super(IIIFDizasManifest, self).__init__(**kwargs)
        self.agg_condition = {"aggs": {"filtered_Quelldatum": {
            "date_histogram": {"field": "Quelldatum", "interval": "year", "order": {"_count": "desc"}}}}}

    def _get_manifest_query(self, limit, terms_manifest_id_field):
        """ to be overwritten, define query to get all data for a manifest

        :param limit: max number of pages to be returned
        :param terms_manifest_id_field: es field name to be used to limit query to a certain manifest
        :return: es query dict
        """

        """             "script_fields": {
                "year_month": {
                    "script": {
                        "lang": "painless",
                        "source": "doc['Quelldatum'].value.toString('yyyy-MM')"
                    }
                }
            },"""
        if self.search_marker:
            query_term = self._manifest_ids[0][2:]
            manifest_query = {
                "size": limit,
                # todo: aus ini auslesen
                "_source": {"excludes": ["*fulltext", "textcoords*"]},
                "query": {
                    "bool": {
                        "must": [
                                    {"exists": {"field": self._canvas_id_field}},
                                    {"match": {self.s.mapping_fulltext["fulltext"]: query_term}}

                                ] + self._get_col_filters4es()
                    }

                }
            }
        else:
            manifest_query = {
                "size": limit,
                "_source": {"excludes": [ "*fulltext", "textcoords*" ]},
                "query": {
                    "bool": {
                        "must": [
                            {"exists": {"field": self._canvas_id_field}},
                            {"terms": {terms_manifest_id_field: self._manifest_ids}}
                        ]+ self._get_col_filters4es()
                    }

                }
            }

        return  manifest_query

    def _get_new_manifest(self, page_metadata: dict, label:str="Sammelmappe DIZAS") -> dict:
        """return a fresh manifest for a page (already containing metadata)
        if multiple manifests are merged to one manifest no AlephLink and Schlagwort is added on toplevel

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_manifest)
        #labels that shall be added to metadata section at manifest label
        label_dict = OrderedDict([("Aleph Link", "Dokumentensammlung SWA"),("title", "Dossiername"),
                                  ("descr_fuv", "Firmen und Verbände"), ("descr_person", "Personen"), ("descr_sach", "Sachdeskriptor")])

        new_manifest["metadata"] = []
        new_manifest["attribution"] = "Archiviert und zur Verfügung gestellt vom Schweizerischen Wirtschaftsarchiv SWA"
        new_manifest["license"] = "http://ezas.wirtschaftsarchiv.ch/swa/aboutezas.html"
        new_manifest["logo"] = {
              "@id" : "https://ub-sipi.ub.unibas.ch/logos/logo-swa-mit-farbe.tif/full/200,/0/default.jpg",
              "service" : {
                "@context" : "http://iiif.io/api/image/2/context.json",
                "@id" : "http://example.org/image-service/logo",
                "profile" : "http://iiif.io/api/image/2/level2.json"
              }
        }
        if self.project in ["zas_flex_manifest2", "authzas_flex_manifest2"]:
            new_manifest["viewingHint"] = "continuous"
            new_manifest["viewingDirection"] = "left-to-right"

        if self.multiple_ids_marker or self._flex:
            #new_manifest["label"] = label
            new_manifest["label"] = {"de": ["IIIF Presentation API 2.1 (Beta) ZAS Portal"], "en": ["IIIF Presentation API 2.1 (Beta) ZAS"]}
            new_manifest["description"] = {"de": ["Sie nutzen die IIIF Presentation API des ZAS Portals. Wir befinden uns noch im Beta-Betrieb. "
                                                 "Nutzen Sie die Manifeste in anderen IIIF-Applikationen, aber Achtung diese sind noch nicht zu 100% stabil."],
                                           "en": ["You are using the IIIF Presentaton API (Beta) from ZAS Portal. "
                                                 "We are still in beta. Reuse the manifest in other IIIF Manifests but they are not 100% stable."]}
        else:
            label = page_metadata.get(self._manifest_label_field, "Kein Titel")
            # to get label from id-label fields
            if label and isinstance(label, list) and isinstance(label[0], dict):
                label= label[0].get("label", label)
            for field, label_value in label_dict.items():
                if field in page_metadata and page_metadata[field]:
                    if field == "Aleph Link":
                        page_metadata[field] = '<a href="' + page_metadata[field] + '">Aleph Link</a>'
                    if isinstance(page_metadata[field], list):
                        # muss gleich sein wie metadaten generierung im ViewGenerator damit nachher gleiche Werte auf tieferer Ebene ausgeschlossen werden

                        value = ", ".join([p for p in page_metadata[field] if isinstance(p, str)])
                        if not value:
                            value = ", ".join([p.get("label") for p in page_metadata[field] if isinstance(p, dict) and p.get("label")])
                    else:
                        value = page_metadata[field]
                    new_manifest["metadata"].append({"label": label_value, "value": value})
            new_manifest["label"] = self._build_full_manifest_label(page_metadata, label)
            # is already included: new_manifest["description"] = "Description: Dossier " + self._build_full_manifest_label(page_metadata, label)
        return new_manifest

    def _get_new_canvas(self, page: dict) -> dict:
        """
        return a fresh canvas for a page (already containing metadata)
        if multiple manifests are merged to one manifest  AlephLink and Schlagwort are added to canvas metadata

        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        # doppelte Seiten: http://ub-universalviewer.ub.unibas.ch/uv/uv.html#?manifest=http://ub-test-iiifpresentation.ub.unibas.ch/dizas/collection
        new_canvas = deepcopy(self.iiif_canvas)
        # todo: anpassen für ezas
        newspaper_field = "Quelle"  # name of newspaper
        try:
            newspaper = self.tostr(page[newspaper_field])
        except KeyError:
            newspaper = self.tostr(page.get("Quellname","Unbekannt"))
        newspaper_date_field = "Quelldatum"

        metadata_fields = []
        # kann auch Array sein
        date = self.tostr(page.get(newspaper_date_field,""))
        # BAU_1_003888026_44_1.j2k = 1
        try:
            page_number = str(int(page[self.s.mapping_manifest["page_id"]].split("_")[4].split(".j2k")[0]))
            page_counter = page["pages"]
            page_label = " {}/{}".format(page_number, page_counter)
        except (IndexError, AttributeError):
            # BAU_1_003888026_44.j2k
            page_label = ""
        new_canvas["label"] = date + " " + newspaper + page_label
        new_canvas["navDate"] = date

        if self.multiple_ids_marker:
            newspaper_descriptor = "title"
            aleph_link = "Aleph Link"
            descriptor = self.tostr(page[newspaper_descriptor])
            additional_metadata_fields = [newspaper_descriptor, aleph_link]
            metadata_fields.extend(additional_metadata_fields)
            new_canvas["label"] = descriptor + " " + date + " " + newspaper + page_label

        view_object = ViewGenerator(self.s)
        from rdv_querybuilder_zas_ubit.zas.view import ZasView
        from rdv_config_store_ubit import ConfigStore
        pj_conf = ConfigStore()
        pj_conf.load_project_config("zas-prod")
        view_object = ZasView(page_id = page.get("id"), page = page, lang = "de", project_config_store=pj_conf)

        # todo: projekt dizas variabel gestalten
        if self.project == "ezas_object_sb":
            inst_kuerzel = "ezas"
        elif self.project == "dizas_object_sb":
            inst_kuerzel = "dizas"
        else:
            inst_kuerzel = "ezas"

        new_canvas["metadata"] = view_object.get_label_value_metadata(dict_type="iiif_v2",
                                                           exclude_fields=["Digitalisat Artikel", "Digitalisate Dossier", "Dokumentensammlung"])

        if self.multiple_ids_marker:
            for label in metadata_fields:
                if label in page and page[label] and page[label] != "None":
                    if label == "Aleph Link":
                        page[label] = '<a href="' + page[label] + '">Aleph Link</a>'
                    new_canvas["metadata"].append({"label": label, "value": page[label]})

        self._add_seealso(new_canvas, page)

        return new_canvas

    def _add_seealso(self, new_canvas: dict, page: dict) -> None:
        """
        update iiif Canvas with links to seeAlso/Rendering Representations
        :param new_canvas: dict containing iiif canvas
        :param page: dict containing all metadata from ES for a page
        """
        """new_canvas["seeAlso"] = [
            {"label": pdf, "@id": self._get_seealso_path(page, "pdf") + "/" + pdf, "format": "application/pdf"} for pdf
            in page.get("pdf_files",[])]
        new_canvas["seeAlso"].extend(
            [{"label": txt, "@id": self._get_seealso_path(page, "txt") + "/" + txt, "format": "text/txt"} for txt in
             page.get("txt_files",[])])
        new_canvas["seeAlso"].extend(
            [{"label": xml, "@id": self._get_seealso_path(page, "xml") + "/" + xml, "format": "text/xml"} for xml in
             page.get("xml_files",[]) if not xml.endswith("_meta.xml")])"""
        new_canvas["seeAlso"]= []
        new_canvas["seeAlso"].extend(
            [{"label": "ALTo", "id": self._get_seealso_path(page, "xml") + "/" + xml,
              "format": "application/xml", "profile": "http://www.loc.gov/standards/alto/ns-v3#"} for xml in
             page.get("xml_files",[]) if xml.endswith("_Alto.xml") ])
        new_canvas["rendering"] = new_canvas["seeAlso"]

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        if self.project in (["zas_flex_manifest", "zas_flex_manifest2", "zas_flex_manifest_v3", "zas_dossiers_object_sb"] + \
                ["authzas_flex_manifest", "authzas_flex_manifest2", "authzas_flex_manifest_v3", "authzas_dossiers_object_sb"]):
            # dizas / ezas for resolving path
            subset = page["proj_id"]
            try:
                return self.imageserver_host  + subset + "/" + page[self.s.mapping_manifest["page_id"]].get("id")
            except AttributeError:
                return self.imageserver_host  + subset + "/" + page[self.s.mapping_manifest["page_id"]]
        else:
            return self.imageserver_host + page[self.s.mapping_manifest["page_id"]]
            # parse.quote_plus("BAU_1_" + page["Systemnummer"] + "/" + page["Artikel_ID"] + "/j2k/" + page["jp2_files"])

    def _get_seealso_path(self, page: dict, type_: str) -> str:
        """ build see also path

        :param page: dict containing all metadata from ES for a page
        :param type_: type of seeAlso Representation
        :return: URL for seeAlso Representation
        """

        subset = page["proj_id"]
        return self.seealso_host + subset + "/"
        return self.seealso_host

    def build_same_size_buckets(self, agg_results):
        buckets = []
        aggr_counter = 0
        agg_start = ""
        agg_end = ""
        for b in sorted(agg_results["aggregations"]["filtered_Quelldatum"]["buckets"],
                        key=lambda x: x["key_as_string"]):
            # todo: wert anpassen, max ist 9999 durch suche
            if aggr_counter < 500 and aggr_counter > 0:
                aggr_counter += b["doc_count"]
                agg_end = b["key_as_string"]
            else:
                if agg_end:
                    buckets.append({"agg_start": agg_start, "agg_end": agg_end, "aggr": aggr_counter})
                agg_start = b["key_as_string"]
                aggr_counter = b["doc_count"]
        else:
            agg_end = b["key_as_string"]
            buckets.append({"agg_start": agg_start, "agg_end": agg_end, "aggr": aggr_counter})
        return buckets

    def _define_subcat_buckets(self, buckets, query):
        results = {"hits": {"hits": []}}
        for bucket in buckets:
            bucket_filtet = {"filter": {
                "bool": {"must": [{"range": {"Quelldatum": {"gte": bucket["agg_start"], "lte": bucket["agg_end"]}}}]}}}
            bucket_query = deepcopy(query)
            bucket_query["query"]["bool"].update(bucket_filtet)
            bucket_result = self.es.search(index=self.index, body=bucket_query)

            # todo: url anpassen
            # todo define: bucket_label, flex_manifest, agg_start, agg_end,

            store_url = self.iiif_service_host + "zas_flex_manifest/store_id/"
            store_id = requests.post(store_url, json=bucket_query, verify=False).json()["dyn_manif_id"]

            for result in bucket_result["hits"]["hits"]:
                result["_source"]["bucket_label"] = "{}/{}".format(bucket["agg_start"][:4], bucket["agg_end"][:4])
                result["_source"]["flex_manifest"] = store_id.strip()

            results["hits"]["hits"].extend(bucket_result["hits"]["hits"])
        return results

class IIIFDizasSplitManifest(IIIFManifest):

    def __init__(self, **kwargs):
        """build Manifest for DIZAS project"""
        super(IIIFDizasManifest, self).__init__(**kwargs)

        self._manifest_id = kwargs["manifest_ids"]
        self._manifest_ids = self.split_manifest_ids(kwargs["manifest_ids"])

