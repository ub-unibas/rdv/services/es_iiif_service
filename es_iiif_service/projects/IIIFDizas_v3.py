from copy import deepcopy
import requests
from urllib import parse
from collections import OrderedDict

from es_iiif_service import IIIFCollection, IIIFManifest, ESAggTemplates, ViewGenerator, IIIFCollection_v3, IIIFManifest_v3
from rdv_querybuilder_zas_ubit.zas.view import ZasView
from rdv_data_helpers_ubit.projects.zas.zas import ZAS_SOURCE_ESFIELD, ZAS_DATE_ESFIELD, ZAS_TITLE_ESFIELD
from rdv_data_helpers_ubit import IIIF_IMGS_ESFIELD

class IIIFDizasCollection_v3(IIIFCollection_v3):

    iiif_top_collection = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "id": "http://dizas",
        "label": IIIFCollection_v3._build_lang_map("DIZAS"),
        "summary": IIIFCollection_v3._build_lang_map("Ausschnittesammlung DIZAS"),
        "type": "Collection",
        "items": [],
    }

    def __init__(self, **kwargs):
        """build Collection for DIZAS project"""
        super(IIIFDizasCollection_v3, self).__init__(**kwargs)

    def _get_new_manifest(self, page: dict, label: str) -> dict:
        """to be overwritten: return a fresh manifest reference to a collection

        :param page: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_collection_manifest)
        newspaper_descriptor = "title"
        if self._manifest_filter:
            new_manifest.update(self._build_lang_map_l("Sammelmappe: " + " | ".join([str(p[newspaper_descriptor]) for p in page])))
        else:
            new_manifest.update(self._build_lang_map_l(self._build_full_manifest_label(page, label)))
        return new_manifest

    def _get_agg_field_type(self, field: str) -> dict:
        """
        return ES search snippet how aggregation for special fields shall be build
        (e.g. for date: aggregation for every year)

        :param field: field name in es
        :return: dict containing ES search snippet used for building aggregation
        """
        collection_es_func = {"year": ESAggTemplates.number_es_field}
        return collection_es_func.get(field, {})


class IIIFDizasManifest_v3(IIIFManifest_v3):

    def __init__(self, **kwargs):
        """build Manifest for DIZAS project"""
        super(IIIFDizasManifest_v3, self).__init__(**kwargs)
        self.agg_condition = {"aggs": {"filtered_Quelldatum": {
            "date_histogram": {"field": ZAS_DATE_ESFIELD, "interval": "year", "order": {"_count": "desc"}}}}}

    def _get_manifest_query(self, limit, terms_manifest_id_field):
        """ to be overwritten, define query to get all data for a manifest

        :param limit: max number of pages to be returned
        :param terms_manifest_id_field: es field name to be used to limit query to a certain manifest
        :return: es query dict
        """

        """             "script_fields": {
                "year_month": {
                    "script": {
                        "lang": "painless",
                        "source": "doc['Quelldatum'].value.toString('yyyy-MM')"
                    }
                }
            },"""
        if self.search_marker:
            query_term = self._manifest_ids[0][2:]
            manifest_query = {
                "size": limit,
                # todo: aus ini auslesen
                "_source": {"excludes": ["*fulltext", "textcoords*"]},
                "query": {
                    "bool": {
                        "must": [
                                    {"exists": {"field": self._canvas_id_field}},
                                    {"match": {self.s.mapping_fulltext["fulltext"]: query_term}}

                                ] + self._get_col_filters4es()
                    }

                }
            }
        else:
            manifest_query = {
                "size": limit,
                "_source": {"excludes": [ "*fulltext", "textcoords*" ]},
                "query": {
                    "bool": {
                        "must": [
                            {"exists": {"field": self._canvas_id_field}},
                            {"terms": {terms_manifest_id_field: self._manifest_ids}}
                        ]+ self._get_col_filters4es()
                    }

                }
            }

        return  manifest_query

    def _get_new_manifest(self, page_metadata: dict, label:str="Sammelmappe DIZAS") -> dict:
        """return a fresh manifest for a page (already containing metadata)
        if multiple manifests are merged to one manifest no AlephLink and Schlagwort is added on toplevel

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_manifest)
        #labels that shall be added to metadata section at manifest label
        label_dict = OrderedDict([("Aleph Link", "Dokumentensammlung SWA"),("title", "Dossiername"),
                                  ("descr_fuv", "Firmen und Verbände"), ("descr_person", "Personen"), ("descr_sach", "Sachdeskriptor")])

        new_manifest["metadata"] = []
        new_manifest["requiredStatement"] = self._build_lang_map_l_v("Attribution", "Archiviert und zur Verfügung gestellt vom Schweizerischen Wirtschaftsarchiv SWA")
        new_manifest["rights"] = "http://rightsstatements.org/vocab/InC/1.0/"
        new_manifest["behavior"] = ["unordered"]
        #new_manifest["viewingDirection"] = "left-to-right"
        if 0:
            new_manifest["logo"] = [{
                  "id" : "https://ub-sipi.ub.unibas.ch/logos/logo-swa-mit-farbe.tif/full/200,/0/default.jpg",
                "type": "Image",
                "format": "image/png",
                  "service" : [{
                      "type": "ImageService2",
                      "profile": "level2",
                                 "id": "https://ub-sipi.ub.unibas.ch/logos/logo-swa-mit-farbe.tif"
                  }]
            }]


        if self.multiple_ids_marker or self._flex:

            new_manifest["label"] = {"de": ["IIIF Presentation API 3.0 (Beta) ZAS Portal"], "en": ["IIIF Presentation API 3.0 (Beta) ZAS Portal"]}
            new_manifest["description"] = {"""de": ["Sie nutzen die IIIF Presentation API des ZAS Portals. Wir befinden uns noch im Beta-Betrieb. "
                                                 "Nutzen Sie die Manifeste in anderen IIIF-Applikationen, aber Achtung diese sind noch nicht zu 100% stabil."]"""
                                           "en": ["You are using the IIIF Presentaton API (Beta) from ZAS Portal. "
                                                 "We are still in beta. Reuse the manifest in other IIIF Manifests but they are not 100% stable."]}
            """new_manifest["summary"] = {"de": ["Sie nutzen die IIIF Presentation API des ZAS Portals. Wir befinden uns noch im Beta-Betrieb. "
                                                 "Nutzen Sie die Manifeste in anderen IIIF-Applikationen, aber Achtung diese sind noch nicht zu 100% stabil."],
                                           "en": ["You are using the IIIF Presentaton API (Beta) from ZAS Portal. "
                                                 "We are still in beta. Reuse the manifest in other IIIF Manifests but they are not 100% stable."]}"""
        else:
            label = page_metadata.get(ZAS_TITLE_ESFIELD, "Kein Titel")
            if label and isinstance(label, list) and isinstance(label[0], dict):
                label= label[0].get("label", label)
            for field, label_value in label_dict.items():
                if field in page_metadata and page_metadata[field]:
                    if field == "Aleph Link":
                        page_metadata[field] = '<a href="' + page_metadata[field] + '">Aleph Link</a>'
                    if isinstance(page_metadata[field], list):
                        # muss gleich sein wie metadaten generierung im ViewGenerator damit nachher gleiche Werte auf tieferer Ebene ausgeschlossen werden
                        value = ", ".join([p for p in page_metadata[field] if isinstance(p, str)])
                        if not value:
                            value = ", ".join([p.get("label") for p in page_metadata[field] if isinstance(p, dict) and p.get("label")])
                    else:
                        if isinstance(page_metadata[field], dict) and page_metadata[field].get("label"):
                            value = page_metadata[field].get("label")
                        else:
                            value = page_metadata[field]
                    new_manifest["metadata"].append(self._build_lang_map_l_v(label_value, value))
            new_manifest.update(self._build_lang_map_l(self._build_full_manifest_label(page_metadata, label)))
            # is already included: new_manifest["description"] = "Description: Dossier " + self._build_full_manifest_label(page_metadata, label)
        return new_manifest

    def _get_new_canvas(self, page: dict) -> dict:
        """
        return a fresh canvas for a page (already containing metadata)
        if multiple manifests are merged to one manifest  AlephLink and Schlagwort are added to canvas metadata

        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        # doppelte Seiten: http://ub-universalviewer.ub.unibas.ch/uv/uv.html#?manifest=http://ub-test-iiifpresentation.ub.unibas.ch/dizas/collection
        new_canvas = deepcopy(self.iiif_canvas)

        try:
            newspaper = self.tostr(page.get(ZAS_SOURCE_ESFIELD))
        except KeyError:
            newspaper = page.get("Quelle","Unbekannt")

        metadata_fields = []
        # kann auch Array sein
        date = self.tostr(page.get(ZAS_DATE_ESFIELD))
        # BAU_1_003888026_44_1.j2k = 1
        page_title = "{} ".format(self.tostr(page.get(ZAS_TITLE_ESFIELD, "") or  ""))
        try:
            if isinstance(page[self.s.mapping_manifest["page_id"]], dict):
                filename = page[self.s.mapping_manifest["page_id"]].get("id")
            else:
                filename = page[self.s.mapping_manifest["page_id"]]
            page_number = str(int(filename.split("_")[4].split(".j2k")[0]))
            page_counter = page["pages"]
            page_label = " {}/{}".format(page_number, page_counter)
        except IndexError:
            # BAU_1_003888026_44.j2k
            page_label = ""
        new_canvas.update(self._build_lang_map_l("{}{} {}{}".format(page_title, date,newspaper,page_label)))
        new_canvas["navDate"] = date

        if self.multiple_ids_marker:
            newspaper_descriptor = "title"
            aleph_link = "Aleph Link"
            descriptor = self.tostr(page[newspaper_descriptor])
            additional_metadata_fields = [newspaper_descriptor, aleph_link]
            metadata_fields.extend(additional_metadata_fields)

            new_canvas.update(self._build_lang_map_l(descriptor + " " + date + " " + newspaper + page_label))
        from rdv_config_store_ubit import ConfigStore
        pj_conf = ConfigStore()
        pj_conf.load_project_config("zas-prod")
        view_object = ZasView(page_id = page.get("id"), page = page, lang = "de", project_config_store=pj_conf)

        new_canvas["metadata"] = view_object.get_label_value_metadata(dict_type="iiif_v3",
                                                           exclude_fields=["Digitalisat Artikel", "Digitalisate Dossier", "Dokumentensammlung"])

        if self.multiple_ids_marker:
            for label in metadata_fields:
                if label in page and page[label] and page[label] != "None":
                    if label == "Aleph Link":
                        page[label] = '<a href="' + page[label] + '">Aleph Link</a>'
                    new_canvas["metadata"].append(self._build_lang_map_l_v(label, page[label], "de"))


        self._add_seealso(new_canvas, page)

        return new_canvas



    def _add_seealso(self, new_canvas: dict, page: dict) -> None:
        """
        update iiif Canvas with links to seeAlso/Rendering Representations
        :param new_canvas: dict containing iiif canvas
        :param page: dict containing all metadata from ES for a page
        """

        new_canvas["seeAlso"] = [
            {"label": self._build_lang_map(self._cleanse_pdf_path(page, pdf),"de"), "type": "Dataset",
             "id": self._get_seealso_path(page, "pdf") + "/" + self._cleanse_pdf_path(page, pdf), "format": "application/pdf"} for pdf
            in page.get("pdf_files",[])]
        new_canvas["seeAlso"].extend(
            [{"label": self._build_lang_map(txt,"de"), "type": "Dataset", "id": self._get_seealso_path(page, "txt") + "/" + txt, "format": "text/txt"} for txt in
             page.get("txt_files",[])])
        new_canvas["seeAlso"].extend(
            [{"label": self._build_lang_map(xml,"de"), "type": "Dataset", "id": self._get_seealso_path(page, "xml") + "/" + xml, "format": "text/xml"} for xml in
             page.get("xml_files",[])  if not xml.endswith("_Alto.xml") ])

        bsb_plugin = [{"label": self._build_lang_map(xml,"de"), "type": "Dataset", "@id": self._get_seealso_path(page, "xml") + "/" + xml,
              "format": "application/xml+alto", "profile": "http://www.loc.gov/standards/alto/v3/alto.xsd"} for xml in
             page.get("xml_files",[]) if xml.endswith("_Alto.xml") ]
        # todo: anpassen: da bsb plugin nur mit einseitigen Alto XML umgehen kann
        if page.get("pages", 0) < 2:
            new_canvas["seeAlso"].extend(bsb_plugin)

        # zip Download Option
        from urllib.parse import unquote, quote_plus
        manifest_url = "{}{}/{}/{}/".format(self.iiif_service_host, self.project, self._manifest_id, "flex_manifest" if self._flex else "manifest")
        manifest_zipdl_url = "{}dl/iiif?manif={}".format(self.iiif_service_host, quote_plus(manifest_url, safe=""))
        new_canvas["seeAlso"].extend(
            [{"label": self._build_lang_map("ZipDownload","de"), "type": "Dataset", "id": manifest_zipdl_url, "format": "application/zip"}])

        #if not xml.endswith("_meta.xml")
        new_canvas["rendering"] = new_canvas["seeAlso"]

    def _cleanse_pdf_path(self, page, pdf):
        subset = page["proj_id"]

        if subset == "ezas":
            # "f_drive17a17aa5-Basler Zeitung_240_20161013_23.pdf" -> "f_drive-17a-17aa5-Basler Zeitung_240_20161013_23.pdf"
            zas_id = page.get("zas_id")
            disk_l, path =pdf.split("_drive", maxsplit=1)
            folder, rest = path.split(zas_id + "-")
            pdf = "-".join(["{}_drive".format(disk_l), folder, zas_id, rest])
            return pdf
        else:
            return pdf

    def _get_image_dimensions(self, page):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._image_width_field]
            height = page[self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            pass

        if not image_dimensions.get("width"):
            try:
                width = page[self._canvas_id_field][self._image_width_field]
                height = page[self._canvas_id_field][self._image_height_field]
                image_dimensions = {"width": width, "height": height}
            except (TypeError, KeyError):
                pass
        return image_dimensions

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        if self.project in (["zas_flex_manifest", "zas_flex_manifest2", "zas_flex_manifest_v3", "zas_dossiers_object_sb"] + \
                ["authzas_flex_manifest", "authzas_flex_manifest2", "authzas_flex_manifest_v3", "authzas_dossiers_object_sb"]):
            # dizas / ezas for resolving path
            subset = page["proj_id"]

            return self.imageserver_host  + subset + "/" + page[self.s.mapping_manifest["page_id"]]
        else:
            return self.imageserver_host + page[self.s.mapping_manifest["page_id"]]

            # parse.quote_plus("BAU_1_" + page["Systemnummer"] + "/" + page["Artikel_ID"] + "/j2k/" + page["jp2_files"])

    def _get_resource_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        if self.project in (["zas_flex_manifest", "zas_flex_manifest2", "zas_flex_manifest_v3", "zas_dossiers_object_sb"] + \
                ["authzas_flex_manifest", "authzas_flex_manifest2", "authzas_flex_manifest_v3", "authzas_dossiers_object_sb"]):
            # dizas / ezas for resolving path
            subset = page["proj_id"]
            if isinstance(page[self.s.mapping_manifest["page_id"]], dict):
                filename = page[self.s.mapping_manifest["page_id"]].get("id")
            else:
                filename = page[self.s.mapping_manifest["page_id"]]
            return self.imageserver_host  + subset + "/" + filename
        else:
            if isinstance(page[self.s.mapping_manifest["page_id"]], dict):
                filename = page[self.s.mapping_manifest["page_id"]].get("id")
            else:
                filename = page[self.s.mapping_manifest["page_id"]]
            return self.imageserver_host + filename
            # parse.quote_plus("BAU_1_" + page["Systemnummer"] + "/" + page["Artikel_ID"] + "/j2k/" + page["jp2_files"])

    def _get_seealso_path(self, page: dict, type_: str) -> str:
        """ build see also path

        :param page: dict containing all metadata from ES for a page
        :param type_: type of seeAlso Representation
        :return: URL for seeAlso Representation
        """

        subset = page["proj_id"]
        return self.seealso_host + subset

    def build_same_size_buckets(self, agg_results):
        buckets = []
        aggr_counter = 0
        agg_start = ""
        agg_end = ""
        for b in sorted(agg_results["aggregations"]["filtered_Quelldatum"]["buckets"],
                        key=lambda x: x["key_as_string"]):
            # todo: wert anpassen, max ist 9999 durch suche
            if aggr_counter < 500 and aggr_counter > 0:
                aggr_counter += b["doc_count"]
                agg_end = b["key_as_string"]
            else:
                if agg_end:
                    buckets.append({"agg_start": agg_start, "agg_end": agg_end, "aggr": aggr_counter})
                agg_start = b["key_as_string"]
                aggr_counter = b["doc_count"]
        else:
            agg_end = b["key_as_string"]
            buckets.append({"agg_start": agg_start, "agg_end": agg_end, "aggr": aggr_counter})
        return buckets

    def _define_subcat_buckets(self, buckets, query):
        results = {"hits": {"hits": []}}
        for bucket in buckets:
            bucket_filtet = {"filter": {
                "bool": {"must": [{"range": {ZAS_DATE_ESFIELD: {"gte": bucket["agg_start"], "lte": bucket["agg_end"]}}}]}}}
            bucket_query = deepcopy(query)
            bucket_query["query"]["bool"].update(bucket_filtet)
            bucket_result = self.es.search(index=self.index, body=bucket_query)

            # todo: url anpassen
            # todo define: bucket_label, flex_manifest, agg_start, agg_end,

            store_url = self.iiif_service_host + "zas_flex_manifest/store_id/"
            store_id = requests.post(store_url, json=bucket_query, verify=False).json()["dyn_manif_id"]

            for result in bucket_result["hits"]["hits"]:
                result["_source"]["bucket_label"] = "{}/{}".format(bucket["agg_start"][:4], bucket["agg_end"][:4])
                result["_source"]["flex_manifest"] = store_id.strip()

            results["hits"]["hits"].extend(bucket_result["hits"]["hits"])
        return results

class IIIFDizasSplitManifest(IIIFManifest):

    def __init__(self, **kwargs):
        """build Manifest for DIZAS project"""
        super(IIIFDizasManifest_v3, self).__init__(**kwargs)

        self._manifest_id = kwargs["manifest_ids"]
        self._manifest_ids = self.split_manifest_ids(kwargs["manifest_ids"])