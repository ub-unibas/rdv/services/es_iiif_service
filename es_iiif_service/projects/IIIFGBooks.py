from es_iiif_service.projects.IIIFBaseProj import IIIFBaseProjManifest_v3, IIIFBaseProjManifest
from copy import deepcopy

class IIIFGbooksManifest_v3(IIIFBaseProjManifest_v3):
    proj_label = "Google Books Swiss"
    config_store_id = "gbooks_obj"
    continous_ini = "gbooks2"

    def _get_resource_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        img_path = page[self._canvas_id_field].get("id")
        img_path = img_path.replace("master", "latest").replace("/ZBZ/", "/ZBZH/")
        version = page.get("version")
        if version:
            img_path = img_path.replace("latest", version)
        return self.imageserver_host + img_path

    def _get_new_canvas(self, page: dict) -> dict:
        """
        to be overwritten: return a fresh canvas for a page (already containing metadata)
        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        new_canvas = super()._get_new_canvas(page)
        img_data = page[self._canvas_id_field]
        new_canvas.update(self._build_lang_map_l("{}".format(img_data.get("order_label"))))
        self._add_seealso(new_canvas, page)
        return new_canvas

    def _add_seealso(self, new_canvas, page):
        img_data = page[self._canvas_id_field]
        version = page.get("version")
        # "label": self._build_lang_map(hocr,"de"),
        new_canvas["seeAlso"] = [{"@id": self.seealso_host + hocr.replace("latest", version or "latest").replace("master", version or "latest").replace("/ZBZ/", "/ZBZH/"), "type": "Dataset",   "format": "text/vnd.hocr+html",
                      "profile": "http://kba.cloud/hocr-spec/1.2/"} for hocr in img_data.get("hocr_id",[])]
        new_canvas["rendering"] = new_canvas["seeAlso"]

    def _get_image_dimensions(self, page):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._canvas_id_field][self._image_width_field]
            height = page[self._canvas_id_field][self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            pass
        return image_dimensions

class IIIFGbooksManifest(IIIFBaseProjManifest):
    proj_label = "Google Books Swiss"
    config_store_id = "gbooks_obj"
    continous_ini = "gbooks_pages2"

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        return self.imageserver_host + page[self._canvas_id_field].get("id")

    def _get_image_dimensions(self, page, page_id, image_dimensions_db={}):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._canvas_id_field][self._image_width_field]
            height = page[self._canvas_id_field][self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            pass
        return image_dimensions