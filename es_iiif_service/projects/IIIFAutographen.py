from es_iiif_service.projects.IIIFBaseProj import IIIFBaseProjManifest_v3, IIIFBaseProjManifest
from rdv_query_builder import AutographenView

class IIIFAutographenManifest_v3(IIIFBaseProjManifest_v3):
    proj_label = "Autographensammlung Geigy-Hagenbach Universitätsbibliothek Basel"
    view_class = AutographenView
    continous_ini = "autographen2"

    def _get_resource_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        if page[self._canvas_id_field].get("url","").startswith("http"):
            return page[self._canvas_id_field].get("url")
        else:
            return self.imageserver_host + page[self._canvas_id_field].get("url")

    def _get_image_dimensions(self, page):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._canvas_id_field][self._image_width_field]
            height = page[self._canvas_id_field][self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            pass
        return image_dimensions

class IIIFAutographenManifest(IIIFBaseProjManifest):
    proj_label = "Autographensammlung Geigy-Hagenbach Universitätsbibliothek Basel"
    view_class = AutographenView
    continous_ini = "autographen2"

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        if page[self._canvas_id_field].get("url","").startswith("http"):
            return page[self._canvas_id_field].get("url")
        else:
            return self.imageserver_host + page[self._canvas_id_field].get("url")

    def _get_image_dimensions(self, page, page_id, image_dimensions_db={}):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._canvas_id_field][self._image_width_field]
            height = page[self._canvas_id_field][self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            pass
        return image_dimensions
