from es_iiif_service.projects.IIIFBaseProj import IIIFBaseProjManifest_v3, IIIFBaseProjManifest
from copy import deepcopy

class IIIFAutographenManifest_v3(IIIFBaseProjManifest_v3):
    proj_label = "Autographensammlung Geigy-Hagenbach Universitätsbibliothek Basel"
    config_store_id = "autographen"
    continous_ini = "autographen2"

    def _get_resource_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """

        if page[self._canvas_id_field].get("url","").startswith("http"):
            return page[self._canvas_id_field].get("url")
        else:
            return self.imageserver_host + page[self._canvas_id_field].get("url")

    def _get_image_dimensions(self, page):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._canvas_id_field][self._image_width_field]
            height = page[self._canvas_id_field][self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            pass
        return image_dimensions

    def _get_new_canvas(self, page: dict) -> dict:
        """
        to be overwritten: return a fresh canvas for a page (already containing metadata)
        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        new_canvas = deepcopy(self.iiif_canvas)
        img_data = page[self._canvas_id_field]
        new_canvas.update(self._build_lang_map_l("{}".format(img_data.get("order_label"))))
        self._add_seealso(new_canvas, page)
        return new_canvas

    def _add_seealso(self, new_canvas, page):
        img_data = page[self._canvas_id_field]
        # "label": self._build_lang_map(hocr,"de"),
        new_canvas["seeAlso"] = []
        canvas_id = page[self._canvas_id_field].get("url").split("/")[-2]
        fulltext_url = f"https://127.0.0.1:5001/autographen/get_fulltext/{canvas_id}"
        if 0:
            new_canvas["seeAlso"] = [{"@id": self.seealso_host + hocr, "type": "Dataset",   "format": "text/vnd.hocr+html",
                                      "profile": "http://kba.cloud/hocr-spec/1.2/"} for hocr in img_data.get("hocr_id",[])]
        bsb_plugin = [{"label": {"de": ["Test"]}, "type": "Dataset",
                       "@id": fulltext_url,
                       "format": "application/xml+alto", "profile": "http://www.loc.gov/standards/alto/v3/alto.xsd"}]
        # todo: anpassen: da bsb plugin nur mit einseitigen Alto XML umgehen kann

        new_canvas["seeAlso"].extend(bsb_plugin)

        new_canvas["rendering"] = new_canvas["seeAlso"]

class IIIFAutographenManifest(IIIFBaseProjManifest):
    proj_label = "Autographensammlung Geigy-Hagenbach Universitätsbibliothek Basel"
    config_store_id = "autographen"
    continous_ini = "autographen2"

    def _get_image_path(self, page: dict) -> str:
        """
        get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        if page[self._canvas_id_field].get("url","").startswith("http"):
            return page[self._canvas_id_field].get("url")
        else:
            return self.imageserver_host + page[self._canvas_id_field].get("url")

    def _get_image_dimensions(self, page, page_id, image_dimensions_db={}):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._canvas_id_field][self._image_width_field]
            height = page[self._canvas_id_field][self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            pass
        return image_dimensions
