import logging
import os
import json
import sys
import urllib
import hashlib

from flask import Flask, request, Response, abort
from flask_cors import CORS
from werkzeug.middleware.proxy_fix import ProxyFix
from flask_compress import Compress
from pkg_resources import resource_stream, Requirement, resource_filename
from cache_decorator_redis_ubit import CacheDecorator, NoCache
from es_iiif_service import IIIFSearch, IIIFMetadataSearch, IIIFAutocomplete, IIIFIniSettings, \
    ViewSettings
from flask_restx import Resource, Api, fields
# todo: anpassen
es_iiif_app = Flask(__name__) #, root_path= "/home/martin/PycharmProjects/es_iiif_service/es_iiif_service/templates" if socket.gethostname() == "reisache" else "/home/iiif/es_iiif_service_test/flask_root" )
es_iiif_app.wsgi_app = ProxyFix(es_iiif_app.wsgi_app)

logging.basicConfig(level=logging.INFO, stream=sys.stdout)

origins = [
    "https?://127.0.0.1.*",
    "https?://localhost.*",
    "https?://ub-.*.unibas.ch",
    "https?://192.168.128.*",
    "https?://.*.ub-digitale-dienste.k8s-001.unibas.ch",
    "https://mirador-dev.netlify.app/"
]
CORS(es_iiif_app, origins=origins, supports_credentials=True)
Compress(es_iiif_app)
es_iiif_api = Api(es_iiif_app, doc='/doc/')

# TODO wieder einbauen
#from rdv_iiif_download import ns_iiif_dl
#es_iiif_api.add_namespace(ns_iiif_dl)

def get_view_settings(project: str, base_url: str, debug: bool) -> ViewSettings:
    """ get settings for a specific project for a rdv view

    :param project: name of project
    :param base_url: name of base url for iiif service from flask request
    :param debug: if in debug mode
    :return: Object containing Attributes for settings from ini File
    """
    ini_path = os.path.join( "ini")
    ini_zas_file = os.path.join(ini_path, "zas", project + ".ini")
    ini_port_file = os.path.join(ini_path, "portraet", project + ".ini")
    ini_ap_file = os.path.join(ini_path, "afrikaportal", project + ".ini")
    for spec_file in [ini_zas_file, ini_port_file, ini_ap_file]:
        if os.path.exists(resource_filename(Requirement.parse("es_iiif_service"), spec_file)):
            ini_file = spec_file
            break
    else:
        ini_file = os.path.join(ini_path, project + ".ini")

    return ViewSettings(ini_file, os.path.join(ini_path, "global.ini"), base_url, debug)

def get_settings(project: str, base_url: str, debug: bool, mode: bool = False) -> IIIFIniSettings:
    """ get settings for a specific project

    :param project: name of project
    :param base_url: name of base url for iiif service from flask request
    :param debug: if in debug mode
    :return: Object containing Attributes for settings from ini File
    """
    ini_path = "es_iiif_ini"
    ini_file= ""
    try:
        if mode == "test":
            ini_zas_file = os.path.join(ini_path, "zas_test", project + ".ini")
            ini_port_file = os.path.join(ini_path, "portraet_test", project + ".ini")
            ini_ap_file = os.path.join(ini_path, "afrikaportal_test", project + ".ini")
        elif mode == "atestset":
            ini_zas_file = os.path.join(ini_path, "zas_testset", project + ".ini")
            ini_port_file = os.path.join(ini_path, "portraet_testset", project + ".ini")
            ini_ap_file = os.path.join(ini_path, "afrikaportal_testset", project + ".ini")
        elif mode == "auth":
            ini_zas_file = os.path.join(ini_path, "zas_auth", project + ".ini")
            ini_port_file = os.path.join(ini_path, "portraet_auth", project + ".ini")
            ini_ap_file = os.path.join(ini_path, "afrikaportal_auth", project + ".ini")
        elif mode == "kc":
            ini_zas_file = os.path.join(ini_path, "zas_kc", project + ".ini")
            ini_port_file = os.path.join(ini_path, "portraet_kc", project + ".ini")
            ini_ap_file = os.path.join(ini_path, "afrikaportal_kc", project + ".ini")
        elif mode == "dev":
            ini_zas_file = os.path.join(ini_path, "zas_dev", project + ".ini")
            ini_port_file = os.path.join(ini_path, "portraet_test", project + ".ini")
            ini_ap_file = os.path.join(ini_path, "afrikaportal_test", project + ".ini")
        else:
            ini_zas_file = os.path.join(ini_path, "zas", project + ".ini")
            ini_port_file = os.path.join(ini_path, "portraet", project + ".ini")
            ini_ap_file = os.path.join(ini_path, "afrikaportal", project + ".ini")

        for spec_file in [ini_zas_file, ini_port_file, ini_ap_file]:
            if os.path.exists(resource_filename(Requirement.parse("es_iiif_service"), spec_file)):
                ini_file = spec_file
                break
        else:
            ini_file = os.path.join(ini_path, project + ".ini")
        print("INI", ini_file)
        return IIIFIniSettings(ini_file, os.path.join(ini_path, "global.ini"), base_url, debug)
    except Exception:
        import traceback
        traceback.print_exc()
        print("Do Exception Handling", ini_file)


def return_response(r: str) -> Response:
    """ return flask Response, set Access-Control-Allow-Origin

    :param r: IIIF-Object
    :return: flask Response with necessary headers
    """
    if isinstance(r, dict):
        r = json.dumps(r)
    resp = Response(r)
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = "application/json"
    return resp

def return_html_response(r: str) -> Response:
    """ return flask Response, set Access-Control-Allow-Origin

    :param r: IIIF-Object
    :return: flask Response with necessary headers
    """
    if isinstance(r, dict):
        r = json.dumps(r)
    resp = Response(r)
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Content-Type'] = "text/html"
    return resp


def overwrite_settings(s):
    """overwrite settings with url params"""
    try:
        request.args = parse_args(request.args["enc_args"])
        try:
            col_struct = request.args["col_struct"]
            s.collection_structure = col_struct.split("$,$")
            s.collection_sort = s.collection_structure
            s.mapping_manifest["manifest_id"] = s.collection_structure[-1]
            s.mapping_manifest["manifest_label"] = s.collection_structure[-1]
            s.url_param["col_struct"] = s.collection_structure
        except KeyError:
            pass

        try:
            manif_struct = request.args["manif_struct"]
            s.manifest_structure = manif_struct.split("$,$")
            s.manifest_sort = [s.mapping_manifest["page_id"]] + s.manifest_structure
            s.url_param["manif_struct"] = s.manifest_structure
        except KeyError:
            pass

        try:
            manif_field = request.args["manif_field"]
            s.mapping_manifest["manifest_id"] = manif_field
            s.mapping_manifest["manifest_label"] = manif_field
            s.url_param["manif_field"] = manif_field
        except KeyError:
            pass

        try:
            col_filters = {f.split("$:$")[0]: f.split("$:$")[1] for f in request.args["col_filter"].split("$,$")}
            s.url_param["col_filter"] = col_filters
        except KeyError:
            pass

    except Exception:
        import traceback
        traceback.print_exc()
        pass

def parse_args(args_str: str) -> dict:
    args = {}
    for arg in args_str.split("$~$"):
        k,v = arg.split("$§$")
        args[k] = v
    return args

resource_fields = es_iiif_api.model("Resource", {
    "project": fields.String(description="The name", required=True),
    "manifest_ids": fields.String(description="The name", required=True),
})

@es_iiif_api.route('/<project>/view/<page_id>/<lang>')
class ApiRDVView(Resource):
    def get(self, project, page_id, lang):
        s = get_view_settings(project, request.host_url, es_iiif_app.debug)
        view_object = s.view_class(s)
        view_object.logger.info(project + ": RDV View: " + page_id)
        r = view_object(page_id, lang)
        return return_response(r)


@es_iiif_api.route('/<project>/collection/', doc={"description": "Route to main collection"})
@es_iiif_api.route('/<project>/collection/<path:collection_path>/')
class ApiIIIFCollection(Resource):
    def get(self, project, collection_path=""):
        s = get_settings(project, request.host_url, es_iiif_app.debug)
        if s.project.get("request_param") == "True":
            overwrite_settings(s)

        cachedecorator = CacheDecorator if s.manifest_cache and s.manifest_cache != "False" else NoCache
        @cachedecorator()
        # cache_url to differ cache between test and prod system
        def get_collection(project, collection_path, cache_url):
            collecion_object = s.collection_class(settings=s, project=project, collection_path=collection_path)
            logged_path = collection_path or "Main"
            collecion_object.logger.info(project + ": Collection: " + logged_path)
            collecion_object.logger.info(str(request.args))
            r = collecion_object()
            return r

        return return_response(get_collection(project=project, collection_path=collection_path, cache_url= request.host_url))

@es_iiif_api.route('/atestset<project>/<manifest_ids>/flex_manifest/', defaults={'flex': True, "mode": "atestset"})
@es_iiif_api.route('/atestset<project>/<manifest_ids>/manifest/', defaults={'flex': False, "mode": "atestset"})
@es_iiif_api.route('/auth<project>/<manifest_ids>/flex_manifest/', defaults={'flex': True, "mode": "auth"})
@es_iiif_api.route('/auth<project>/<manifest_ids>/manifest/', defaults={'flex': False, "mode": "auth"})
@es_iiif_api.route('/kc<project>/<manifest_ids>/flex_manifest/', defaults={'flex': True, "mode": "kc"})
@es_iiif_api.route('/kc<project>/<manifest_ids>/manifest/', defaults={'flex': False, "mode": "kc"})
@es_iiif_api.route('/test<project>/<manifest_ids>/flex_manifest/', defaults={'flex': True, "mode": "test"})
@es_iiif_api.route('/test<project>/<manifest_ids>/manifest/', defaults={'flex': False, "mode": "test"})
@es_iiif_api.route('/dev<project>/<manifest_ids>/flex_manifest/', defaults={'flex': True, "mode": "dev"})
@es_iiif_api.route('/dev<project>/<manifest_ids>/manifest/', defaults={'flex': False, "mode": "dev"})
@es_iiif_api.route('/<project>/<manifest_ids>/flex_manifest/', defaults={'flex': True, "mode": "prod"})
@es_iiif_api.route('/<project>/<manifest_ids>/manifest/', defaults={'flex': False, "mode": "prod"})
@es_iiif_api.doc(model=resource_fields)
class ApiIIIFManifest(Resource):


    def get(self, project, manifest_ids, flex, mode):
        try:
            s = get_settings(project, request.host_url, es_iiif_app.debug, mode)

            if s.project.get("request_param") == "True":
                overwrite_settings(s)

            cachedecorator = CacheDecorator if s.manifest_cache and s.manifest_cache != "False" else NoCache
            # include host in caching
            @cachedecorator()
            def get_manifest(project, manifest_ids, cache_url):
                manifest_object = s.manifest_class(settings=s, project=project, manifest_ids=manifest_ids, flex=flex)
                manifest_object.logger.info(project + ": Manifest: " + manifest_ids)
                r = manifest_object()
                print("After Manifest Return")
                return r
            if 0:
                import cProfile
                import pstats
                cProfile.runctx('get_manifest(project=project, manifest_ids=manifest_ids, cache_url= request.host_url)', globals(), locals(),"restats")
                p = pstats.Stats('restats')
                p.sort_stats('cumtime').print_stats(0.02)
                #_get_manifest_pages
                # _build_manifest _build_content
                p.print_callees('_build_manifest')

            manifest = get_manifest(project=project, manifest_ids=manifest_ids, cache_url= request.host_url)
            return return_response(manifest)
        except Exception as e:
            import traceback
            traceback.print_exc()
            logging.error(e)


@es_iiif_api.route('/<project>/services/<manifest_ids>/flex_search/', methods=['GET'], defaults={'flex': True, "mode": "prod"})
@es_iiif_api.route('/<project>/services/<manifest_ids>/search/', methods=['GET'], defaults={'flex': False, "mode": "prod"})
@es_iiif_api.route('/dev<project>/services/<manifest_ids>/flex_search/', defaults={'flex': True, "mode": "dev"})
@es_iiif_api.route('/dev<project>/services/<manifest_ids>/search/', defaults={'flex': False, "mode": "dev"})
@es_iiif_api.doc(params={"q": "Query String", "manifest_ids": "Comma seperated List of manifests"})
class ApiIIIFSearch(Resource):
    def get(self, project, manifest_ids, flex, mode):
        s = get_settings(project, request.host_url, es_iiif_app.debug, mode)

        q = request.args["q"]
        try:
            manif_field = request.args["manif_field"]
            s.mapping_fulltext["manifest"] = manif_field + ".keyword"
            s.url_param["manif_field"] = manif_field
        except KeyError:
            pass

        # because uv sends windows-1252 encoded strings: gepr%E4gt, workaround, error prone
        # query_string = request.query_string.decode("windows-1252")
        # unqoted_string = urllib.parse.unquote(query_string, encoding="windows-1252")
        # args = dict(zip(unqoted_string.split("=")[0::2],unqoted_string.split("=")[1::2]))
        # q = args["q"]

        if 0:
            import cProfile
            import pstats
            cProfile.runctx('IIIFSearch(settings=s, project=project, q=q, manifest_ids=manifest_ids, flex=flex)', globals(), locals(),"restats")
            p = pstats.Stats('restats')
            p.sort_stats('cumtime').print_stats(0.1)
            p.sort_stats('cumtime').print_stats('IIIF')
            #_get_manifest_pages
            # _build_manifest _build_content
            #p.print_callees('IIIF')


        search_object = IIIFSearch(settings=s, project=project, q=q, manifest_ids=manifest_ids, flex=flex)
        search_object.logger.info(project + ": Search: '" + q + "' Manifest: " + manifest_ids)
        r = search_object()

        return return_response(r)


@es_iiif_api.route('/<project>/services/<manifest_ids>/metadata_search/', methods=['GET'])
class ApiIIIFMetadataSearch(Resource):
    def get(self, project, manifest_ids):
        s = get_settings(project, request.host_url, es_iiif_app.debug)

        q = request.args["q"]
        try:
            manif_field = request.args["manif_field"]
            s.mapping_fulltext["manifest"] = manif_field + ".keyword"
            s.url_param["manif_field"] = manif_field
        except KeyError:
            pass


        # because uv sends windows-1252 encoded strings: gepr%E4gt, workaround, error prone, version 3.0.33 seems to be osolete
        # query_string = request.query_string.decode("windows-1252")
        # unqoted_string = urllib.parse.unquote(query_string, encoding="windows-1252")
        # args = dict(zip(unqoted_string.split("=")[0::2],unqoted_string.split("=")[1::2]))
        # q = args["q"]

        search_object = IIIFMetadataSearch(settings=s, project=project, q=q, manifest_ids=manifest_ids)
        search_object.logger.info(project + ": MetadataSearch: '" + q + "' Manifest: " + manifest_ids)
        r = search_object()
        return return_response(r)


@es_iiif_api.route('/<project>/services/<manifest_ids>/flex_autocomplete/', methods=['GET'], defaults={'flex': True})
@es_iiif_api.route('/<project>/services/<manifest_ids>/autocomplete/', methods=['GET'], defaults={'flex': False})
class ApiIIIFAutocomplete(Resource):
    def get(self, project, manifest_ids, flex):
        s = get_settings(project, request.host_url, es_iiif_app.debug)

        # q = request.args["q"]

        # because uv sends windows-1252 encoded strings: gepr%E4gt, workaround, error prone,  version 3.0.33
        # seems to be osolete for search but not for autocomplete
        # seems to be solved
        if 0:
            query_string = request.query_string.decode("windows-1252")
            unqoted_string = urllib.parse.unquote(query_string, encoding="windows-1252")
            args = dict(zip(unqoted_string.split("=")[0::2],unqoted_string.split("=")[1::2]))
            q = args["q"]
        q = request.args["q"]
        try:
            manif_field = request.args["manif_field"]
            s.mapping_fulltext["manifest"] = manif_field + ".keyword"
            s.url_param["manif_field"] = manif_field
        except KeyError:
            pass

        if 0:
            import cProfile
            import pstats
            cProfile.runctx('IIIFAutocomplete(settings=s, project=project, q=q, manifest_ids=manifest_ids, flex=flex)', globals(), locals(),"restats")
            p = pstats.Stats('restats')
            p.sort_stats('cumtime').print_stats(0.1)
            p.sort_stats('cumtime').print_stats('IIIF')
            #_get_manifest_pages
            # _build_manifest _build_content
            #p.print_callees('IIIF')

        search_object = IIIFAutocomplete(settings=s, project=project, q=q, manifest_ids=manifest_ids, flex=flex)
        search_object.logger.info(project + ": Autocomplete: '" + q + "' Manifest: " + manifest_ids)
        r = search_object()
        return return_response(r)



# for post requests
# todo: nicht von aussen zügnglich machen
@es_iiif_api.route('/atestset<project>/store_id/', defaults={"mode": "atestset"})
@es_iiif_api.route('/auth<project>/store_id/', defaults={"mode": "auth"})
@es_iiif_api.route('/kc<project>/store_id/', defaults={"mode": "kc"})
@es_iiif_api.route('/test<project>/store_id/', defaults={"mode": "test"})
@es_iiif_api.route('/dev<project>/store_id/', defaults={"mode": "dev"})
@es_iiif_api.route('/<project>/store_id/', methods=['POST'], defaults={"mode": "prod"})
class IDStore(Resource):
    def post(self,project, mode):

        # todo: limit access to localhost, ub-rdv
        if False and request.remote_addr not in ["127.0.0.1", "131.152.230.160", "131.152.230.33"]:
            abort(403)
        s = get_settings(project, request.host_url, es_iiif_app.debug, mode)
        cache = CacheDecorator()
        query = request.json

        check_sum = hashlib.md5(json.dumps(query).encode('utf-8')).hexdigest()
        # necessary to only include correct
        _canvas_id_field = s.mapping_manifest["page_id"]
        query["query"]["bool"]["must"].append({"exists": {"field": _canvas_id_field}})
        cache_key = cache.get_cache_key(unique_id=check_sum)
        cache.set_keyvalue_cache(key=cache_key, value=query)
        print("STORE query", query)
        return {"dyn_manif_id": cache_key}

@es_iiif_api.route('/atestset<project>/get_id/<unique_id>', defaults={"mode": "atestset"})
@es_iiif_api.route('/auth<project>/get_id/<unique_id>', defaults={"mode": "auth"})
@es_iiif_api.route('/kc<project>/get_id/', defaults={"mode": "kc"})
@es_iiif_api.route('/test<project>/get_id/<unique_id>', defaults={"mode": "test"})
@es_iiif_api.route('/dev<project>/get_id/', defaults={"mode": "dev"})
@es_iiif_api.route('/<project>/get_id/<unique_id>', defaults={"mode": "prod"})
class IDRetriever(Resource):
    def get(self, project, unique_id, mode):
        s = get_settings(project, request.host_url, es_iiif_app.debug, mode)
        cache = CacheDecorator()
        query = cache.get_keyvalue_cache(unique_id)
        return query

#new = ApiIIIFManifest()
#print(new.get("dizas_dossiers_test_object_sb","001447489", False).json)