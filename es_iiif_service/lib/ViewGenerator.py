import json
import requests
from typing import Any

from elasticsearch import Elasticsearch
from cache_decorator_redis_ubit import CacheDecorator
import pygsheets
from flask import render_template

# sheet to define which fields shall be ingested
SHEET_FIELDS2INGEST = "fields_filter"
# sheet name in which spreadsheet ids for each insitution are listed
SHEET_SETTINGS_INSTITUTION = "institution_settings"
# column name for institution id
COLUMN_MAPPING_NAME = "Institutionskürzel"
# column name for spreadsheetId
COLUMN_SPREADSHEETID = "SpreadsheetId"

COLUMN_ORDER = "Order"
COLUMN_DISPLAY_LABEL = "Feldlabel"
COLUMN_ES_FIELD = "Feld"

# so umbauen, dass Google doccs sheet erst nach dem Anreichern befüllt wird
class ViewGenerator:

    def __init__(self, settings):
        """build Views for RDV"""
        # todo warum nicht von IIIF Template abgeleitet?
        self.s = settings
        self.es = Elasticsearch(self.s.es_host)
        self.project = self.s.project
        self.logger = self.s.logger

    def __call__(self, page_id: str, lang: str) -> str:
        self.result = self.get_rdv_view(page_id, lang)
        return self.return_result()

    def return_result(self) -> str:
        return json.dumps(self.result)

    def get_view_def(self, institution, inst_spreadsheet_id):

        @CacheDecorator()
        def cache_inst_fields(self: Any, institution: str  = "", inst_spreadsheet_id: str ="") -> dict:
            """
            get definition for field display for an institution
            :param self:
            :param institution: shortname for institution (to get spreadsheet id for settings)
            :return:
            """

            gc = pygsheets.authorize(service_file=self.s.project["auth_file"])
            # if no spreadsheet defined, check for config sheet for subprojects
            if not inst_spreadsheet_id:
                spreadsheet = gc.open_by_key(self.s.project["config_sheet"])
                inst_sheet = spreadsheet.worksheet_by_title(SHEET_SETTINGS_INSTITUTION)
                inst_settings = inst_sheet.get_all_records()
                inst_spreadsheet_id = ""
                for inst in inst_settings:
                    if inst[COLUMN_MAPPING_NAME] == institution:
                        inst_spreadsheet_id = inst[COLUMN_SPREADSHEETID]
                        break
                else:
                    self.logger.warning("No Spreadsheet defined")
            spreadsheet_inst = gc.open_by_key(inst_spreadsheet_id)
            fields_sheet = spreadsheet_inst.worksheet_by_title(SHEET_FIELDS2INGEST)
            return fields_sheet.get_all_records()

        try:
            fields_def = cache_inst_fields(self, institution=institution, inst_spreadsheet_id=inst_spreadsheet_id)
        except Exception:
            #todo: Exception genauer definieren
            import traceback
            traceback.print_exc()
            fields_def = []
        return fields_def

    def get_iiif_view(self, page: str, institution: str, lang: str, exclude_fields = [], inst_spreadsheet_id=""):
        """ extract only those metadatafields to be shown from Object that are defined in Google Spreadsheet

        :param page: metadata for a page
        :param institution: name of institution
        :return: array containing metadata in IIIF Standard
        """

        def sort_order(x):
            try:
                return int(x.get(COLUMN_ORDER, 0))
            except ValueError:
                return 999

        descr_metadata = []
        fields_def = self.get_view_def(institution=institution, inst_spreadsheet_id=inst_spreadsheet_id)
        # todo: show_all_metadata ergänzen
        # all metadata shall be shown, if no spreadsheet id
        if not fields_def: # and self.s.show_all_metadata:
            filtered_fields = [{COLUMN_ES_FIELD: x} for x in page]
        else:
            filtered_fields = filter(lambda x: x.get(COLUMN_ORDER), fields_def)
        fields = sorted(list(filtered_fields), key=lambda x: sort_order(x))
        for field in fields:
            label = field.get("{} {}".format(COLUMN_DISPLAY_LABEL, lang)) \
                    or field.get(COLUMN_ES_FIELD) or field.get(COLUMN_DISPLAY_LABEL, "")
            es_field = field[COLUMN_ES_FIELD]
            if label not in exclude_fields:
                if page.get(es_field):
                    if isinstance(page[es_field], list):
                        # braucht es da darüber teilweise der Titel gebildet wird: lookup = metadata["label"] + metadata["value"]
                        value = ", ".join([p for p in page[es_field] if isinstance(p, str)])
                        if not value:
                            value = ", ".join([p.get("label") for p in page[es_field] if isinstance(p, dict) and p.get("label")])
                    else:
                        value = page[es_field]
                    descr_metadata.append({"label": label, "value": value})

        return descr_metadata

    def get_rdv_edit_view(self, page: str, institution: str, lang: str, exclude_fields = [], inst_spreadsheet_id=""):
        """ extract only those metadatafields to be shown from Object that are defined in Google Spreadsheet

        :param page: metadata for a page
        :param institution: name of institution
        :return: array containing metadata in IIIF Standard
        """

        def sort_order(x):
            try:
                return int(x.get(COLUMN_ORDER, 0))
            except ValueError:
                return 999

        descr_metadata = []
        fields_def = self.get_view_def(institution=institution, inst_spreadsheet_id=inst_spreadsheet_id)
        # todo: show_all_metadata ergänzen
        # all metadata shall be shown, if no spreadsheet id
        if not fields_def and self.s.show_all_metadata:
            filtered_fields = [{COLUMN_ES_FIELD: x} for x in page]
        else:
            filtered_fields = filter(lambda x: x.get(COLUMN_ORDER), fields_def)

        fields = sorted(list(filtered_fields), key=lambda x: sort_order(x))
        for field in fields:
            label = field.get("{} {}".format(COLUMN_DISPLAY_LABEL, lang)) \
                    or field.get(COLUMN_ES_FIELD) or field.get(COLUMN_DISPLAY_LABEL, "")
            es_field = field[COLUMN_ES_FIELD]
            if label not in exclude_fields:
                if page.get(es_field):
                    if isinstance(page[es_field], list):
                        # braucht es da darüber teilweise der Titel gebildet wird: lookup = metadata["label"] + metadata["value"]
                        value = ", ".join([p for p in page[es_field] if isinstance(p, str)])
                        values = [p for p in page[es_field] if isinstance(p, str)]
                    else:
                        value = page[es_field]
                    descr_metadata.append({"label": label, "value": value, "field_id": es_field})

        return descr_metadata

    def get_iiif_view_v3(self, page: str, institution: str, lang: str, exclude_fields = [], inst_spreadsheet_id=""):
        """ extract only those metadatafields to be shown from Object that are defined in Google Spreadsheet

        :param page: metadata for a page
        :param institution: name of institution
        :return: array containing metadata in IIIF Standard
        """

        def sort_order(x):
            try:
                return int(x.get(COLUMN_ORDER, 0))
            except ValueError:
                return 999

        descr_metadata = []
        fields_def = self.get_view_def(institution=institution, inst_spreadsheet_id=inst_spreadsheet_id)
        # todo: show_all_metadata ergänzen
        # all metadata shall be shown, if no spreadsheet id
        if not fields_def and self.s.show_all_metadata:
            filtered_fields = [{COLUMN_ES_FIELD: x} for x in page]
        else:
            filtered_fields = filter(lambda x: x.get(COLUMN_ORDER), fields_def)
        fields = sorted(list(filtered_fields), key=lambda x: sort_order(x))
        for field in fields:
            label = field.get("{} {}".format(COLUMN_DISPLAY_LABEL, lang)) \
                    or field.get(COLUMN_ES_FIELD) or field.get(COLUMN_DISPLAY_LABEL, "")
            es_field = field[COLUMN_ES_FIELD]
            if label not in exclude_fields:
                if page.get(es_field):
                    if isinstance(page[es_field], list):
                        # braucht es da darüber teilweise der Titel gebildet wird: lookup = metadata["label"] + metadata["value"]
                        value = ", ".join([p for p in page[es_field] if isinstance(p, str)])
                    else:
                        value = page[es_field]
                    descr_metadata.append({"label": {"de": [label]}, "value": {"de": [value]}})

        return descr_metadata

    def get_inst_kuerzel(self, page):
        # todo: anpassen
        return page.get("iiif_ini") or "dizas_test"

    def get_manifest_url(self, page):
        manif_id = page.get("manif_id")
        if manif_id:
            if self.s.debug:
                return "http://localhost:5001/{}".format(manif_id)
            else:
                # todo anpassen
                return "https://ub-test-iiifpresentation.ub.unibas.ch/{}".format(manif_id)
        else:
            return ""

    def get_preview_image(self, page):
        return []
        manif_url = self.get_manifest_url(page)
        manifest = requests.get(manif_url).json()
        preview_iiif_url = manifest["sequences"][0]["canvases"][0]["images"][0]["resource"]["service"]["@id"]
        return [preview_iiif_url]

    def get_title(self, page):
        return page.get("Title")

    def get_obj_type(self, page):
        return "tbd"

    def get_rdv_view(self, page_id: str, lang: str):
        """ extract only those metadatafields to be shown for an object that are defined in Google Spreadsheet

        :param page_id: id of a page
        :param lang: language setting for metadata
        :return: array containing metadata for RDV detail page display
        """

        @CacheDecorator()
        def cache_es_obj(page_id: str, es_host=self.es.transport.hosts, proj=self.project) -> dict:
            """
            query es for page metadata
            :param page_id: id of a page
            :return: dict containing metadata from es
            """
            query = {
                "query": {
                    "bool": {
                        "must": {
                            "match": {
                                "_id": page_id
                            }
                        }
                    }
                }
            }
            print(self.s.index_manifest, query, self.es)
            es_results = self.es.search(index=self.s.index_manifest, body=query)

            try:
                results = es_results["hits"]["hits"][0]["_source"]
                return results
            except KeyError:
                return {}
            except IndexError:
                return {}

        page = cache_es_obj(page_id)
        obj_type = self.get_obj_type(page)
        manif_url = self.get_manifest_url(page)
        inst_kuerzel = self.get_inst_kuerzel(page)
        descr_metadata = self.get_iiif_view(page, inst_kuerzel, lang)
        def_metadata = {}

        #todo: anpassen
        #if requests.head(manif_url).ok:
        descr_metadata.append(
            {"label": "IIIF Manifest", "link": manif_url, "value": "Link zum Manifest"})
        def_metadata["iiif_manifest"] = manif_url
        def_metadata["preview_image"] = self.get_preview_image(page)
        def_metadata["title"] = self.get_title(page)
        def_metadata["id"] = page_id
        def_metadata["object_type"] = obj_type

        return {"desc_metadata": descr_metadata, "func_metadata": def_metadata}

    def build_list_view(self, page_id: str, lang: str) -> str:
        rdv_view_fields = self.get_rdv_view(page_id, lang)

class ZasView(ViewGenerator):

    def __init__(self, settings):
        super().__init__(settings)

    def get_inst_kuerzel(self, page):
        return page.get("proj_id")

    def get_obj_type(self, page):
        zas_type = page.get("proj_id")
        if zas_type in ["dizas", "ezas"]:
            return "Zeitungsartikel"
        else:
            return "Dokumentensammlung"

    def get_manifest_url(self, page):
        print(page)
        proj = self.get_inst_kuerzel(page)
        zas_id= page.get("zas_id")
        print([proj, zas_id, "manifest"])
        manif_id =  "/".join([proj, zas_id, "manifest"])

        if manif_id:
            if self.s.debug:
                return "https://localhost:5001/{}".format(manif_id)
            else:
                return "https://ub-test-iiifpresentation.ub.unibas.ch/{}".format(manif_id)
        else:
            return ""

    def get_preview_image(self, page):
        zas_type = page.get("proj_id")
        if page.get("jp2_files"):
            return ["https://ub-sipi.ub.unibas.ch/{}/{}".format("dizas" if zas_type == "dizas_test" else "ezas",page.get("jp2_files")[0])]
        else:
            return []

    def get_title(self, page):
        title = page.get("Titel") or page.get("title") or page.get("descr_all")
        if isinstance(title, list):
            title = ", ".join([t.get("label", "Keine Titel") if isinstance(t, dict) else t for t in title if t])
        return title

    def build_list_view(self, page_id: str, lang: str, search_snippet) -> str:
        rdv_view_fields = self.get_rdv_view(page_id, lang)


class AfrikaportalView(ViewGenerator):

    def get_preview_image(self, page):
        return []

    def get_inst_kuerzel(self, page):
        return page.get("main_institution", [""])[0]

    def get_obj_type(self, page):
        inst = page.get("fct_institution")[0]
        if isinstance(inst, list):
            inst = ", ".join([t for t in inst if t])
        return inst

    def get_title(self, page):
        title = page.get("field_title")
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        return title

    def get_manifest_url(self, page):
        manif_id = "afrikaportal/" + page.get("field_id")[0]
        if manif_id:
            if self.s.debug:
                return "https://127.0.0.1:5001/{}/manifest".format(manif_id)
            else:
                # todo anpassen
                return "https://ub-test-iiifpresentation.ub.unibas.ch/{}/manifest".format(manif_id)
        else:
            return ""

class NewspaperView(ViewGenerator):

    def get_preview_image(self, page):
        return []

    def get_obj_type(self, page):
        inst = page.get("issue_type")
        if isinstance(inst, list):
            inst = ", ".join([t for t in inst if t])
        return inst

    def get_title(self, page):
        title = page.get("date_str")
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        ausgabe = page.get("number")
        if isinstance(ausgabe, list):
            ausgabe = ", ".join([t for t in ausgabe if t])
        return "{} Nr. {}".format(title, ausgabe)

    def get_manifest_url(self, page):
        print(page)
        manif_id = "ub-newspapers/" + page.get("manifest_id")
        if manif_id:
            if self.s.debug:
                return "https://127.0.0.1:5001/{}/manifest".format(manif_id)
            else:
                # todo anpassen
                return "https://ub-test-iiifpresentation.ub.unibas.ch/{}/manifest".format(manif_id)
        else:
            return ""

    def get_manifest_url_impresso(self, page):
        manif_id = "impresso_sb/" + page.get("manifest_id")
        if manif_id:
            if self.s.debug:
                return "https://127.0.0.1:5001/{}/manifest".format(manif_id)
            else:
                # todo anpassen
                return "https://ub-test-iiifpresentation.ub.unibas.ch/{}/manifest".format(manif_id)
        else:
            return ""

class PortraetView(ViewGenerator):

    def get_preview_image(self, page):
        return []

    def get_title(self, page):
        title = page.get("cat_title")
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        return title

    def get_obj_type(self, page):
        obj_type = page.get("cat_type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_manifest_url(self, page):
        manif_id = "digispace_port/" + page.get("sys_id")
        if manif_id:
            if self.s.debug:
                return "https://127.0.0.1:5001/{}/manifest".format(manif_id)
            else:
                # todo anpassen
                return "https://ub-test-iiifpresentation.ub.unibas.ch/{}".format(manif_id)
        else:
            return ""


class UnibasDigiView(PortraetView):
    pass


    def get_manifest_url(self, page):
        if "mets_iiifmanifest" in page:
            return page["mets_iiifmanifest"]
        else:
            manif_id = "digispace/" + page.get("manifest_id")
            if manif_id:
                if self.s.debug:
                    return "https://127.0.0.1:5001/{}/manifest".format(manif_id)
                else:
                    # todo anpassen
                    return "https://ub-test-iiifpresentation.ub.unibas.ch/{}".format(manif_id)
            else:
                return ""