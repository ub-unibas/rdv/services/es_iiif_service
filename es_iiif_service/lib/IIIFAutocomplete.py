import json
from typing import Callable
from copy import deepcopy
from collections import Counter

from elasticsearch import helpers

from es_iiif_service.lib import IIIFUtils, IIIFIniSettings
from cache_decorator_redis_ubit import CacheDecorator


class IIIFAutocomplete(IIIFUtils):

    def __init__(self, settings: IIIFIniSettings, q: str, manifest_ids: str, project: str = "", flex: bool = False) -> None:
        """
        do fulltext query within a manifest, returns iiif annotation list with results
        returns first the full page (_search_es),
        afterwards relevant coords and words are extracted (_filter_text_field)

        :param settings: settings for IIIF Service
        :param q: string to query
        :param manifest_ids: list of manifest ids seperated by ",", limits q to ids which are part of manifest
        :param project: name of project
        :param flex: if manifest is created based on es query and not stable
        """
        super(IIIFAutocomplete, self).__init__(settings, project, flex)
        self.index = self.s.index_fulltext

        self._q = q.lower()
        self._manifest_id = manifest_ids
        self._manifest_ids = self.split_manifest_ids(manifest_ids)

        # mapping
        self._textcoords = self.s.mapping_fulltext["textcoords"]
        self._fulltext = self.s.mapping_fulltext["fulltext"]
        self._manifest_field = self.s.mapping_fulltext["manifest"]

        super().after_init()

        self._autocomplete_suggestions = self._cache_autocomplete()
        self.result = self._build_iiif_autocomplete_response()

    def _cache_autocomplete(self) -> Counter:
        """return autocomplete suggestions either from es or cache and set cache

        :return: Counter with autocomplete results
        """

        @CacheDecorator()
        def cache_setter(self_auto: IIIFAutocomplete, q: str):
            """ nested function so that data from es is also cached and can be accessed via cache

            :param self_auto: pass self-object to cache function
            :param q: query word: is used to build cache key
            :return: data from es either via cache or es call
            """

            @CacheDecorator()
            def cache_getter(self_auto2: IIIFAutocomplete, q: str):
                """ nested function so that data from es is also cached and can be accessed via cache

                :param self_auto2: pass self-object to cache function
                :param q: query word: is used to build cache key
                :return: data from es either via cache or es call
                """
                es_results = self_auto2._search_es()
                es_words = self_auto2._build_word_array(es_results)
                return es_words

            # bigger three because UniversalViewer autocomplete starts with 3 letters,
            # needs to be bigger than 1 otherwise on letter searches (e.g. a) would be used for all other caches
            # because cache_setter for "a" would be ""
            if len(self._q) > 3:
                q_getter = self._q[:-1]
            else:
                q_getter = self._q
            # get last autocomplete cache
            es_words = cache_getter(self_auto, q=q_getter)
            # filter last autocomplete cache to set new cache
            # second filtering to filter cache (cache is always used for one letter shorter words)
            filtered_words = self_auto._filter_autocomplete_hits(es_words)
            return filtered_words

        return Counter(cache_setter(self, q=self._q))

    def _search_es(self) -> list:
        """
        search es for items containing query word in fulltext
        :return: list of pages containing results
        """
        if self._manifest_ids[0].startswith("$q") and self.s.project.get("request_param") == "True":
            query_term = self._manifest_ids[0][2:]
            filters = {"match": {self.s.mapping_fulltext["fulltext"]: query_term}}
        else:
            filters = {"terms": {self._manifest_field: self._manifest_ids}}

        if self.project == "impresso":
            match_q = {"match": {"pages.fulltext.autocomplete": self._q}}
        else:
            match_q = {"match": {"fulltext.autocomplete": self._q}}

        search_query = {
            "query": {
                "bool": {
                    "must":  [match_q],
                    "filter": {
                        "bool": {
                            "should": filters
                        }
                    }
                }
            }
        }

        if self._manifest_query:
            # search_query = json.loads(self._manifest_query)
            search_query = self._manifest_query
            # todo: anpassen
            self._manifest_query["_source"].append("textcoords")
            for key in ["_source", "aggs", "sort", "track_scores", "track_total_hits"]:
                try:
                    del self._manifest_query[key]
                except KeyError:
                    pass

            search_query.setdefault("query",{}).setdefault("bool",{}).setdefault("must",[]).append(match_q)

        self.logger.debug("query autocomplete: " + json.dumps(search_query))
        results = self.es.search(index=self.index, body=search_query)

        return results

    def _build_word_array(self, es_results: list) -> list:
        """
        filters from textcoords fields only relevant hits within a page

        :param es_results: list of hits from ES
        :return: list containing found words with coords within a canvas_id
        """

        hits = []
        for n, result in enumerate(es_results.get("hits",{}).get("hits",[])):
            # todo: limit to 500 pages
            if n > 500:
                break
            page = result["_source"]
            for page_num, each_page in enumerate(page[self._textcoords]):
                for word in each_page:
                    hits.append(word[0].lower())
        return hits

    def _filter_autocomplete_hits(self, es_words: list) -> list:
        """ sort array and only get those values which start with same value (are hits for autocompletion)

        :param es_words: list of words to be filtered
        :return: list of words that are relevant for autocomplete
        """

        filter_end = self._q[:-1] + chr(ord(self._q[-1]) + 1)
        es_words.extend([self._q, filter_end])
        es_words.sort()
        start = es_words.index(self._q)
        end = es_words.index(filter_end)
        return es_words[start + 1:end]

    def _build_autocomplete_id(self, q: str) -> str:
        """
        build id for autocomplete result
        :param q: string to query
        :return: id-string for search (same as request url)
        """

        return self._build_autocomplete_url(self._manifest_ids) + "?q=" + q

    def _build_iiif_autocomplete_response(self) -> dict:
        """
        builds iiif response to autocomplete request
        :return: dict contatining iiif search termlist
        """

        response = deepcopy(IIIFUtils.iiif_autocomplete)
        response["@id"] = self._build_autocomplete_id(self._q)

        for word, count in self._autocomplete_suggestions.most_common(10):
            autocomplete = {"match": word, "count": count, "label": word + " (" + str(count) + " Treffer)",
                            "search": self._build_search_id(word, self._manifest_ids)}
            response["terms"].append(autocomplete)

        return response

    def get_cache_key(self, *args, fn: Callable = lambda x: x, **kwargs):
        """ get cache key from function arguments and function name ( is called from class CacheDecorator)
            is overwritten to return better cache-keys

        :param self: self can be used to access the object specific arguments from a function
        :param args: positional args from funct
        :param fn: function for which cache decorator shall be used
        :param kwargs: keyword args from funct
        :return: cache key built from object/functions arguments
        """

        cache_key = "_".join(["iiif", self.index + ",".join(self._manifest_ids) + kwargs["q"]])
        return cache_key
