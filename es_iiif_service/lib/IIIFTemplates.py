import json
import csv
import os
import requests
import logging
import urllib.parse
from typing import Callable
from collections import OrderedDict
from copy import deepcopy

from elasticsearch import Elasticsearch
from cache_decorator_redis_ubit import CacheDecorator, NoCache

from es_iiif_service.lib import IIIFIniSettings

r_pre = "?enc_args="

class IIIFUtils:
    """Templates to build iiif objects and functions to build ids and urls for IIIF"""
    iiif_top_collection = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "@id": "",
        "label": "",
        "@type": "sc:Collection",
        "collections": [],
    }

    iiif_collection = {
        "@id": "",
        "@type": "sc:Collection",
        "label": ""
    }

    iiif_collection_manifest = {
        "@id": "",
        "@type": "sc:Manifest",
        "label": ""
    }

    iiif_manifest = {
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "@id": "",
        "@type": "sc:Manifest",
        "label": "",
        "navDate": "",
        "license": "http://rightsstatements.org/vocab/CNE/1.0/",
        "attribution": "University Library Basel",
        "sequences": [],
        "logo": "https://www.unibas.ch/dam/jcr:93abfa0e-58d6-45ed-930a-de414ca40b13/uni-basel-logo.svg"
    }

    iiif_sequence = {
        "@type": "sc:Sequence",
        "canvases": []
    }

    iiif_range = {
        "@id": "",
        "@type": "sc:Range",
        "label": "",
    }

    iiif_canvas = {"@id": "",
                   "@type": "sc:Canvas",
                   "label": "",
                   "height": 0,
                   "width": 0,
                   "images": [],
                   "metadata": []
                   }

    iiif_image = {
        "@id": "",
        "@type": "oa:Annotation",
        "motivation": "sc:painting",
        "on": "",
        "resource": {
            "@type": "dctypes:Image",
            "format": "image/jpeg",
            "height": "",
            "width": "",
            "service": {
                "@context": "http://iiif.io/api/image/2/context.json",
                "@id": "",
                "profile": "http://iiif.io/api/image/2/level2.json"
            }
        }
    }

    iiif_search_service = {
        "@context": "http://iiif.io/api/search/0/context.json",
        "@id": "",
        "profile": "http://iiif.io/api/search/0/search"
    }

    iiif_autocomplete_service = {
        "@id": "",
        "profile": "http://iiif.io/api/search/0/autocomplete"
    }

    iiif_resource = {
        "@id": "",
        "@type": "oa:Annotation",
        "motivation": "sc:painting",
        "resource": {
            "@type": "cnt:ContentAsText",
            "chars": ""
        },
        "on": ""
    }

    iiif_search_hit =  {
      "@type": "search:Hit",
      "annotations": [

      ],
      "before": "",
      "after": ""
    }

    iiif_no_fulltext_resource = {
        "@id": "",
        "@type": "oa:Annotation",
        "motivation": "sc:painting",
        "resource": {
            "@type": "cnt:ContentAsText",
            "chars": ""
        },
        "on": ""
    }

    iiif_response = {
        "@context": "http://iiif.io/api/search/0/context.json",
        "@id": "",
        "@type": "sc:AnnotationList",
        "resources": [],
        "hits": []
    }

    iiif_autocomplete = {
        "@context": "http://iiif.io/api/search/0/context.json",
        "@id": "",
        "@type": "search:TermList",
        "terms": []
    }

    def __init__(self, settings: IIIFIniSettings, project: str, flex: bool = False) -> None:
        """ same attributes for all services (collection, manifest, search, autocomplete)"""
        self.s = settings
        self.use_cache = True if self.s.manifest_cache and self.s.manifest_cache != "False" else False
        self.cache = CacheDecorator if self.use_cache else NoCache
        self.project = project
        self.logger = self.s.logger
        self._get_hosts(self.s.hosts)
        self.search_marker = False
        self._flex =flex
        self.result = {}
        # condition to aggregate to big results to build subcollections
        self.agg_condition = {}

    def after_init(self):
        try:
            self._manifest_query = json.loads(self.get_uuid_query(self.s.base_url, self.project,
                                                       self._manifest_id) if self._flex else "{}")
        except TypeError:
            self._manifest_query = self.get_uuid_query(self.s.base_url, self.project,
                                                                  self._manifest_id) if self._flex else "{}"
    def __call__(self) -> str:
        return self.return_result()

    def return_result(self) -> str:
        return json.dumps(self.result)

    def split_manifest_ids(self, manifest_ids: str) -> list:
        return manifest_ids.split(self.s.project.get("split_char",","))

    def _flex_service(self, service):
        if self._flex:
            return "/flex_{}/".format(service)
        else:
            return "/{}/".format(service)

    def _build_search_url(self, manifests: list) -> str:
        """ return uri for search endpoint

        :param manifests: list of manifest ids
        :return: url for search endpoint
        """
        if isinstance(manifests, list):
            manifest_str = ",".join(manifests)
        else:
            manifest_str = manifests

        if self.s.index_fulltext:
            return self.iiif_service_host + self.project + "/services/" + self.url_encode4es(manifest_str) + self._flex_service("search")
        else:
            return self.iiif_service_host + self.project + "/services/" + self.url_encode4es(manifest_str) + "/metadata_search/"

    def _build_autocomplete_url(self, manifests: list) -> str:
        """ return uri for autocomplete endpoint

        :param manifests: list of manifest ids
        :return: url for search endpoint
        """
        if isinstance(manifests, list):
            manifest_str = ",".join(manifests)
        else:
            manifest_str = manifests
        autocomp_url = self.iiif_service_host + self.project + "/services/" + self.url_encode4es(manifest_str) + self._flex_service("autocomplete")

        if self.s.project.get("request_param") == "True":
            if self.s.url_param.get("manif_field"):
                autocomp_url += "{}manif_field$§${}".format("$~$" if  "?enc_args=" in autocomp_url else "?enc_args=", self.s.url_param.get("manif_field"))

            if self.s.url_param.get("col_filters"):
                autocomp_url += "{}col_filter$§${}".format("$~$" if "?enc_args=" in autocomp_url else "?enc_args=",
                                                      "$,$".join(["{}.{}".format(k, v) for k, v in
                                                                self.s.url_param.get("col_filter").items()]))

        return autocomp_url

    def _get_autocomplete_service(self, manifests: list) -> dict:
        """
        return iiif autocomplete service object
        :param manifests: list of manifest ids
        :return: dict containing iiif search service
        """

        new_autocomplete_service = deepcopy(self.iiif_autocomplete_service)
        new_autocomplete_service["@id"] = self._build_autocomplete_url(manifests)
        return new_autocomplete_service

    def _get_search_service(self, manifests: list) -> dict:
        """
        return iiif search service object
        :param manifests: list of manifest ids
        :return: dict containing iiif search service
        """
        new_service = deepcopy(self.iiif_search_service)

        new_service["@id"] = self._build_search_url(manifests)
        # not for metadata
        if self.s.index_fulltext:
            new_service["service"] = self._get_autocomplete_service(manifests)

        return new_service

    def _homogenize_part(self, part):
        """ remove image server domain from image id, so id can be reused for canves, etc

        :param part:
        :return:
        """
        if isinstance(part, dict):

            part = part.get("url") or part.get("id") or part.get("link")  or part.get("filename")

            part = part.replace("https://", "").replace("http://", "")

        if self.imageserver_host and self.imageserver_host in part:
            return part.split(self.imageserver_host)[1]
        else:
            return part


    def _build_iiif_id(self, iiif_object: str, part: str) -> str:
        """
        builds iiif id for different iiif objects
        :param iiif_object: type of iiif object
        :param part: individual string part for each object
        :return: id string for iiif object
        """

        # todo: besser definieren
        iiif_url = self.iiif_service_host
        if iiif_object == "on_canvas" and self.project != "impresso":
            return iiif_url + "id/canvas/" + self._homogenize_part(part)
        elif iiif_object == "on_canvas":
            return iiif_url + "id/canvas/" + self._homogenize_part(part)
        elif iiif_object == "seq":
            return iiif_url + "id/sequence/" + part
        elif iiif_object == "mediaseq":
            return iiif_url + "id/mediasequence/" + part
        elif iiif_object == "canvas":
            return iiif_url + "id/canvas/" + str(self._homogenize_part(part))
        elif iiif_object == "resource":
            return iiif_url + "id/resource/" + str(part)
        elif iiif_object == "image":
            return iiif_url + "id/image/" + str(part)
        elif iiif_object == "range":
            return iiif_url + "/id/range/" + part
        elif iiif_object == "body":
            return iiif_url + "/id/body/" + str(part)
        else:
            return iiif_url + "missing_id_definition"

    def _build_search_id(self, q: str, manifest_ids: list) -> str:
        """
        build id for search result
        :param q: string to query
        :return: id-string for search (same as request url)
        """
        search_url = self._build_search_url(manifest_ids) + "?q=" + q

        if self.s.project.get("request_param") == "True":
            if self.s.url_param.get("manif_field"):
                search_url += "{}manif_field$§${}".format("?enc_args=", self.s.url_param.get("manif_field"))

            if self.s.url_param.get("col_filters"):
                search_url += "{}col_filter$§${}".format("$~$" if "?enc_args=" in search_url else "?enc_args=",
                                                      "$,$".join(["{}$:${}".format(k, v) for k, v in
                                                                self.s.url_param.get("col_filter").items()]))

        return search_url

    def _build_manifest_url(self, id_: str, page: dict = {}, flex= False) -> str:
        """ build url for manifest

        :param id_: id for manifest
        :param page: example page for which manifest is built
        :return: return url for manifest
        """
        urlpart_manifest = "/manifest/" if not flex else "/flex_manifest/"
        manif_url = self.iiif_service_host + self.project + "/" + self.url_encode4es(str(id_)) + urlpart_manifest
        if self.s.project.get("request_param") == "True":

            if self.s.url_param.get("manif_struct"):
                manif_url += "{}manif_struct$§${}".format("$~$" if "?enc_args=" in manif_url else "?enc_args=",",".join(self.s.url_param.get("manif_struct")))

            if self.s.url_param.get("col_struct"):
                manif_url += "{}manif_field$§${}".format("$~$" if "?enc_args=" in manif_url else "?enc_args=", self.s.url_param.get("col_struct")[-1])

            elif self.s.url_param.get("manif_field"):
                manif_url += "{}manif_field$§${}".format("$~$" if "?enc_args=" in manif_url else "?enc_args=", self.s.url_param.get("manif_field"))


            if hasattr(self,"_collection_filters") and self._collection_filters:
                manif_url += "{}col_filter$§${}".format("$~$" if "?enc_args=" in manif_url else "?enc_args=",
                                                      "$,$".join(["{}$:${}".format(k,v) for k,v in self._collection_filters.items()]))

            elif self.s.url_param.get("col_filters"):
                manif_url += "{}col_filter$§${}".format("$~$" if "?enc_args=" in manif_url else "?enc_args=",
                                                       "$,$".join(["{}$:${}".format(k,v) for k,v in self.s.url_param.get("col_filter").items()]))

        return manif_url

    def _get_col_filters4es(self):
        if self.s.url_param.get("col_filter"):
            return [{"term": {self._get_agg_field(k):v}} for k, v in self.s.url_param.get("col_filter").items()]
        else:
            return []

    def _build_full_manifest_label(self, page_metadata: dict, label) -> str:
        """
        build label from Collection Path, change last collection path to field manifest_label,
        because last collection path is often only an ID

        :param page_metadata: dict containing all metadata from ES for first page
        :return: label string containing full collection path
        """

        structure = deepcopy(self.s.collection_structure)
        del structure[-1]
        # does not work for list values
        # structure.append(self.s.mapping_manifest["manifest_label"])
        descriptors = [",".join(self.r_list(page_metadata,field)) for field in structure]
        descriptors.append(self.tostr(label))
        hierarchy_descriptor = " -- ".join([d for d in descriptors if d])
        return hierarchy_descriptor

    def _build_col_url(self, filters: OrderedDict) -> str:
        """ build url for (sub)collection using filters for subcollections

        :param filters: key, value pair used as filters when creating collection
        :return: uri for collections
        """

        # also it is an Ordered-Dict as all values are appended at the end,
        # collection structure is needed to have correct order with "all"-values
        ordered_filters = []
        for key in self.s.collection_structure:
            if key in filters:
                ordered_filters.append(self.url_encode4es(filters[key]))
        if ordered_filters:
            col_url = self.iiif_service_host + self.project + "/collection/" + "/".join(ordered_filters) + "/"
        else:
            col_url = self.iiif_service_host + self.project + "/collection/"

        if self.s.project.get("request_param") == "True":
            if self.s.url_param.get("col_struct"):
                col_url += "?enc_args=col_struct$§${}".format("$,$".join(self.s.url_param.get("col_struct")))

            if self.s.url_param.get("manif_struct"):
                col_url += "{}manif_struct$§${}".format("$~$" if "?enc_args=" in col_url else "?enc_args=",",".join(self.s.url_param.get("manif_struct")))

        return col_url

    def _build_col_label(self, filters: dict, manifest_filter: list) -> str:
        """ build label for collections

        :param filters: key, value pair used as filters when creating collection
        :return: label for collections
        """

        # when manifest filters are used (collections out of set of manifest ids is build)
        # , no coharent label can be built therefore use Sammelmappe
        if hasattr(self, "manifest_filter") and manifest_filter:
            return "Sammelmappe " + self.project
        else:
            return " ".join(filters.values())

    def _get_agg_field(self, field: str) -> str:
        """ return fieldname for aggregation (not analyzed field)
        -> based on assumptions that those fields are named .keyword

        :param field: name of field to check if it has a keyword subfield
        :return: name of aggregation field
        """

        if self._check_keyword_field(field):
            return field + ESAggTemplates.keyword_subfield
        else:
            return field

    def _get_hosts(self, hosts: dict) -> None:
        """expand different hosts from settings dict to attributes

        :param hosts: dict from settings containing different hosts
        """
        es_host = hosts["es_host"]
        logging.info("Elasticsearch Host: " + es_host)
        self.es = Elasticsearch(es_host)
        self.iiif_service_host = self.s.base_url or hosts["iiif_service_host"]
        self.seealso_host  = hosts.get("seealso_host","")
        self.imageserver_host  = hosts["imageserver_host"]

    @staticmethod
    def _build_ordered_set(array1: list, array2: list) -> list:
        """ merges two lists (each value can only exist once), order is kept (order from array1 is dominant)

        :param array1: first array to merge, order is dominant
        :param array2: second array to merge, order is kept, when value does not exist in array1
        :return: list which only contains each value once but keeps order from array1
        """
        odict1 = OrderedDict([(key, 1) for key in array1])
        odict2 = OrderedDict([(key, 1) for key in array2])
        odict1.update(odict2)
        return list(odict1.keys())

    def _check_keyword_field(self, field: str) -> bool:
        """check if keyword field is available for aggs, if not use normal field

        :param field: name of field in ES
        :return: false or true if .keyword subfield exists
        """

        keyword_exists = {
            "size": 0,
            "query": {
                "exists": {"field": field + ".keyword"}
            }
        }

        keyword_results = self.es.search(index=self.index, body=keyword_exists)

        # adaptation for es7
        if isinstance(keyword_results["hits"]["total"], dict):
            keyword_results["hits"]["total"] = keyword_results["hits"]["total"]["value"]
        if keyword_results["hits"]["total"] > 0:
            return True
        else:
            return False

    def get_metadata(self, sys_id: str) -> dict:
        """
        read metadata from csv file and extract metadata for sys number
        :param sys_id: sysnumber without dsv prefix
        :return: key, value dict with metadta for sys id
        """

        # CacheDecorator might not keep Order as long as json?
        # @CacheDecorator(socket=self.s.cache_socket, cache=True, db=3, exp_time=self.s.exp_time)
        def cache_metadata(self):
            metadata_dict = {}
            try:
                with open(self.s.project.get("metadata_file","")) as metadata_file:
                    csvreader = csv.DictReader(metadata_file)
                    for line in csvreader:
                        if not line or not line.get("Systemnummer","").strip():
                            continue
                        new_line = OrderedDict()
                        extr_sys_id = str("{:09d}".format(int(line.get("Systemnummer",""))))
                        if extr_sys_id:
                            # because before Python 3.6 csvDictReader does not return OrderedDict
                            for field in csvreader.fieldnames:
                                new_line[field] = line.get(field,"")
                            metadata_dict[extr_sys_id] = new_line
                return metadata_dict
            except FileNotFoundError:
                self.logger.warning("File {} could not be opened!".format(self.s.project.get("metadata_file","")))
                return metadata_dict

        metadata = cache_metadata(self)
        return metadata.get(sys_id,{})

    def get_cache_key(self, *args, fn: Callable = lambda x: x, **kwargs) -> str:
        """ get cache key from function arguments and function name ( is called from class CacheDecorator)
            is overwritten to return better cache-keys

        :param self: self can be used to access the object specific arguments from a function
        :param args: positional args from funct
        :param fn: function for which cache decorator shall be used
        :param kwargs: keyword args from funct
        :return: cache key built from object/functions arguments
        """

        fn = fn
        try:
            func_name = fn.__name__
        except AttributeError:
            func_name = "none"
        if func_name == "cache_metadata":
            cache_key = "_".join(["iiif", self.s.project.get("metadata_file",""), func_name])
        elif func_name == "cache_inst_fields":
            cache_key = "_".join(["iiif", kwargs["institution"][0] , func_name])
        elif func_name == "load_image_dimensions_db":
            cache_path = os.path.join("/tmp", "iiif", self.project)
            cache_key = os.path.join(cache_path, self._manifest_id + ".json")
            if not os.path.exists(cache_path):
                os.makedirs(cache_path)
        else:
            cache_key = "_".join(["iiif", self.project, str(self.es.transport.hosts), self.index + str(kwargs["query"]), func_name])
        return cache_key

    def get_uuid_query(self, host, project, uuid):
        cache = CacheDecorator()
        query = cache.get_keyvalue_cache(uuid)
        # TODO: auskommentiert da Problem mit Kubernets daher direkter Aufruf
        if 0:
            query_url = "{}{}/get_id/{}".format(host, project, uuid)
            query = requests.get(query_url, verify=False).json()
        return query

    @staticmethod
    def url_encode4es(value):
        value = value.replace("\n","%0A")
        value = value.replace("/", "§§§")
        return value


    @staticmethod
    def r_list(dict, key):
        if dict.get(key, ""):
            if isinstance(dict[key], list):
                return [str(x) for x in dict[key]]
            else:
                return [str(dict[key])]
        else:
            return []

    @staticmethod
    def tostr(value):
        if value is None:
            return str(value)
        if isinstance(value, list):
            return " -- ".join(set([str(v) for v in value]))
        if isinstance(value, dict):
            return " -- ".join(set([str({k: value[k]}) for k in sorted(value.keys())]))
        if isinstance(value, int) or isinstance(value, float):
            return str(value)
        else:
            return value

    @classmethod
    def extend_list(cls, pages, key):
        extended = []
        for page in pages:
            extended.extend(cls.r_list(page, key))
        return extended

class ESAggTemplates:
    """Templates for querying ES"""
    keyword_subfield = ".keyword"

    @staticmethod
    def number_es_field(key: str, value: str) -> dict:
        """generate aggregations query for numbers"""
        es_field = {
            "range": {
                key: {
                    "gte": value,
                    "lt": int(value) + 1
                }
            }
        }
        return es_field

    @staticmethod
    def date_es_field(key: str, value: str) -> dict:
        """generate aggregations query for dates"""
        es_field = {
            "range": {
                key: {
                    "gte": value,
                    "lt": value + "||+1y"
                }
            }
        }
        return es_field
