import os
import json
import requests
from copy import deepcopy
from collections import OrderedDict

from es_iiif_service.lib import IIIFUtils_v3, IIIFIniSettings

class IIIFManifest_v3(IIIFUtils_v3):

    def __init__(self, settings: IIIFIniSettings, manifest_ids: str  = "", project: str = "", flex: bool = False) -> None:
        """
        build IIIF Manifest from ES data

        :param settings: settings loaded from settings file for a project
        :param manifest_ids: list of manifest ids seperated by "," for which a manifest shall be built
            (one manifest can be built out of many manifests)s
        :param project: name of project
        :param flex: if manifest is created based on es query and not stable
        """

        super(IIIFManifest_v3, self).__init__(settings, project, flex)
        self.index = self.s.index_manifest

        self._collection_class = self.s.collection_class
        self._page_limit = self.s.page_limit
        self._search_service_exists = True if self.s.index_fulltext or self.s.index_metadata_search else False
        self._sort_fields = self.s.manifest_sort
        self._manifest_id = manifest_ids.replace("§§§","/")
        self._manifest_ids = self.split_manifest_ids(self._manifest_id)

        # mapping
        self._manifest_id_field = self.s.mapping_manifest["manifest_id"]
        self._manifest_label_field = self.s.mapping_manifest["manifest_label"]
        self._canvas_id_field = self.s.mapping_manifest["page_id"]
        self._image_width_field = self.s.mapping_manifest["pagewidth"]
        self._image_height_field = self.s.mapping_manifest["pageheight"]

        # if a manifest is built for more than one ID, the metadata has to be generated differently
        if len(self._manifest_ids) > 1:
            # e.g the label field has to be included in the structure
            self.s.manifest_structure = self._build_ordered_set(self.s.collection_structure, self.s.manifest_structure)
            self.multiple_ids_marker = 1
        else:
            self.multiple_ids_marker = 0

        if self._manifest_ids[0].startswith("$q") and self.s.project.get("request_param") == "True":
            self.search_marker = True
        else:
            self.search_marker = False
        super().after_init()


    def __call__(self) -> str:
        self.result = self._build_manifest()
        return self.return_result()

    def _get_manifest_query(self, limit: int, terms_manifest_id_field: str) -> dict:
        """ to be overwritten, define query to get all data for a manifest

        :param limit: max number of pages to be returned
        :param terms_manifest_id_field: es field name to be used to limit query to a certain manifest
        :return: es query dict
        """

        if self.search_marker:
            query_term = self._manifest_ids[0][2:]
            manifest_query = {
                "size": limit,
                # todo: aus ini auslesen
                "_source": {"excludes": ["*fulltext", "textcoords*"]},
                "query": {
                    "bool": {
                        "must": [
                                    {"exists": {"field": self._canvas_id_field}},
                                    {"match": {self.s.mapping_fulltext["fulltext"]: query_term}}

                                ] + self._get_col_filters4es()
                    }
                }
            }

        else:

            manifest_query = {
                "size": limit,
                # todo: aus ini auslesen
                "_source": {"excludes": ["*fulltext", "textcoords*"]},
                "query": {
                    "bool": {
                        "must": [
                            {"exists": {"field": self._canvas_id_field}},
                            {"terms": {terms_manifest_id_field: self._manifest_ids}}

                        ]+ self._get_col_filters4es()
                    }

                }
            }

        return manifest_query

    def build_same_size_buckets(self, agg_results):
        buckets = []
        aggr_counter = 0
        agg_start = ""
        agg_end = ""
        for b in sorted(agg_results["aggregations"]["filtered_Quelldatum"]["buckets"],
                        key=lambda x: x["key_as_string"]):
            # todo: wert anpassen, max ist 9999 durch suche
            if aggr_counter < 700 and aggr_counter > 0:
                aggr_counter += b["doc_count"]
                agg_end = b["key_as_string"]
            else:
                if agg_end:
                    buckets.append({"agg_start": agg_start, "agg_end": agg_end, "aggr": aggr_counter})
                agg_start = b["key_as_string"]
                aggr_counter = b["doc_count"]
        else:
            agg_end = b["key_as_string"]
            buckets.append({"agg_start": agg_start, "agg_end": agg_end, "aggr": aggr_counter})
        return buckets

    # for caching results
    def post(self):
        pass

    def _define_subcat_buckets(self, buckets, query):
        results = {"hits": {"hits": []}}
        for bucket in buckets:
            bucket_filtet = {"filter": {
                "bool": {"must": [{"range": {"Quelldatum": {"gte": bucket["new_start"], "lte": bucket["new_end"]}}}]}}}
            bucket_query = deepcopy(query)
            bucket_query["query"]["bool"].update(bucket_filtet)
            bucket_result = self.es.search(index=self.index, body=bucket_query)

            # todo: url anpassen
            # todo define: bucket_label, flex_manifest, agg_start, agg_end,

            store_url = self.iiif_service_host + "/zas_flex_manifest/store_id/"
            store_id = requests.post(store_url, json=bucket_query, verify=False).json()["dyn_manif_id"]
            for result in bucket_result["hits"]["hits"]:
                result["_source"]["bucket_label"] = "{}/{}".format(bucket["new_start"], bucket["new_end"])
                result["_source"]["flex_manifest"] = store_id.strip()
            results["hits"]["hits"].extend(bucket_result["hits"]["hits"])
        return results

    def _build_subsets(self, query: dict):

        @self.cache()
        def cache_subsets(self_manifest: IIIFManifest_v3, query: dict, es_host=self.es.transport.hosts,
                     proj=self.project) -> list:
            """ nested function so that data from es is also cached and can be accessed via cache

            :param self_manifest: pass self-object to cache function
            :param query: query to build cache key
            :param es_host: to build different cache for each host
            :param proj: to build different cache for each proj (e.g. ezas)
            :return: data from es either via cache or es call
            """

            agg_query = deepcopy(query)
            agg_query.update(self_manifest.agg_condition)
            agg_results = self.es.search(index=self.index, body=agg_query)

            buckets = self.build_same_size_buckets(agg_results)
            bucketed_results = self._define_subcat_buckets(buckets, query)
            pages = self_manifest._build_canvas_structure(bucketed_results)
            pages_dict = {p["flex_manifest"]:p for p in pages}
            return pages_dict

        return cache_subsets(self, query=query)

    def _get_manifest_pages(self, limit: int = 9999) -> list:
        """get all pages for a manifest

        :param limit: max number of pages to be returned
        :return: list of pages (dict containing information for every page from ES)
        """

        terms_manifest_id_field = self._get_agg_field(self._manifest_id_field)
        manifest_query = self._manifest_query or self._get_manifest_query(limit, terms_manifest_id_field)
        self.logger.debug("query manifest on index {}: {}".format(self.index, json.dumps(manifest_query)))

        @self.cache()
        def cache_es(self_manifest: IIIFManifest_v3, query: dict, es_host=self.es.transport.hosts, proj=self.project) -> list:
            """ nested function so that data from es is also cached and can be accessed via cache

            :param self_manifest: pass self-object to cache function
            :param query: query to build cache key
            :param es_host: to build different cache for each host
            :param proj: to build different cache for each proj (e.g. ezas)
            :return: data from es either via cache or es call
            """
            # ACHTUNG Unterschied zur score Sortierung in der normalen Listenansicht!!!!: dfs_query_then_fetch so order stays the same -> important for uv scroll views
            results = self_manifest.es.search(index=self.index, body=query, search_type="dfs_query_then_fetch")
            pages = self_manifest._build_canvas_structure(results)
            return pages

        count_query = {"query": deepcopy(manifest_query["query"])}
        count = self.es.count(index=self.index, body=count_query)
        # todo: wert anpassen, max ist 9999 durch suche

        if count["count"] > 700 and self.agg_condition:
            self.logger.info(
                "Subaggregtion with condition {} for limit {} is performed".format(self.agg_condition,
                                                                                         count["count"]))
            pages_dict = self._build_subsets(query=manifest_query)
            return pages_dict
        else:
            self.logger.info(
                "Subaggregtion with condition {} for limit {} is not performed".format(self.agg_condition,
                                                                                         count["count"]))
            manifest_query["_source"]= {"excludes": ["*fulltext", "textcoords*"]}
            pages_array = cache_es(self, query=manifest_query)
            return pages_array

    def _build_canvas_structure(self, results):
        pages = []
        for result in results["hits"]["hits"]:
            # add painless script fields
            result["_source"].update(result.get("fields", {}))
            source = json.dumps(result["_source"])
            source_b = source.encode('utf-8')
            source_dict = json.loads(source_b.decode())
            # if pages are stored as a list within one ES entry, create a copy of all metadata for each page
            if isinstance(source_dict.get(self._canvas_id_field, ""), list):
                for n, value in enumerate(source_dict[self._canvas_id_field]):
                    source_copy = deepcopy(source_dict)
                    source_copy[self._canvas_id_field] = value
                    # get same position from width and height array for a specific image
                    try:
                        source_copy[self._image_width_field] = int(source_dict[self._image_width_field][n])
                        source_copy[self._image_height_field] = int(source_dict[self._image_height_field][
                            n])
                    except (KeyError, TypeError):
                        pass
                    pages.append(source_copy)
            else:
                pages.append(source_dict)
        return pages

    def _build_structure(self, pages: list, manifest_structure: list, filters: OrderedDict = OrderedDict()) -> list:
        """build array containing iiif ranges for building internal structure of a manifest

        :param pages: list of pages within a structure level
        :param manifest_structure: structure levels which are underneath the set filters
        :param filters: filters which shall be applied to pages
        :return: array containing iiif ranges
        """

        structure = []
        # get field for building next structure level and all possible values for this structure level
        struct_field = manifest_structure[0]
        dist_values = set([str(page.get(struct_field,"")) for page in pages])

        # array containing fields for deeper structure levels
        deeper_structure = manifest_structure[1:]

        for value in sorted(dist_values):
            copy_filters = deepcopy(filters)
            copy_filters[struct_field] = value
            # get only images which are part of the same branch
            filtered_pages = self._apply_restrictions(pages, copy_filters)

            if filtered_pages:
                copy_range = deepcopy(IIIFUtils_v3.iiif_range)
                copy_range["id"] = self._build_iiif_id("range", self._build_range_id(copy_filters))
                copy_range.update(self._build_lang_map_l(value))

                # if there are no further structure level add reference to canvas ids
                if not deeper_structure:
                    copy_range["canvases"] = [self._build_iiif_id("canvas", filtered_page[self._canvas_id_field]) for
                                              filtered_page in
                                              filtered_pages]
                # else add reference to next structure levels (ranges)
                else:
                    # no filters = top level
                    if not filters:
                        copy_range["behavior"] = "top"
                    else:
                        copy_range["partOf"] = [self._build_partof_map(self._build_iiif_id("range", self._build_range_id(filters)))]
                    next_struct_field = deeper_structure[0]
                    next_dist_values = set([str(filtered_page.get(next_struct_field,"")) for filtered_page in filtered_pages])
                    for next_value in sorted(next_dist_values):
                        next_pages = list(filter(lambda x: x.get(next_struct_field,"") == next_value, filtered_pages))
                        new_filters = deepcopy(copy_filters)
                        new_filters[next_struct_field] = next_value
                        if next_pages:
                            copy_range.setdefault("ranges", []).append(
                                self._build_iiif_id("range", self._build_range_id(new_filters)))
                structure.append(copy_range)
                # add ranges for deeper structures
                if deeper_structure:
                    structure.extend(self._build_structure(filtered_pages, deeper_structure, copy_filters))

        return structure

    def _build_structure2(self, pages: list, manifest_structure: list, filters: OrderedDict = OrderedDict()) -> list:
        """build array containing iiif ranges for building internal structure of a manifest

        :param pages: list of pages within a structure level
        :param manifest_structure: structure levels which are underneath the set filters
        :param filters: filters which shall be applied to pages
        :return: array containing iiif ranges
        """

        structure = []
        # get field for building next structure level and all possible values for this structure level
        struct_field = manifest_structure[0]
        dist_values = set(self.extend_list(pages, struct_field))

        # array containing fields for deeper structure levels
        deeper_structure = manifest_structure[1:]
        for value in sorted(dist_values):
            copy_filters = deepcopy(filters)
            copy_filters[struct_field] = value
            # get only images which are part of the same branch
            filtered_pages = self._apply_restrictions2(pages, copy_filters)

            if filtered_pages:
                copy_range = deepcopy(IIIFUtils_v3.iiif_range)
                copy_range["id"] = self._build_iiif_id("range", self._build_range_id(copy_filters))
                copy_range.update(self._build_lang_map_l(value))

                # if there are no further structure level add reference to canvas ids
                if not deeper_structure:

                    copy_range["items"] = [{"id": self._build_iiif_id("canvas", self._get_page_id(filtered_page)), "type": "Canvas"} for
                                              filtered_page in
                                              filtered_pages]
                # else add reference to next structure levels (ranges)
                else:
                    # no filters = top level
                    if not filters:
                        copy_range["behavior"] = "top"
                    else:
                        copy_range["partOf"] = [self._build_partof_map(self._build_iiif_id("range", self._build_range_id(filters)))]
                    next_struct_field = deeper_structure[0]
                    next_dist_values = set(self.extend_list(filtered_pages, next_struct_field))
                    for next_value in sorted(next_dist_values):
                        #auf in umändern
                        next_pages = list(filter(lambda x: next_value in self.r_list(x,next_struct_field), filtered_pages))
                        new_filters = deepcopy(copy_filters)
                        new_filters[next_struct_field] = next_value
                        if next_pages:
                            copy_range.setdefault("items", []).append(
                                {"id": self._build_iiif_id("range", self._build_range_id(new_filters)), "type": "Range", "label": self._build_lang_map(next_value)})
                structure.append(copy_range)
                # add ranges for deeper structures
                if deeper_structure:
                    structure.extend(self._build_structure2(filtered_pages, deeper_structure, copy_filters))

        return structure

    @staticmethod
    def _apply_restrictions2(pages: list, filters: OrderedDict) -> list:
        """ filter pages which have the same value for all structure levels

        :param pages: array of pages
        :param filters: dictionary containing field names and values, used as filters for pages
        :return: array of pages which pass filters
        """
        filtered_pages = []

        for page in pages:
            for field, value in filters.items():
                if value not in IIIFManifest_v3.r_list(page, field):
                    break
            else:
                filtered_pages.append(page)

        return filtered_pages

    @staticmethod
    def _apply_restrictions(pages: list, filters: OrderedDict) -> list:
        """ filter pages which have the same value for all structure levels

        :param pages: array of pages
        :param filters: dictionary containing field names and values, used as filters for pages
        :return: array of pages which pass filters
        """
        filtered_pages = []

        for page in pages:
            for field, value in filters.items():
                if page.get(field,"") != value:
                    break
            else:
                filtered_pages.append(page)

        return filtered_pages

    def _build_range_id(self, filters: OrderedDict) -> str:
        """ build an id for each range based on the filters

        :param filters: filters used to build a substructure
        :return: id string for iiif range
        """

        return "/".join(list(filters.values()))

    def _get_within_collection_filters(self, page_metadata: dict) -> OrderedDict:
        """ build collection filters from page_metadata values and collection structure

        :param page_metadata: dict containing metadata for a page of a manifest
        :return: dictionary cotaining field, value filters
        """

        collection_filters = OrderedDict()
        for field in self.s.collection_structure[:-1]:
            value = page_metadata.get(field, "")
            if isinstance(value,list):
                value = ",".join(value)
            collection_filters[field] = str(value)
        return collection_filters

    def get_image_dimensions(self, page, page_id, image_dimensions_db={}) -> dict:
        return image_dimensions_db[page_id]

    def _build_manifest(self) -> dict:
        """ create a manifest or if too many different manifests shall be included collections

        :return: dict containing iiif Manifest
        """

        pages = self._get_manifest_pages()
        #print(tuple([x.get(field, ["2020"]) or "2020" for x in pages for field in self._sort_fields]))
        # isinstance dict means that data is used for building subcollections
        if self._sort_fields and not isinstance(pages, dict):
            pages.sort(key=lambda x: str(tuple([x.get(field, ["zzz"]) or ["zzz"] for field in self._sort_fields])))
        # if it is only one manifest or the amount of pages is not higher than the page_limit create one manifest
        # TODO: wieder umstellen, multiple ids marker funktioniert momentan nicht
        # (len(pages) <= self._page_limit and not self.multiple_ids_marker) and
        if not isinstance(pages, dict):
            if pages:
                # use first page to extract metadata
                page_metadata = pages[0]
                col_filters = self._get_within_collection_filters(page_metadata)
                # warum??? todo
                import pprint
                #pprint.pprint(page_metadata)
                #manifest_id = str(page_metadata[self._manifest_id_field])

                new_sequence = deepcopy(self.iiif_sequence)
                new_sequence["id"] = self._build_iiif_id("seq", self._manifest_id)

                # get new manifest with metdata
                new_manifest = self._get_new_manifest(page_metadata,self._manifest_id)
                new_manifest["id"] = self._build_manifest_url(self._manifest_id, page_metadata)
                new_manifest["partOf"] = [self._build_partof_map(self._build_col_url(col_filters))]

                new_manifest["structures"] = self._build_structure2(pages, self.s.manifest_structure)

                if self._search_service_exists:
                    new_manifest["service"] = [self._get_search_service(self._manifest_id)]
                # todo herausfinden wie ich Cache-Loader blockiere
                @self.cache()
                def load_image_dimensions_db(self_manifest):
                    image_dimensions_db = {}
                    for n, page in enumerate(pages):
                        image_link = self._get_resource_path(page)
                        page_id = page[self_manifest._canvas_id_field]

                        # get image dimensions if stored in ES otherwise try to get info from IIIFimage server
                        try:
                            width = page[self_manifest._image_width_field]
                            height = page[self_manifest._image_height_field]
                        except KeyError:
                            self.logger.debug("No Image dimensions in ES available for " + image_link +
                                              ", use IIIF Image Server to extract dimensions.")
                            try:
                                # needs to much time at the moment
                                return {}
                                image_response = requests.get(image_link + "/info.json", verify=False, timeout=2)
                                if image_response.ok:
                                    image_info =  image_response.json()
                                    width = image_info["width"]
                                    height = image_info["height"]
                                else:
                                    raise requests.exceptions.ConnectionError
                            except KeyError:
                                self.logger.warning(
                                    "No Image dimensions in IIIF Image Server available neither " + image_link)
                                return {}
                            except requests.exceptions.ReadTimeout:
                                self.logger.warning(
                                    "IIIF Image Server timout " + image_link)
                                return {}
                            except requests.exceptions.MissingSchema:
                                self.logger.critical(
                                    "IIIF Image URL does not have URL Schema " + image_link)
                                return {}
                            except requests.exceptions.ConnectionError:
                                import traceback
                                traceback.print_exc()
                                self.logger.critical(
                                    "No connection to IIIF Image Server " + image_link)
                                return {}
                        image_dimensions_db[page_id] = {"width": width, "height": height}
                    return image_dimensions_db

                # todo: wieder einkommentieren
                # image_dimensions_db = load_image_dimensions_db(self)
                for n, page in enumerate(pages):
                    new_canvas = self._build_content(new_manifest, page, page_metadata, image_dimensions_db = {})
                    new_manifest["items"].append(new_canvas)
                return new_manifest

            else:
                self.logger.info("Manifest does not exist: " + self._manifest_id)
                return {}
        # else split in multiple subcollections
        else:
            self.s.collection_structure = [self._manifest_id_field]
            # todo: anpassen: multiple ids Möglichkeit wieder aufnhemen
            # new_collection = self._collection_class(settings=self.s, project=self.project, manifest_filter=self._manifest_ids)
            new_collection = self._collection_class(settings=self.s, project=self.project,
                                                    collection_data=pages)
            return new_collection.result

    def _build_content(self, new_manifest, page, page_metadata, image_dimensions_db):
        resource_link = self._get_resource_path(page)
        file_type = resource_link.split(".")[-1].lower()
        if file_type in ["mp3"]:
            return self._build_canvas(new_manifest, page, page_metadata, image_dimensions_db, type_="Sound", format="audio/mp3")
        elif file_type in ["jpg", "tif"]:
            return self._build_canvas(new_manifest, page, page_metadata, image_dimensions_db, type_="Image", format="image/jpeg", service="iiif")
        elif file_type in ["mp4"]:
            return self._build_canvas(new_manifest, page, page_metadata, image_dimensions_db, type_="Video", format="video/mp4")
        elif file_type in ["pdf"]:
            return self._build_canvas(new_manifest, page, page_metadata, image_dimensions_db, type_="Document",
                                  format="application/pdf")
        else:
            return self._build_canvas(new_manifest, page, page_metadata, image_dimensions_db, type_="Image", format="image/jpeg", service="iiif")

    def _get_duration(self, page):
        """return duration of av objects, tbo
        returns {"duration": seconds}
        """
        return {"duration": 0}

    def _get_image_dimensions(self, page):
        image_dimensions = {"width": 0, "height": 0}

        try:
            width = page[self._image_width_field]
            height = page[self._image_height_field]
            image_dimensions = {"width": width, "height": height}
        except (TypeError, KeyError):
            import traceback
            traceback.print_exc()
            pass
        return image_dimensions

    def _build_canvas(self, new_manifest, page, page_metadata, image_dimensions_db, type_, format, service="None"):

        image_link = self._get_resource_path(page)
        page_id = self._get_page_id(page)

        new_canvas = self._get_new_canvas(page)
        new_canvas["id"] = self._build_iiif_id("canvas", page_id)

        if type_ in ["Sound", "Video"]:
            new_canvas.update(self._get_duration(page))
        elif type_ in ["Image"]:
            new_canvas.update(self._get_image_dimensions(page))
            new_canvas["thumbnail"] = [{
                "id": image_link,
                "type": "Image",
            }]

        new_resource = deepcopy(self.iiif_resource_annotation)
        new_resource["id"] = self._build_iiif_id("annlist", page_id)
        new_resource["items"][0]["id"] = self._build_iiif_id("body", page_id)
        new_resource["items"][0]["body"]["type"] = type_
        new_resource["items"][0]["body"]["format"] = format
        new_resource["items"][0]["body"]["id"] = image_link
        new_resource["items"][0]["target"] = self._build_iiif_id("canvas", page_id)

        if service == "iiif":
            iiif_image_service = deepcopy(self.iiif_image_service)
            iiif_image_service["id"] = image_link
            new_canvas["thumbnail"][0].setdefault("service",[]).append(iiif_image_service)
            new_canvas["thumbnail"][0]["id"] = image_link + "/full/200,/0/default.jpg"
            new_resource["items"][0]["body"].setdefault("service",[]).append(iiif_image_service)
            new_resource["items"][0]["body"]["id"] = image_link + "/full/200,/0/default.jpg"

        new_canvas["items"] = [new_resource]
        new_canvas["annotations"] = self.get_annotations(page)

        # deduplication of metadata from manifest and canvas level
        metadata_manifest = [str(meta_manif["label"]) + self.tostr(meta_manif["value"])
                             for meta_manif in new_manifest.get("metadata", [])]
        metadata_canvas = []
        for metadata in new_canvas["metadata"]:
            lookup = str(metadata["label"]) + self.tostr(metadata["value"])
            if lookup not in metadata_manifest:
                metadata_canvas.append(metadata)
        new_canvas["metadata"] = metadata_canvas

        return new_canvas

    def get_annotations(self, page):
        return []

    def calculate_xywh(self, page):
        region = page.get('iiif_region').replace("pct:", "")
        region_perc = region.split(",")
        width = page[self._canvas_id_field][self._image_width_field]
        height = page[self._canvas_id_field][self._image_height_field]
        xywh_coords = []
        for n, r in enumerate(region_perc):
            if n % 2 == 0:
                coord = int(width) * float(r) / 100
            else:
                coord = int(height) * float(r) / 100
            xywh_coords.append(str(int(coord)))
        return ",".join(xywh_coords)


    # methods to be overwritten
    def _get_object_type(self, page: dict) -> str:
        """
        to be overwritten: determine object type
        :param page: dict containing all metadata from ES for a page
        :return: object type: sound, video, image, iiif
        """
        return "Image", "iiif"

    def _get_new_manifest(self, page_metadata, label):
        """to be overwritten: return a fresh manifest (already containing metadata)

        :param page_metadata: dict containing all metadata from ES for first page
        :return: dict containing iiif manifest
        """

        new_manifest = deepcopy(self.iiif_manifest)
        new_manifest.update(self._build_lang_map_l(self._build_full_manifest_label(page_metadata, self._manifest_id)))
        return new_manifest

    def _get_new_canvas(self, page: dict) -> dict:
        """
        to be overwritten: return a fresh canvas for a page (already containing metadata)
        :param page: dict containing all metadata from ES for a page
        :return: dict containing iiif canvas
        """
        new_canvas = deepcopy(self.iiif_canvas)
        return new_canvas

    def _get_resource_path(self, page: dict) -> str:
        """
        to be overwritten: get path to page on IIIF Image Server
        :param page: dict containing all metadata from ES for a page
        :return: URL to images
        """
        return page[self._canvas_id_field]

    def _get_page_id(self, page: dict) -> str:
        return page[self._canvas_id_field]