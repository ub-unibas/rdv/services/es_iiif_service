from copy import deepcopy
from urllib import parse
import json
from .IIIFManifest import IIIFManifest

class IIIFManifestPDF(IIIFManifest):
    iiif_manifest = {
        "@context": [
            "http://wellcomelibrary.org/ld/ixif/0/context.json",
            "http://iiif.io/api/presentation/2/context.json"
        ],
        "@id": "",
        "@type": "sc:Manifest",
        "label": "",
        "license": "http://rightsstatements.org/vocab/CNE/1.0/",
        "attribution": "Archiviert und zur Verfügung gestellt vom Schweizerischen Wirtschaftsarchiv SWA",
        "sequences": [],
        "mediaSequences": [],
        "logo": "https://www.unibas.ch/dam/jcr:93abfa0e-58d6-45ed-930a-de414ca40b13/uni-basel-logo.svg"
    }

    iiif_mediaSequences = {
        "@type": "ixif:MediaSequence",
        "elements": [],
        "label": "XSequence 0",
        "@id": "",
    }

    iiif_elements = {
        "@id": "",
        "@type": "foaf:Document",
        "format": "application/pdf",
        "label": "",
        "metadata": [
        ],
    }

    iiif_sequence = {
        "@type": "sc:Sequence",
        "compatibilityHint": "displayIfContentUnsupported",
        "canvases": []
    }

    def __init__(self, **kwargs):
        """build Manifest for Impresso project"""
        super(IIIFManifestPDF, self).__init__(**kwargs)

    def _get_image_path(self, page: dict) -> str:
        subpath = page[self._canvas_id_field]
        linke = subpath.get("link")
        linke = linke.replace("ub2.unibas.ch/", "ub.unibas.ch/")
        return linke

    def _get_image_label(self, page: dict) -> str:
        subpath = page[self._canvas_id_field]
        label = subpath.get("label")
        return label

    def _get_pdf_thumbnail(self, page: dict) -> str:
        return "https://ub-sipi.ub.unibas.ch/logos/logo-swa-mit-farbe.tif/full/200,/0/default.jpg"

    def _build_manifest(self) -> dict:
        """ create a manifest or if too many different manifests shall be included collections

        :return: dict containing iiif Manifest
        """

        pages = self._get_manifest_pages()
        # print(tuple([x.get(field, ["2020"]) or "2020" for x in pages for field in self._sort_fields]))
        # isinstance dict means that data is used for building subcollections
        if self._sort_fields and not isinstance(pages, dict):
            pages.sort(key=lambda x: str(tuple([x.get(field, ["zzz"]) or ["zzz"] for field in self._sort_fields])))

        # if it is only one manifest or the amount of pages is not higher than the page_limit create one manifest
        # TODO: wieder umstellen, multiple ids marker funktioniert momentan nicht
        # (len(pages) <= self._page_limit and not self.multiple_ids_marker) and
        if not isinstance(pages, dict):
            if pages:
                # use first page to extract metadata
                page_metadata = pages[0]
                col_filters = self._get_within_collection_filters(page_metadata)
                # warum??? todo
                manifest_id = str(page_metadata[self._manifest_id_field])

                new_sequence = deepcopy(self.iiif_sequence)
                new_media_sequence = deepcopy(self.iiif_mediaSequences)
                new_media_sequence["@id"] = self._build_iiif_id("mediaseq", self._manifest_id)

                new_sequence["@id"] = self._build_iiif_id("seq", self._manifest_id)

                # get new manifest with metdata
                new_manifest = self._get_new_manifest(page_metadata, self._manifest_id)
                new_manifest["@id"] = self._build_manifest_url(self._manifest_id, page_metadata)
                new_manifest["within"] = self._build_col_url(col_filters)
                new_manifest["sequences"].append(new_sequence)
                new_manifest["mediaSequences"].append(new_media_sequence)

                new_manifest["structures"] = self._build_structure2(pages, self.s.manifest_structure)

                if self._search_service_exists:
                    new_manifest["service"] = self._get_search_service(self._manifest_id)

                image_dimensions_db = {}

                for n, page in enumerate(pages):
                    image_link = self._get_image_path(page)
                    image_label = self._get_image_label(page)
                    page_id = self._get_page_id(page)

                    new_element = deepcopy(self.iiif_elements)
                    new_element["@id"] = image_link
                    new_element["label"] = image_label
                    new_element["thumbnail"] = self._get_pdf_thumbnail(page)

                    image_dimensions = self._get_image_dimensions(page, page_id, image_dimensions_db)
                    new_canvas = self._get_new_canvas(page)
                    new_canvas["@id"] = self._build_iiif_id("canvas", page_id)
                    new_canvas.update(image_dimensions)

                    new_image = deepcopy(self.iiif_image)
                    new_image["@id"] = self._build_iiif_id("image", page_id)
                    new_image["on"] = new_canvas["@id"]
                    new_image["resource"]["service"]["@id"] = image_link
                    new_image["resource"].update(image_dimensions)
                    new_image["resource"]["@id"] = self._build_iiif_id("resource", page_id)

                    new_canvas["images"] = [new_image]
                    new_sequence["canvases"].append(new_canvas)

                    # deduplication of metadata from manifest and canvas level
                    metadata_manifest = [json.dumps(meta_manif, sort_keys=True)
                                         for meta_manif in new_manifest.get("metadata", [])]
                    metadata_canvas = []
                    for metadata in new_canvas["metadata"]:
                        lookup = json.dumps(metadata, sort_keys=True)
                        if lookup not in metadata_manifest:
                            metadata_canvas.append(metadata)
                    new_element["metadata"] = metadata_canvas
                    new_canvas["metadata"] = metadata_canvas
                    new_media_sequence["elements"].append(new_element)
                return new_manifest

            else:
                self.logger.info("Manifest does not exist: " + self._manifest_id)
                return {}
        # else split in multiple subcollections
        else:
            self.s.collection_structure = [self._manifest_id_field]
            # todo: anpassen: multiple ids Möglichkeit wieder aufnhemen
            # new_collection = self._collection_class(settings=self.s, project=self.project, manifest_filter=self._manifest_ids)
            new_collection = self._collection_class(settings=self.s, project=self.project,
                                                    collection_data=pages)
            return new_collection.result

