from .IIIFSettings import IIIFIniSettings, ViewSettings
from .IIIFTemplates import IIIFUtils, ESAggTemplates
from .IIIFTemplates_v3 import IIIFUtils_v3
from .IIIFCollection import IIIFCollection
from .IIIFCollection_v3 import IIIFCollection_v3
from .IIIFManifest import IIIFManifest
from .IIIFManifest_v3 import IIIFManifest_v3
from .IIIFManifestPDF import IIIFManifestPDF
from .IIIFSearch import IIIFSearch
from .IIIFMetadataSearch import IIIFMetadataSearch
from .IIIFAutocomplete import IIIFAutocomplete

from .ViewGenerator import ViewGenerator, ZasView, PortraetView, AfrikaportalView, UnibasDigiView, NewspaperView


