import json
from typing import Callable
from collections import Counter, OrderedDict
from copy import deepcopy

from es_iiif_service.lib import IIIFUtils_v3, ESAggTemplates, IIIFIniSettings
from cache_decorator_redis_ubit import CacheDecorator


class IIIFCollection_v3(IIIFUtils_v3):

    def __init__(self, settings: IIIFIniSettings, collection_path: str = "", project: str = "",
                 manifest_filter: list = None, collection_data: dict = None) -> None:
        """ build IIIF Collection with references to subcollections or manifests

        :param settings: settings loaded from settings file for a project
        :param collection_path: url path containing filters to build (sub-)collection
        :param project: name of project
        :param manifest_filter: array of manifest ids for which (a) collection(s) should be build
        """
        if manifest_filter is None:
            manifest_filter = []
        super(IIIFCollection_v3, self).__init__(settings, project)
        self.index = self.s.index_manifest

        self._sort_fields = self.s.collection_sort
        self._manifest_filter = manifest_filter
        self._canvas_id_field = self.s.mapping_manifest["page_id"]
        # build collection filters (key, value pairs) from uri
        self._collection_filters = self._get_collection_filters(collection_path)

        self._manifest_id_field = self.s.mapping_manifest["manifest_id"]
        self._manifest_label_field = self.s.mapping_manifest["manifest_label"]

        if collection_data:
            self._collection_data = collection_data
        else:
            self._collection_data = {}

        self.result = self._build_collection()

    def _build_man_col_filters(self, aggs_field: str):
        aggs_keyw_field = self._get_agg_field(aggs_field)

        filters = [{"terms": {aggs_keyw_field: self._manifest_filter}},
                   {"exists": {"field": self._canvas_id_field}}]
        return filters

    def _get_manifest_collections(self, aggs_field: str, limit: int = 3000) -> dict:
        """ get aggregations if collection shall be built from manifest ids,
        new submanifests are created if too many pages are in one manifest

        :param aggs_field: name of field for which aggregation shall be done
        :param limit: max amount of sub manifests
        :return: key for each collection level, value: example page for collection level to access metadata
        """

        results = {}

        filters = self._build_man_col_filters(aggs_field)
        query_filter = {"size": limit,
                        "_source": {"excludes": ["*fulltext", "textcoords*"]},
                        "query": {
            "constant_score": {
                "filter": {
                    "bool": {
                        "must": filters
                    }
                }
            }
        }}


        self.logger.debug("query collection (built from manifests): " + json.dumps(query_filter))

        @CacheDecorator()
        def cache_es(self_col: IIIFCollection_v3, query: dict, es_host=self.es.transport.hosts, proj=self.project) -> tuple:
            """ nested function so that data from es is also cached and can be accessed via cache

            :param self_col: pass self-object to cache function
            :param query: query to build cache key
            :return: data from es either via cache or es call
            """
            search_response = self_col.es.search(index=self_col.index, body=query)
            hits = [hit["_source"] for hit in search_response["hits"]["hits"]]
            # todo: so umschreiben, dass er update macht und nicht überschreibt
            d = {key: hit for hit in hits for key in self_col.r_list(hit, aggs_field) }
            m = Counter(self_col.extend_list(hits,aggs_field))
            return d, m

        distinct_results, manifest_ids_amounts = cache_es(self, query=query_filter)

        # split manifests in sub-manifests if there are too many pages
        page_count = 0
        set_manifest_ids = set()
        distant_manifests_metadata = []
        for manifest_id, pages in manifest_ids_amounts.items():
            page_metadata = distinct_results[manifest_id]
            if page_count + pages < self.s.page_limit:
                if manifest_id not in set_manifest_ids:
                    set_manifest_ids.add(manifest_id)
                    distant_manifests_metadata.append(page_metadata)
                page_count += pages
            else:
                results[",".join(set_manifest_ids)] = distant_manifests_metadata
                distant_manifests_metadata = [page_metadata]
                set_manifest_ids = {manifest_id}
                page_count = pages
        else:
            results[",".join(set_manifest_ids)] = distant_manifests_metadata

        return results

    def _build_col_filters(self):
        filters = [{"exists": {"field": self._canvas_id_field}}]
        # todo: ony for memobase


        for key, value in self._collection_filters.items():
            if value == "all":
                continue
            if self._get_agg_field_type(key):
                filters.append(self._get_agg_field_type(key)(key, value))
            else:
                if self._check_keyword_field(key):
                    filters.append({"terms": {key + ESAggTemplates.keyword_subfield: [value]}})
                else:
                    # todo: why? welches szenario?
                    self.logger.warning("Missing field definition for " + key + ". "
                                        + key + ": " + value + " not used for filtering.")
        return filters

    def _get_collections(self, aggs_field: str, limit: int = 3000) -> dict:
        """get all distant issues for a newspaper and a year

        :param aggs_field: name of field for which aggregation shall be done
        :param limit: max amount of sub collections
        :return: dict: key for each collection level, value: example page for collection level to access metadata
        """


        results = {}

        aggs_keyw_field = self._get_agg_field(aggs_field)

        collection_query = {
            "size": 0,
            "aggs": {
                "agg_field": {
                    "terms": {
                        "field": aggs_keyw_field,
                        "size": limit,
                    },
                    "aggs": {
                        "single_hits": {
                            "top_hits": {
                                "_source": {"excludes": ["*fulltext", "textcoords*"]},
                                "size": 1,
                            }
                        }
                    }
                }
            }
        }

        filters = self._build_col_filters()
        if filters:
            query_filter = {"query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": filters
                        }
                    }
                }
            }}
            collection_query.update(query_filter)

        self.logger.debug("query collection: " + json.dumps(collection_query))

        @CacheDecorator()
        def cache_es2(self_col: IIIFCollection_v3, query: dict, es_host=self.es.transport.hosts, proj=self.project) -> dict:
            """ nested function so that data from es is also cached and can be accessed via cache

            :param self_col: pass self-object to cache function
            :param query: query to build cache key
            :return: data from es either via cache or es call
            """
            search_response = self_col.es.search(index=self_col.index, body=query)

            for bucket in search_response["aggregations"]["agg_field"]["buckets"]:
                if bucket["doc_count"]:
                    if "key_as_string" in bucket:
                        results[bucket["key_as_string"]] = bucket["single_hits"]["hits"]["hits"][0]["_source"]
                    else:
                        results[str(bucket["key"])] = bucket["single_hits"]["hits"]["hits"][0]["_source"]
            return results

        return cache_es2(self, query=collection_query)

    def _build_filters(self) -> str:
        """ return aggregation field (=last level or filter which is set to all

        :return: field name used for aggregation
        """

        level = len(self._collection_filters)
        try:
            agg_level = list(self._collection_filters.values()).index("all")
            aggregation_field = self.s.collection_structure[agg_level]
            # todo: why?
            #raise ValueError
        except ValueError:
            aggregation_field = self.s.collection_structure[level]
        return aggregation_field

    def _sort_col(self, item):
        sort_tuple = list()
        for field in self._sort_fields:
            if isinstance(item[1],dict):
                sort_value = item[1].get(field, "")
                sort_tuple.append(sort_value)
            else:
                pass
        if isinstance(item[0],list):
            sort_tuple.append(tuple(item[0]))
        else:
            sort_tuple.append(item[0])
        return tuple(sort_tuple)

    def _build_collection(self) -> dict:
        """ build collection with reference to subcollections or submanifests

        :return: dict containing iiif collection object
        """

        aggregation_field = self._build_filters()
        if self._collection_data:
            aggregated_collections = self._collection_data
            self._manifest_label_field = "bucket_label"
            flex = True
        elif self._manifest_filter:
            aggregated_collections = self._get_manifest_collections(aggregation_field)
        else:
            aggregated_collections = self._get_collections(aggregation_field)
            flex = False

        new_iiif_top_collection = deepcopy(self.iiif_top_collection)
        # instance because of different structure in _get_manifest_collections

        # key=lambda x: str(tuple([x[1].get(field, "") if isinstance(x[1],dict) else "" for field in self._sort_fields]
        # aggregated_collections.update({"männer":{"sort_label": "maenner","label": "männer", "field_1": "M"}})
        for value, page in sorted(aggregated_collections.items(), key=lambda x: self._sort_col(x)):

            if page:
                label = page.get(self._manifest_label_field, "") or value

                param_filters = deepcopy(self._collection_filters)
                # delete last entry to have only filters for parents
                parent_filters = OrderedDict(tuple(self._collection_filters.items())[0:-1])
                # add aggregated value to filters for building labels and urls
                param_filters[aggregation_field] = value
                new_iiif_top_collection["id"] = self._build_col_url(self._collection_filters)
                if self._collection_filters:
                    new_iiif_top_collection["partOf"] = [self._build_partof_map(self._build_col_url(parent_filters))]
                # um topcollection label nicht zu überschreiben
                if self._build_col_label(self._collection_filters, self._manifest_filter):
                    new_iiif_top_collection.update(self._build_lang_map_l(self._build_col_label(self._collection_filters, self._manifest_filter)))

                # if last collection-level -> reference on manifests
                if aggregation_field == self.s.collection_structure[-1]:
                    new_manifest = self._get_new_manifest(page, label)
                    new_manifest["id"] = self._build_manifest_url(value, page=page, flex=flex)
                    new_manifest["partOf"] = [self._build_partof_map(self._build_col_url(self._collection_filters))]
                    new_iiif_top_collection.setdefault("items", []).append(new_manifest)
                # else reference on next collection level
                else:
                    new_collection = deepcopy(self.iiif_collection)
                    new_collection["id"] = self._build_col_url(param_filters)
                    new_collection["partOf"] = [self._build_partof_map(self._build_col_url(self._collection_filters))]
                    new_collection.update(self._build_lang_map_l(self._build_col_label(param_filters, self._manifest_filter)))
                    new_iiif_top_collection.setdefault("items", []).append(new_collection)

        return new_iiif_top_collection

    def _get_collection_filters(self, collection_path: str) -> OrderedDict:
        """ build collection filters from uri path and collection structure
        (position of a value in uri path = position of key/field in collection structure)

        :param collection_path: url string containing filters for collection
        :return: dictionary cotaining field, value filters
        """

        collection_filters = OrderedDict()
        if collection_path:
            sub_collections = collection_path.split("/")
            for field in self.s.collection_structure:
                value = sub_collections.pop(0)
                # if value != "all":
                collection_filters[field] = value
                if not sub_collections:
                    break
        return collection_filters

    def _get_agg_field_type(self, field: str) -> Callable:
        """
        return ES search snippet how aggregation for special fields shall be build
        (e.g. for date: aggregation for every year)

        :param field: field name in es
        :return: dict containing ES search snippet used for building aggregation
        """
        return None

    def _get_new_manifest(self, page: dict, label: str) -> dict:
        """to be overwritten: return a fresh manifest reference to a collection

        :param page: dict containing all metadata from ES for first page
        :param label: key value which was used for grouping
        :return: dict containing iiif manifest
        """
        new_manifest = deepcopy(self.iiif_manifest)

        new_manifest.update(self._build_lang_map_l(self._build_full_manifest_label(page, label)))
        return new_manifest
