from copy import deepcopy
from .IIIFTemplates import IIIFUtils, ESAggTemplates


class IIIFUtils_v3(IIIFUtils):
    """Templates to build iiif objects and functions to build ids and urls for IIIF"""
    iiif_top_collection = {
        "@context": "http://iiif.io/api/presentation/3/context.json",
        "id": "",
        "label": "",
        "type": "Collection",
        "items": [],
    }

    iiif_collection = {
        "id": "",
        "type": "Collection",
        "label": ""
    }

    iiif_collection_manifest = {
        "id": "",
        "type": "Manifest",
        "label": ""
    }

    iiif_manifest = {
        "@context": "http://iiif.io/api/presentation/3/context.json",
        "id": "",
        "type": "Manifest",
        "label": "",
        "items": [],
    }

    iiif_sequence = {
        "canvases": []
    }

    iiif_range = {
        "id": "",
        "type": "Range",
        "label": "",
    }

    iiif_canvas = {"id": "",
                   "type": "Canvas",
                   "label": "",
                   "metadata": []
                   }

    iiif_resource_annotation = {
        "id": "",
        "type": "AnnotationPage",
        "items": [
            {
                "id": "",
                "type": "Annotation",
                "motivation": "painting",
                "target": "",
                "body":
                    {
                        "id": "",
                        "type": "",
                        "format": "",
                    }
            }
        ]
    }

    iiif_image_service = {
            "type": "ImageService2",
            "profile": "http://iiif.io/api/image/2/level2.json",
            "id": ""
    }

    iiif_audio_multiple = {
        "id": "",
        "type": "AnnotationPage",
        "items": [
            {
                "id": "",
                "type": "Annotation",
                "motivation": "painting",
                "target": "",
                "body":
                    {
                        "id": "",
                        "type": "Image",
                        "format": "image/jpeg",
                    }
            }
        ]
    }

    iiif_image = {
        "id": "",
        "type": "AnnotationPage",
        "items": [
            {
                "id": "",
                "type": "Annotation",
                "motivation": "painting",
                "target": "",
                "body":
                    {
                        "id": "",
                        "type": "Image",
                        "format": "image/jpeg",
                        "service": [{
                            "type": "ImageService2",
                            "profile": "http://iiif.io/api/image/2/level2.json",
                            "id": ""
                        }]
                    }
            }]

    }

    iiif_search_service = {
        "@context": "http://iiif.io/api/search/0/context.json",
        "id": "",
        "type": "SearchService1",
        "profile": "http://iiif.io/api/search/0/search"
    }

    iiif_autocomplete_service = {
        "id": "",
        "type": "AutoCompleteService1",
        "profile": "http://iiif.io/api/search/0/autocomplete"
    }

    iiif_resource = {
        "id": "",
        "type": "oa:Annotation",
        "motivation": "painting",
        "resource": {
            "type": "cnt:ContentAsText",
            "chars": ""
        },
        "on": ""
    }

    iiif_no_fulltext_resource = {
        "id": "",
        "type": "oa:Annotation",
        "motivation": "painting",
        "resource": {
            "type": "cnt:ContentAsText",
            "chars": ""
        },
        "on": ""
    }

    iiif_response = {
        "@context": "http://iiif.io/api/search/0/context.json",
        "id": "",
        "type": "AnnotationList",
        "resources": []
    }

    iiif_autocomplete = {
        "@context": "http://iiif.io/api/search/0/context.json",
        "@id": "",
        "@type": "search:TermList",
        "terms": []
    }

    @classmethod
    def _build_partof_map(self, id_, type_="Collection"):
        return {"id": id_, "type": type_}

    @classmethod
    def _build_lang_map_l_v(cls, label, value, lang="de"):
        label = cls._build_lang_map_l(label, lang)
        value = cls._build_lang_map_v(value, lang)
        label.update(value)
        return label

    @classmethod
    def _build_lang_map_s(cls, summary, lang="de"):
        return {"summary": cls._build_lang_map(summary, lang)}

    @classmethod
    def _build_lang_map_l(cls, label_value, lang="de"):
        return {"label": cls._build_lang_map(label_value, lang)}

    @classmethod
    def _build_lang_map_v(cls, value, lang="de"):
        return {"value": cls._build_lang_map(value, lang)}

    @classmethod
    def _build_lang_map(cls, value, lang="de"):
        return {lang: [value]}

    def _get_search_service(self, manifests: list) -> dict:
        """
        return iiif search service object
        :param manifests: list of manifest ids
        :return: dict containing iiif search service
        """
        new_service = deepcopy(self.iiif_search_service)

        new_service["id"] = self._build_search_url(manifests)
        # not for metadata
        if self.s.index_fulltext:
            new_service["service"] = [self._get_autocomplete_service(manifests)]

        return new_service

    def _get_autocomplete_service(self, manifests: list) -> dict:
        """
        return iiif autocomplete service object
        :param manifests: list of manifest ids
        :return: dict containing iiif search service
        """

        new_autocomplete_service = deepcopy(self.iiif_autocomplete_service)
        new_autocomplete_service["id"] = self._build_autocomplete_url(manifests)
        return new_autocomplete_service
