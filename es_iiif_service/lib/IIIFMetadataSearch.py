import json
from copy import deepcopy

from elasticsearch import helpers

from es_iiif_service.lib import IIIFUtils, IIIFIniSettings
from cache_decorator_redis_ubit import CacheDecorator

class IIIFMetadataSearch(IIIFUtils):

    def __init__(self, settings: IIIFIniSettings, q: str, manifest_ids: str, project: str = "") -> None:
        """
        do fulltext query within a manifest, returns iiif annotation list with results
        returns first the full page (_search_es),
        afterwards relevant coords and words are extracted (_filter_text_field)

        :param settings: settings for IIIF Service
        :param q: string to query
        :param manifest_ids: list of manifest ids seperated by ",", limits q to ids which are part of manifest
        :param project: name of project
        """
        super(IIIFMetadataSearch, self).__init__(settings, project)
        self.index = self.s.index_fulltext

        q_parts = [p.strip() for p in q.split(":")]
        if len(q_parts) == 2:
            self._q = q_parts[1]
            self._q_field = q_parts[0]
        else:
            self._q = q.lower()
            self._q_field = ""
        self._manifest_ids = self.split_manifest_ids(manifest_ids)

        # mapping
        self._textcoords = self.s.mapping_fulltext["textcoords"]
        self._fulltext = self.s.mapping_fulltext["fulltext"]
        self._manifest_field = self.s.mapping_fulltext["manifest"]

        self._word_results = self._search_es()
        self.result = self._build_iiif_search_response()

    def _search_es(self) -> list:
        """
        search es for items containing query word in fulltext
        :return: list of pages containing results
        """

        filters = {"terms": {self._manifest_field: self._manifest_ids}}

        search_query = {
            "query": {
                "bool": {
                    "must": {
                        "query_string": {
                            "query": self._q
                        }
                    },
                    "filter": {
                        "bool": {
                            "should": filters
                        }
                    }
                }
            }
        }

        if self._q_field:
            search_query["query"]["bool"]["must"]["query_string"]["default_field"] = self._q_field


        self.logger.debug("query search: " + json.dumps(search_query))

        @CacheDecorator()
        def cache_es(self_search: IIIFMetadataSearch, query: dict, es_host=self.es.transport.hosts, proj=self.project) -> list:
            """ nested function so that data from es is also cached and can be accessed via cache

            :param self: pass self-object to cache function
            :param query: query to build cache key
            :return: data from es either via cache or es call
            """
            es_results = helpers.scan(self_search.es, index=self.index, query=query)
            results = self._filter_text_field(es_results)
            return results

        return cache_es(self, query=search_query)

    def _filter_text_field(self, es_results: list) -> list:
        """
        filters from textcoords fields only relevant hits within a page

        :param es_results: results from es query
        :return: list containing found words with coords within a canvas_id
        """

        hits = []
        for result in es_results:
            canvas_id = result["_source"].get(self.s.mapping_manifest["page_id"],[])
            if isinstance(canvas_id, list):
                hits.extend(canvas_id)
            else:
                hits.append(canvas_id)
        return hits

    def _build_iiif_search_response(self) -> dict:
        """
        builds iiif response to search request
        :return: dict contatining iiif annotation list
        """
        response = deepcopy(IIIFMetadataSearch.iiif_response)
        response["@id"] = self._build_search_id(self._q, self._manifest_ids)

        for canvas_id in self._word_results:
            new_resource = deepcopy(IIIFMetadataSearch.iiif_no_fulltext_resource)
            new_resource["on"] = self._build_iiif_id("on_canvas", canvas_id ) + "#xywh=0,0,0,0"
            new_resource["@id"] = self._build_iiif_id("resource", canvas_id ) + "#xywh=0,0,0,0"

            response["resources"].append(new_resource)

        return response
