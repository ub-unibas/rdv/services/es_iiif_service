import os
import sys
import importlib
import configparser
from pkg_resources import Requirement, resource_filename
import logging.config
import yaml

module_name = "es_iiif_service"
es_localhost_lookup = {"http://sb-ues5.swissbib.unibas.ch:8080": "localhost:9209",
                        "http://ub-afrikaportal.ub.unibas.ch:9200": "localhost:9208"}
import os
_ROOT = os.path.abspath(os.path.dirname(__file__))
MODE = os.getenv('MODE') or "debug"

class IniSettings:

    def __init__(self, ini_file: str, global_ini_file: str, base_url: str, debug: bool) -> None:
        """load settings from ini file into settings object for easier access

        :param ini_file: path for project ini file
        :param global_ini_file: path for global settings
        :param base_url: name of base url for iiif service from flask request
        :param debug: if app running in debug mode
        """

        try:
            if debug:
                global_ini_file = os.path.dirname(global_ini_file) + "/test_" + os.path.basename(global_ini_file)
                print("GLOBAL INI FILE {} {}".format(global_ini_file, debug))
            config_global = configparser.ConfigParser()
            global_ini_file = resource_filename(Requirement.parse("es_iiif_service"), global_ini_file)
            config_global.read(global_ini_file)
        except Exception as e:
            import traceback
            traceback.print_exc()
            print("Global Settings " + global_ini_file + " does not exist!" + str(e))
            sys.exit(1)
        try:
            self.debug = debug
            self.logger = self.get_logger(ini_file)
        except Exception as e:
            import traceback
            traceback.print_exc()
            print("Global Settings " + global_ini_file + " does not exist!" + str(e))
            sys.exit(1)

    @staticmethod
    def get_logger(ini_file):

        config = yaml.load(open(resource_filename(Requirement.parse("es_iiif_service"), "es_iiif_ini/logger.yaml")), Loader=yaml.Loader)
        logging.config.dictConfig(config)
        if MODE in ["test"] or "test_" in ini_file:
            return logging.getLogger('es_iiif_test')
        elif MODE in ["prod"]:
            return logging.getLogger('es_iiif_prod')
        else:
            return logging.getLogger('es_iiif_debug')

class ViewSettings(IniSettings):
    """attributes should be subset of IIIFIniSettings, so also IIIFIniSettings object can be used e.g. in IIIFAfrikaportal """

    def __init__(self, ini_file: str, global_ini_file: str, base_url: str, debug: bool) -> None:
        """load settings from ini file into settings object for easier access

        :param ini_file: path for project ini file
        :param global_ini_file: path for global settings
        :param base_url: name of base url for iiif service from flask request
        :param debug: if app running in debug mode
        """

        super(ViewSettings, self).__init__(ini_file, global_ini_file, base_url, debug)

        try:
            config = configparser.ConfigParser()
            ini_file = resource_filename(Requirement.parse("es_iiif_service"), ini_file)
            config.read(ini_file)
        except Exception as e:
            self.logger.critical("Project " + ini_file + " does not exist!" + str(e))
            sys.exit(1)

        try:
            self.debug = debug
            module = importlib.import_module(module_name)
            self.index_manifest = dict(config["es"])["index"]
            self.es_host = dict(config["es"])["host"]
            self.project = dict(config["project"])
            self.view_class = getattr(module, self.project.get("view_class", "ViewGenerator"))
            self.show_all_metadata = self.project.get("show_all_metadata", "")
            if debug:
                self.es_host = es_localhost_lookup.get(self.es_host)
                # todo
                self.project["auth_file"] = "/home/martin/Documents/doku/google_drive/iiifafrikaportal-7e3dc9cb627c.json"
        except Exception as e:
            self.logger.critical("Problem loading Config file: " + ini_file + " " + str(e.args))


class IIIFIniSettings(IniSettings):

    def __init__(self, ini_file: str, global_ini_file: str, base_url: str, debug: bool) -> None:
        """load settings from ini file into settings object for easier access

        :param ini_file: path for project ini file
        :param global_ini_file: path for global settings
        :param base_url: name of base url for iiif service from flask request
        :param debug: if app running in debug mode
        """

        super(IIIFIniSettings, self).__init__(ini_file, global_ini_file, base_url, debug)

        try:
            config = configparser.ConfigParser()
            ini_file = resource_filename(Requirement.parse("es_iiif_service"), ini_file)
            config.read(ini_file)
            print(ini_file)
        except Exception as e:
            self.logger.critical("Project " + ini_file + " does not exist!" + str(e))
            sys.exit(1)

        try:

            self.debug = debug
            # dict containing hosts (iiif_server, iiif_service, seealso_url, ES)
            self.hosts = dict(config["hosts"])
            # for compatibility with ViewSettings
            self.es_host = self.hosts["es_host"]
            self.base_url = base_url if base_url.endswith("/") else base_url + "/"

            module = importlib.import_module(module_name)
            # class which is used to build collections
            self.project = config["project"]
            if debug:
                self.hosts["es_host"] = es_localhost_lookup.get(self.es_host)
                self.es_host = es_localhost_lookup.get(self.es_host)
                # todo
                self.project["auth_file"] = "/home/martin/Documents/doku/google_drive/iiifafrikaportal-7e3dc9cb627c.json"
            if self.project.get("use_cache"):
                self.use_cache = False if self.project.get("use_cache") == "False" else True

            self.collection_class = getattr(module, self.project.get("collection_class","IIIFCollection"))

            # class which is used to build manifests
            self.manifest_class = getattr(module, self.project.get("manifest_class","IIIFManifest"))

            # cache for
            self.manifest_cache = self.project.get("manifest_cache", False)
            self.collection_cache = self.project.get("collection_cache", False)
            self.iiif_exp_time = int(self.project.get("iiif_exp_time", "3600"))

            # name of index where information to collections and manifests is stored
            self.index_manifest = config["index"]["index_manifest"]
            # name of index to do search within metadata (if no fulltext is available)
            self.index_metadata_search = config["index"].get("index_metadata_search","")
            # name of index where fulltext information is stored
            self.index_fulltext = config["index"].get("index_fulltext","")

            # array containing fields which are used to build collections structure
            self.collection_structure = config["collection"]["collection_structure"].split("/")
            # array containing fields which are used to build manifest structure
            self.manifest_structure = config["manifest"]["manifest_structure"].split("/")
            # limit for pages within one manifest (if multiple manifests are combined to one)
            self.page_limit = int(config["manifest"]["page_limit"])
            # array containing fields which are used to sort manifest structure
            self.manifest_sort = [c for c in config["manifest"]["manifest_sort"].split(",") if c]
            # array containing fields which are used to sort collection structure
            self.collection_sort = config["collection"]["collection_sort"].split(",")

            # dict containing mapping for relevant fields to build manifest
            self.mapping_manifest = dict(config["mapping_manifest"])
            # dict containing mapping for relevant fields to access fulltext
            self.mapping_fulltext = dict(config["mapping_fulltext"])

            self.url_param = {}
        except Exception as e:
            import traceback
            traceback.print_exc()
            self.logger.critical("Problem loading Config file: " + ini_file + " " + str(e.args))
