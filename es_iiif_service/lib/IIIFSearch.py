import json
from copy import deepcopy

from elasticsearch import helpers

from es_iiif_service.lib import IIIFUtils, IIIFIniSettings
from cache_decorator_redis_ubit import CacheDecorator


class IIIFSearch(IIIFUtils):

    def __init__(self, settings: IIIFIniSettings, q: str, manifest_ids: str, project: str = "", flex: bool = False) -> None:
        """
        do fulltext query within a manifest, returns iiif annotation list with results
        returns first the full page (_search_es),
        afterwards relevant coords and words are extracted (_filter_text_field)

        :param settings: settings for IIIF Service
        :param q: string to query
        :param manifest_ids: list of manifest ids seperated by ",", limits q to ids which are part of manifest
        :param project: name of project
        :param flex: if manifest is created based on es query and not stable
        """
        super(IIIFSearch, self).__init__(settings, project, flex)
        self.index = self.s.index_fulltext
        self._q = q.lower()
        self._manifest_id = manifest_ids
        self._manifest_ids = self.split_manifest_ids(manifest_ids)

        # mapping
        self._textcoords = self.s.mapping_fulltext["textcoords"]
        self._fulltext = self.s.mapping_fulltext["fulltext"]
        self._manifest_field = self.s.mapping_fulltext["manifest"]
        super().after_init()

        self._word_results = self._search_es()

        self.result = self._build_iiif_search_response()

    def _search_es(self) -> list:
        """
        search es for items containing query word in fulltext
        :return: list of pages containing results
        """

        if self._manifest_ids[0].startswith("$q") and self.s.project.get("request_param") == "True":
            query_term = self._manifest_ids[0][2:]
            filters = {"match": {self.s.mapping_fulltext["fulltext"]: query_term}}
        else:
            filters = {"terms": {self._manifest_field: self._manifest_ids}}


        match_q = {"match": {self._fulltext: self._q}}
        match_qs = {"bool": {"should":[]}}
        q_list = match_qs["bool"]["should"]
        for q_word in self._q.split(" "):
            q_list.append({"match": {self._fulltext: q_word}})

        search_query = {
            "query": {
                "bool": {
                    "must": match_qs,
                    "filter": {
                        "bool": {
                            "should": filters
                        }
                    }
                }
            }
        }

        if self._manifest_query:

            orig_query = deepcopy(self._manifest_query)
            # search_query = json.loads(self._manifest_query)
            search_query = self._manifest_query
            # todo: anpassen
            for q in [orig_query, self._manifest_query]:
                for key in ["_source", "aggs"]:
                    try:
                        del q[key]
                    except KeyError:
                        pass
            search_query.setdefault("query",{}).setdefault("bool",{}).setdefault("must",[]).append(match_qs)


        self.logger.debug("query search: " + json.dumps(search_query))

        @CacheDecorator()
        def cache_es(self_search: IIIFSearch, query: dict, es_host=self.es.transport.hosts, proj=self.project) -> list:
            """ nested function so that data from es is also cached and can be accessed via cache

            :param self: pass self-object to cache function
            :param query: query to build cache key
            :return: data from es either via cache or es call
            """
            # dfs_query_then_fetch so order stays the same -> important for uv scroll vies
            es_results = self.es.search(index=self.index, body=query, search_type="dfs_query_then_fetch")

            # for scroll_view search, empty pages also need to be taken into account and added to results
            if self.project == "zas_flex_manifest2":
                search_hits = {page["_id"]: page for page in es_results.get("hits", {}).get("hits", [])}
                orig_query.update({"_source": ["width", self.s.mapping_manifest["page_id"], "pages"]})
                orig_search_result = self.es.search(index=self.index, body=orig_query, search_type="dfs_query_then_fetch")
                orig_hits = orig_search_result.get("hits", {}).get("hits", [])
                for n, orig_page in enumerate(orig_hits):
                    page_id = orig_page["_id"]
                    if page_id in search_hits:
                        orig_hits[n] = search_hits[page_id]
                    else:
                        orig_page["_source"][self._textcoords] = [[]] * orig_page["_source"]["pages"]
                es_results = orig_search_result

            results = self._filter_text_field(es_results)

            return results

        filter_result = cache_es(self, query=search_query, es_host=self.es.transport.hosts, proj=self.project)
        return filter_result

    def _filter_text_field(self, es_results: list) -> list:
        """
        filters from textcoords fields only relevant hits within a page

        :param es_results: results from es query
        :return: list containing found words with coords within a canvas_id
        """
        word_range = 10
        hits = []
        full_scroll_width = 0
        start_canvas_id = ""
        for n, result in enumerate(es_results.get("hits",{}).get("hits",[])):

            page = result["_source"]
            canvas_ids = result["_source"][self.s.mapping_manifest["page_id"]]
            # todo: anpassen
            if self.project in ["ub-newspapers", "arbeitgeber"]:
                canvas_ids = [p["identifier"] for p in result["_source"]["pages"]]
            # because format in dizas2es was changed (Uebergangslösung)
            if isinstance(page.get(self._textcoords), dict):
                array_pages = [page[self._textcoords][str(p)] for p in sorted([int(x) for x in page[self._textcoords].keys()])]
                page[self._textcoords] = array_pages
            width_pages = page.get("width",[])
            if not start_canvas_id:
                if isinstance(canvas_ids, list):
                    start_canvas_id = canvas_ids[0]
                else:
                    start_canvas_id = canvas_ids
            for j, each_page in enumerate(page.get(self._textcoords,[]), start=0):

                for w, word_tuple in enumerate(each_page, start=0):
                    word, coord = word_tuple
                    # scroll view, corrds need to be calculated from starting point
                    if self.project == "zas_flex_manifest2":
                        coord_parts = coord.split(",")
                        coord_parts[0] = str(int(coord_parts[0]) + full_scroll_width)
                        coord = ",".join(coord_parts)
                    for q in self._q.split(" "):
                        if q in word:
                            if isinstance(canvas_ids, list):
                                canvas_id = canvas_ids[j]
                                # hack für avis blatt and Zas collection
                                if isinstance(canvas_id, dict):
                                    canvas_id = canvas_id.get("identifier") or canvas_id.get("id") or canvas_id.get("url")
                            else:
                                canvas_id = canvas_ids
                            start = w - word_range if w - word_range > 0 else 0
                            end = w + word_range if w + word_range < len(each_page) else 0
                            before = [b[0] for b in each_page[start:w]]
                            after = [e[0] for e in each_page[w + 1:end]]
                            if self.project == "zas_flex_manifest2":
                                hits.append([word, coord, start_canvas_id, before, after])
                            else:
                                hits.append([word, coord, canvas_id, before, after])
                width = width_pages[j] if width_pages and isinstance(width_pages, list) else page.get("width",0)
                full_scroll_width += int(width)
        return hits

    def _build_iiif_search_response(self) -> dict:
        """
        builds iiif response to search request
        :return: dict contatining iiif annotation list
        """
        response = deepcopy(IIIFSearch.iiif_response)
        response["@id"] = self._build_search_id(self._q, self._manifest_ids)

        for word, coord, canvas_id, before, after in self._word_results:
            if not coord.startswith("#xywh="):
                coord = "#xywh=" + coord
            new_resource = deepcopy(IIIFSearch.iiif_resource)
            new_resource["resource"]["chars"] = word
            new_resource["on"] = self._build_iiif_id("on_canvas", canvas_id + coord)
            new_resource["@id"] = self._build_iiif_id("resource", canvas_id + coord)

            new_hit = deepcopy(IIIFSearch.iiif_search_hit)
            new_hit["annotations"].append(new_resource["@id"])
            new_hit["before"] = " ".join(before)
            new_hit["after"] = " ".join(after)
            new_hit["match"] = word

            response["hits"].append(new_hit)
            response["resources"].append(new_resource)
        return response
