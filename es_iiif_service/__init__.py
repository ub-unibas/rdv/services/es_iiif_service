from .lib import IIIFIniSettings, ViewSettings, IIIFUtils, ESAggTemplates, \
    IIIFCollection, IIIFCollection_v3, IIIFManifest, IIIFManifestPDF, IIIFManifest_v3, IIIFSearch, IIIFMetadataSearch, IIIFAutocomplete, \
    ViewGenerator, ZasView, PortraetView, AfrikaportalView, UnibasDigiView, NewspaperView
from .projects import IIIFBaseProjManifest, IIIFBaseProjManifest_v3, IIIFImpressoCollection, IIIFImpressoManifest, \
    IIIFDizasCollection, IIIFDizasManifest, \
    IIIFDizasCollection_v3, IIIFDizasManifest_v3, \
    IIIFSozialarchivCollection, IIIFSozialarchivManifest, \
    IIIIFAfrikaportalCollection, IIIFAfrikaportalManifest, \
    IIIFAllDataCollection, IIIFAllDataManifest, \
    IIIFAvisBlattManifest_v3, IIIFAvisBlattManifest,\
    IIIFVLMCollection, IIIFVLMManifest,\
    IIIFZettelkatalogCollection, IIIFZettelkatalogManifest, IIIFZettelkatalogIDManifest,\
    IIIFMemobaseCollection, IIIFMemobaseManifest, \
    IIIFSWAKleinschriftenCollection, IIIFSWAKleinschriftenManifest, IIIFPortraetsManifest_v3, IIIFPortraetsManifest, \
    IIIFSpezialkatManifest_v3, IIIFSpezialkatManifest, IIIFBurckhardtManifest_v3, IIIFBurckhardtManifest, \
    IIIFSWASearchManifest, IIIFSWASearchManifest_v3, IIIFBernoulliManifest, IIIFBernoulliManifest_v3, \
    IIIFNL351Manifest_v3, IIIFNL351Manifest, IIIFAutographenManifest, IIIFAutographenManifest_v3, \
    IIIFITBManifest_v3, IIIFITBManifest, IIIFGbooksManifest_v3, IIIFGbooksManifest, IIIFDigihubManifest, IIIFDigihubManifest_v3


from .esIIIFServiceRest import es_iiif_app

