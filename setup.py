#!/usr/bin/python3
import sys
import os
import re
from setuptools import setup, find_packages


def read(filename):
    return open(os.path.join(os.path.dirname(__file__), filename)).read()

long_description = ""

setup(
    name='es_iiif_service',
    setup_requires=['setuptools-git-versioning'],
    setuptools_git_versioning={
        "enabled": True,
    },
    packages=find_packages(),
    description='Implementation of IIIF Presentation and Search API based on elasticsearch',
    author='Martin Reisacher',
    author_email='martin.reisacher@unibas.ch',
    install_requires=['pygsheets', 'oauth2client', 'elasticsearch<8', 'flask', 'flask_cors',  'flask-restx', 'flask_compress', 'uwsgi',
                      'tripoli', 'pyyaml', 'cache_decorator_redis_ubit', 'logging_decorator_ubit', 'rdv_query_builder',#'rdv_iiif_download',
                      'es_iiif_service', 'werkzeug', 'rdv_data_helpers_ubit', 'rdv_querybuilder_afrikaportal_ubit', 'rdv_querybuilder_zas_ubit',
                      'rdv_querybuilder_proj_ubit', 'rdv_querybuilder_ideas_ubit', 'rdv_querybuilder_itb_ubit', 'rdv_querybuilder_discarded_ubit',
                        'rdv_querybuilder_swa_ubit', 'rdv_config_store_ubit', 'setuptools'
                       ],
    url='',
    long_description=long_description,
    keywords=['elasticsearch', 'elastic', 'iiif', 'iiif-presentation', 'iiif-search', 'cryptography'],
    classifiers=[],
    license='MIT',
    include_package_data=True,
    zip_safe=False
)
