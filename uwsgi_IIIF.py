import os
import logging
from flask.logging import default_handler
from es_iiif_service import es_iiif_app

# If app is started via gunicorn
if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.logger')
    es_iiif_app.logger.handlers = gunicorn_logger.handlers
    es_iiif_app.logger.setLevel(gunicorn_logger.level)
    es_iiif_app.logger.removeHandler(default_handler)
    es_iiif_app.logger.info('Starting production server')

if __name__ == "__main__":
    if os.environ.get('RDV_DEBUG'):
        es_iiif_app.run(debug=True, port=5001, ssl_context='adhoc')
    else:
        es_iiif_app.run(debug=False, port=5000)
