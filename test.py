from pprint import pprint

from tripoli import IIIFValidator

from old.esIIIFService import get_settings
from es_iiif_generator.lib import IIIFSearch, IIIFAutocomplete

def_tests = [
    # tests for dizas project
    {"test_dizas": [
        {"type": "manifest", "manifest_ids": "000280089"},
        # {"type": "collection", "collection_path": ""},
        {"type": "search", "q": "Versandhäuser", "manifest_ids": "000280089"},
        # {"type": "autocomplete", "q": "über", "manifest_ids": "000280089"}
    ]},
    {"test_dizas_object": [
        # {"type": "collection", "collection_path": "Bekleidung/Berner Tagwacht"},
        {"type": "manifest", "manifest_ids": "952695,952700"},

        # {"type": "search", "q": "Duden", "manifest_ids": "603246"},
        # {"type": "autocomplete", "q": "Webstuhl", "manifest_ids": "946160,952831"}
    ]},
    {"test_dizas_zeitung": [{"type": "collection", "collection_path": ""},
                            # test for \n with manifest id
                            {"type": "manifest", "manifest_ids": "Der Bund\n"}]},
    # tests for impresso project
    {"test_impresso": [{"type": "collection", "collection_path": "schwha/1892"},
                       {"type": "collection", "collection_path": ""},
                       {"type": "manifest", "manifest_ids": "schwha-1882-01-02-a-issue"}]},
    # tests for avis-blatt project
    # tests for afrikaportal project
    {"test_afrikaportal": [{"type": "manifest", "manifest_ids": "175eb163-2f84-5b85-9164-e0a6b1ae0d59"},
                           {"type": "collection", "collection_path": ""},
                           {"type": "search", "q": "Windhoek",
                            "manifest_ids": "175eb163-2f84-5b85-9164-e0a6b1ae0d59"}]},
    {"test_afrikaportal_place": [{"type": "collection", "collection_path": "bab_library/PlakateCSV.csv"},
                                 {"type": "manifest", "manifest_ids": "Deutschland"}]},
    {"test_afrikaportal_year": [{"type": "manifest", "manifest_ids": "1960"},
                                {"type": "collection", "collection_path": "bab_library/PlakateCSV.csv"},
                                {"type": "search", "q": "Schweiz", "manifest_ids": "003889012"},
                                {"type": "autocomplete", "q": "eine", "manifest_ids": "003889012"}]},
    # tests for digispace project
    {"test_digispace": [
        {"type": "manifest", "manifest_ids": "5_000111981"},
        #  {"type": "collection", "collection_path": ""}
    ]},
    {"test_avis-blatt": [{"type": "collection", "collection_path": "wochnaaud/1761"},
                         {"type": "manifest", "manifest_ids": "wochnaaud-1761-03-12-a-issue"},
                         ]},
    {"test_orplayground": [{"type": "collection", "collection_path": ""},
                           {"type": "manifest", "manifest_ids": "Huber; Hans"},
                           ]},
    {"test_ezas_sb": [{"type": "manifest", "manifest_ids": "Goldmarkt"}]},
    {"test_vlm": [
        # {"type": "collection", "collection_path": ""},
        {"type": "manifest", "manifest_ids": "5_000133072"}
    ]},
    {"test_vlm_basel": [
        # {"type": "collection", "collection_path": ""},
        {"type": "manifest", "manifest_ids": "Karte"}
    ]},
    {"test_digispace_collection": [

#{"type": "collection", "collection_path": "1_000178563/goobi"},
        {"type": "collection", "collection_path": "5_000213263/vlm"},
        {"type": "manifest", "manifest_ids": "Karte"}
    ]}
,
    {"test_ak2": [
        {"type": "collection", "collection_path": "Z"},
        #{"type": "manifest", "manifest_ids": "Meiwes, Dieter"}
    ]},
    {"test_mscr": [
        {"type": "collection", "collection_path": ""},
        # {"type": "manifest", "manifest_ids": "Meiwes, Dieter"}S
    ]}
]

tests = ["test_ak2"]

def_tests = filter(lambda x: list(x.keys())[0] in tests, def_tests) if tests else def_tests
# run validator for tests
for project_tests in def_tests:
    for project, tests in project_tests.items():
        print(project)
        # get settings
        s = get_settings(project, "localhost")
        for test in tests:
            print(test)
            iiif_type = test.pop("type")
            iiif_class = object
            if iiif_type == "manifest":
                iiif_class = s.manifest_class
            elif iiif_type == "collection":
                iiif_class = s.collection_class
            elif iiif_type == "search":
                iiif_class = IIIFSearch
            elif iiif_type == "autocomplete":
                iiif_class = IIIFAutocomplete

            object_ = iiif_class(settings=s, project=project, **test)
            pprint(object_.result)
            iv = IIIFValidator()
            iv.validate(object_.result)
