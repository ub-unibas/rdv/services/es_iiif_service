FROM index.docker.io/library/python:3.8

ARG K8S_VERSION=v1.20.4
ARG HELM_VERSION=v3.6.3

ENV FLASK_APP rdv_query_builder
ENV MODE k8s

WORKDIR /
RUN mkdir configs
ADD setup.py /
ADD uwsgi_IIIF.py /
ADD requirements.txt /
ADD helm-charts/configmap/test/gunicorn.conf.py /configs/

ADD es_iiif_service /es_iiif_service
ADD es_iiif_ini /es_iiif_ini

RUN python -m pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

WORKDIR /

EXPOSE 5000
#ENTRYPOINT ["bash"]
ENTRYPOINT ["gunicorn"]
CMD ["uwsgi_IIIF:es_iiif_app", "--config", "/configs/gunicorn.conf.py"]
